﻿using UnityEngine;

namespace CargoCult.Template.Common.Separator
{
    public class SeparatorAttribute : PropertyAttribute
    {
        public readonly string Title;

        public SeparatorAttribute()
        {
            Title = "";
        }

        public SeparatorAttribute(string title)
        {
            Title = title;
        }
    }
}