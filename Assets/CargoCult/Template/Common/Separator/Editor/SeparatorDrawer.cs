﻿using UnityEditor;
using UnityEngine;

namespace CargoCult.Template.Common.Separator.Editor
{
    [CustomPropertyDrawer(typeof(SeparatorAttribute))]
    public class SeparatorDrawer : DecoratorDrawer
    {
        private SeparatorAttribute SeparatorAttribute => (SeparatorAttribute)attribute;

        public override void OnGUI(Rect position)
        {
            if(SeparatorAttribute.Title == "")
            {
                position.height = 1;
                position.y += 19;
                GUI.Box(position, "");
            } else
            {
                Vector2 textSize = GUI.skin.label.CalcSize(new GUIContent(SeparatorAttribute.Title));
                textSize = textSize * 2;
                float separatorWidth = (position.width - textSize.x) / 2.0f - 5.0f;
                position.y += 19;

                GUI.Box(new Rect(position.xMin, position.yMin, separatorWidth, 3), "");
                GUI.Box(new Rect(position.xMin + separatorWidth + 5.0f, position.yMin - 8.0f, textSize.x, 20), SeparatorAttribute.Title);
                GUI.Box(new Rect(position.xMin + separatorWidth + 10.0f + textSize.x, position.yMin, separatorWidth, 3), "");
            }
        }

        public override float GetHeight()
        {
            return 41.0f;
        }
    }
}