﻿using System;
using System.Collections.Generic;
using CargoCult.Template.Common.PanelManager.UI;
using CargoCult.Template.Global.BackButton;
using CargoCult.Template.Global.Sound;
using CargoCult.Template.Injectro.Core;
using UnityEngine;

namespace CargoCult.Template.Common.PanelManager
{
    [Singleton]
    public class PanelManager : MonoBehaviour, IPanelManager, IPostResolved
    {
        [Inject] private IBackButtonManager backButtonManager;
        [Inject] private ISoundManager soundManager;

        private readonly List<IView> openedPopUps = new List<IView>();
        private readonly List<IUIHider> hiders = new List<IUIHider>();
        private IView currentContent;
        private IView currentToolbar;
        private IView currentBanner;
        private bool isUIHided;
        private bool openingLastPanel;

        private readonly Stack<ReturnablePanelContainer> returnablePanelContainers = new Stack<ReturnablePanelContainer>();
        
        public void PostResolved()
        {
            backButtonManager.PanelManagerResolved(this);
        }
        
        public void OpenPanel(IView view)
        {
            switch (view.GetViewType())
            {
                case ViewType.Content:
                    OpenContent(view);
                    break;
                case ViewType.Toolbar:
                    OpenToolbar(view);
                    break;
                case ViewType.PopUp:
                    OpenPopUp(view);
                    break;
                case ViewType.Banner:
                    OpenBanner(view);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            GameObject panelGO = view.GetGameObject();
            if (panelGO != null && !isUIHided)
            {
                panelGO.SetActive(true);
            }
        }

        public void ClosePanel(IView view)
        {
            switch (view.GetViewType())
            {
                case ViewType.Content:
                    CloseContent(view);
                    break;
                case ViewType.Toolbar:
                    CloseToolbar(view);
                    break;
                case ViewType.PopUp:
                    ClosePopUp(view);
                    break;
                case ViewType.Banner:
                    CloseBanner(view);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            GameObject panelGO = view.GetGameObject();
            
            if (panelGO != null)
                panelGO.SetActive(false);
        }

        public IView GetCurrentOpenedContentView()
        {
            return currentContent;
        }

        public bool IsHaveOpenedPopup()
        {
            return openedPopUps.Count > 0;
        }

        public void HideUI(IUIHider hider)
        {
            isUIHided = true;
            
            hiders.Add(hider);
            
            currentContent?.GetGameObject().SetActive(false);
            currentToolbar?.GetGameObject().SetActive(false);
            for (int i = 0; i < openedPopUps.Count; i++)
            {
                IView popUp = openedPopUps[i];
                popUp.GetGameObject().SetActive(false);
            }
        }

        public void UnhideUI(IUIHider hider)
        {
            hiders.Remove(hider);

            if (hiders.Count == 0)
            {
                isUIHided = false;
                
                currentContent?.GetGameObject().SetActive(true);
                currentToolbar?.GetGameObject().SetActive(true);
                for (int i = 0; i < openedPopUps.Count; i++)
                {
                    IView popUp = openedPopUps[i];
                    popUp.GetGameObject().SetActive(true);
                }
            }
        }

        public void CloseAllPanels()
        {
            for (int i = openedPopUps.Count - 1; i >= 0; i--)
            {
                IView popUp = openedPopUps[i];
                popUp.Close();
            }

            if (null != currentContent)
            {
                currentContent.Close();
            }

            if (null != currentToolbar)
            {
                currentToolbar.Close();
            }
        }

        public void ResetActiveInputCallers()
        {
            for (int i = openedPopUps.Count - 1; i >= 0; i--)
            {
                IView popUp = openedPopUps[i];
                popUp.ResetInputCallers();
            }

            if (currentContent != null)
            {
                currentContent.ResetInputCallers();
            }

            if (currentToolbar != null)
            {
                currentToolbar.ResetInputCallers();
            }
        }

        public void ResetViewCache()
        {
            returnablePanelContainers.Clear();
        }
        
        public bool TryProcessBackButtonClick()
        {
            if (currentContent != null && !currentContent.CanBeClosedWithBackButton() || isUIHided)
            {
                return true;
            }

            bool isProcessed = false;
            if (openedPopUps.Count > 0)
            {
                IView popUp = openedPopUps[openedPopUps.Count - 1];
                if (!popUp.CanBeClosedWithBackButton())
                {
                    return false;
                }
                
                popUp.Close();
                popUp.ClosedByBackButton();
                isProcessed = true;
            }
            else if (returnablePanelContainers.Count > 0)
            {
                openingLastPanel = true;
                ReturnablePanelContainer container = returnablePanelContainers.Pop();
                container.Panel.SetPanelStatus(container.Status);
                IView activeContentPanel = currentContent;
                OpenPanel((IView)container.Panel);
                activeContentPanel?.ClosedByBackButton();
                isProcessed = true;
            }

            return isProcessed;
        }
        
        private void OpenContent(IView contentView)
        {
            if (currentContent != null)
            {
                CacheView(currentContent);
                currentContent.OnFocusLost();
                currentContent.Close();
            }
            
            currentContent = contentView;

            if (openedPopUps.Count == 0 || !AnyViewOverlapFocus(openedPopUps))
            {
                currentContent.OnFocus();
            }
        }

        private void CacheView(IView view)
        {
            if (!openingLastPanel)
            {
                if (view is IReturnablePanel returnablePanel)
                {
                    returnablePanelContainers.Push(new ReturnablePanelContainer(returnablePanel,
                        returnablePanel.GetPanelStatus()));
                }
            }
            else
            {
                openingLastPanel = false;
            }
        }

        private void OpenPopUp(IView popUpView)
        {
            soundManager.SetPause(popUpView, true);
            
            if (popUpView.IsOverlapFocus())
            {
                if (openedPopUps.Count <= 0)
                {
                    if (currentContent != null)
                        currentContent.OnFocusLost();

                    if (currentToolbar != null)
                        currentToolbar.OnFocusLost();
                }
                else
                {
                    openedPopUps[openedPopUps.Count - 1].OnFocusLost();
                }
            }

            openedPopUps.Add(popUpView);
            popUpView.OnFocus();
            
            GameObject viewGameObject = popUpView.GetGameObject();
            if (viewGameObject != null)
            {
                RectTransform viewRectTransform = viewGameObject.transform as RectTransform;
                if (viewRectTransform != null)
                {
                    viewRectTransform.SetAsLastSibling();
                }
            }
        }
        
        private void OpenBanner(IView bannerView)
        {
            if (currentBanner != null)
            {
                currentBanner.OnFocusLost();
                currentBanner.Close();
            }

            currentBanner = bannerView;
        }

        private void OpenToolbar(IView toolbarView)
        {
            if (currentToolbar != null)
            {
                currentContent.OnFocusLost();
                currentToolbar.Close();
            }

            currentToolbar = toolbarView;

            if (openedPopUps.Count == 0 || !AnyViewOverlapFocus(openedPopUps))
            {
                currentToolbar.OnFocus();
            }
        }

        private void CloseBanner(IView bannerView)
        {
            if (currentBanner == bannerView)
            {
                currentBanner.OnFocusLost();
                currentBanner = null;
            }
        }
        
        private void CloseContent(IView contentView)
        {
            if (currentContent == contentView)
            {
                currentContent.OnFocusLost();
                currentContent = null;
            }
        }

        private void CloseToolbar(IView toolbarView)
        {
            if (currentToolbar == toolbarView)
            {
                currentToolbar.OnFocusLost();
                currentToolbar = null;
            }
        }

        private void ClosePopUp(IView popUpView)
        {
            soundManager.SetPause(popUpView, false);
            popUpView.OnFocusLost();
            openedPopUps.Remove(popUpView);

            if (popUpView.IsOverlapFocus())
            {
                if (openedPopUps.Count == 0)
                {
                    if (currentContent != null)
                        currentContent.OnFocus();

                    if (currentToolbar != null)
                        currentToolbar.OnFocus();
                }
                else
                {
                    openedPopUps[openedPopUps.Count - 1].OnFocus();
                }
            }
        }

        private bool AnyViewOverlapFocus(List<IView> views)
        {
            for (int i = 0; i < views.Count; i++)
            {
                IView view = views[i];
                if (view.IsOverlapFocus())
                {
                    return true;
                }
            }

            return false;
        }

        private struct ReturnablePanelContainer
        {
            public readonly IReturnablePanel Panel;
            public readonly ReturnablePanelStatus Status;

            public ReturnablePanelContainer(IReturnablePanel panel, ReturnablePanelStatus status)
            {
                Panel = panel;
                Status = status;
            }
        }
    }
}