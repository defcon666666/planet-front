﻿namespace CargoCult.Template.Common.PanelManager.UI
{
    public interface IFocusHandler
    {
        void OnFocus();
        void OnFocusLost();
    }
}
