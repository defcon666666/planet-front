﻿

namespace CargoCult.Template.Common.PanelManager.UI
{
    public interface IReturnablePanel
    {
        ReturnablePanelStatus GetPanelStatus();
        void SetPanelStatus(ReturnablePanelStatus status);
    }

    public class ReturnablePanelStatus
    {
    }
}