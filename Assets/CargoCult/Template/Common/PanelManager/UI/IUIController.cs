﻿using CargoCult.Template.Injectro.Core;

namespace CargoCult.Template.Common.PanelManager.UI
{
    public interface IUIController : IBean
    {
        void OnOpen();
        void OnClose();
        void OnFocus();
        void OnFocusLost();
        void OnClosedByBackButton();
    }
}
