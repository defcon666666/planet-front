﻿using CargoCult.Template.Global.Sound;
using CargoCult.Template.Injectro.Core;
using UnityEngine;

namespace CargoCult.Template.Common.PanelManager.UI
{
    public enum ViewType
    {
        Content,
        Toolbar,
        PopUp,
        Banner,
    }

    public interface IView : IBean, ISoundPauseChanger
    {
        void Open();
        void Close();
        void OnFocus();
        void OnFocusLost();
        IUIController GetCurrentController();
        GameObject GetGameObject();
        ViewType GetViewType();
        bool IsOverlapFocus();
        bool IsActive();
        void ResetInputCallers();
        bool CanBeClosedWithBackButton();
        void ClosedByBackButton();
    }
}