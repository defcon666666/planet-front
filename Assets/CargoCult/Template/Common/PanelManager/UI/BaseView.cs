﻿using CargoCult.Template.Common.Input.AdditionalComponents;
using CargoCult.Template.Injectro.Core;
using UnityEngine;

namespace CargoCult.Template.Common.PanelManager.UI
{
    [PostResolveDependency]
    public abstract class BaseView : MonoBehaviour, IView, IPostResolved
    {
        protected bool IsOpened;
        protected bool InFocus;
        [Inject] protected IPanelManager PanelManager;
        
        private IFocusHandler[] focusHandlers;
        private InputCaller[] inputCallers;

        public virtual void PostResolved()
        {
            focusHandlers = GetComponentsInChildren<IFocusHandler>(true);
            inputCallers = GetComponentsInChildren<InputCaller>(true);
        }

        public abstract IUIController GetCurrentController();
        
        public abstract ViewType GetViewType();
        
        public virtual bool IsOverlapFocus()
        {
            return true;
        }
        
        public virtual void Open()
        {
            IsOpened = true;
            PanelManager.OpenPanel(this);

            if (GetCurrentController() != null)
            {
                GetCurrentController().OnOpen(); 
            }
        }

        public virtual void Close()
        {
            IsOpened = false;
            if (GetCurrentController() != null)
            {
                GetCurrentController().OnClose();
            }
            PanelManager.ClosePanel(this);
        }

        public virtual void OnFocus()
        {
            InFocus = true;
            if (GetCurrentController() != null)
            {
                GetCurrentController().OnFocus();
            }

            if (focusHandlers != null && focusHandlers.Length > 0)
            {
                for (int i = 0; i < focusHandlers.Length; i++)
                {
                    IFocusHandler focusHandler = focusHandlers[i];
                    focusHandler.OnFocus();
                }
            }
        }
        
        public virtual void OnFocusLost()
        {
            InFocus = false;
            if (GetCurrentController() != null)
            {
                GetCurrentController().OnFocusLost();
            }
            
            if (focusHandlers != null && focusHandlers.Length > 0)
            {
                for (int i = 0; i < focusHandlers.Length; i++)
                {
                    IFocusHandler focusHandler = focusHandlers[i];
                    focusHandler.OnFocusLost();
                }
            }
        }

        public virtual bool CanBeClosedWithBackButton()
        {
            return true;
        }

        public virtual void ClosedByBackButton()
        {
            IUIController currentController = GetCurrentController();
            if (currentController != null)
            {
                currentController.OnClosedByBackButton();
            }
        }

        public GameObject GetGameObject()
        {
            return gameObject;
        }
        
        public bool IsActive()
        {
            return IsOpened;
        }

        public void ResetInputCallers()
        {
            for (int i = 0; i < inputCallers.Length; i++)
            {
                InputCaller inputCaller = inputCallers[i];
                inputCaller.ResetToDefaultState();
            }
        }
    }
}
