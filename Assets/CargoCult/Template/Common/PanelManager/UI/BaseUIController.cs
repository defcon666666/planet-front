﻿using UnityEngine;

namespace CargoCult.Template.Common.PanelManager.UI
{
    public class BaseUIController : MonoBehaviour, IUIController
    {
        protected bool IsOpened;
        
        public virtual void OnOpen()
        {
            IsOpened = true;
        }

        public virtual void OnClose()
        {
            IsOpened = false;
        }

        public virtual void OnFocus()
        {
            
        }

        public virtual void OnFocusLost()
        {
            
        }

        public virtual void OnClosedByBackButton()
        {
        }
    }
}
