﻿using CargoCult.Template.Common.PanelManager.UI;
using CargoCult.Template.Injectro.Core;

namespace CargoCult.Template.Common.PanelManager
{
    public interface IPanelManager : IBean
    {
        void OpenPanel(IView view);
        void ClosePanel(IView view);
        IView GetCurrentOpenedContentView();
        bool IsHaveOpenedPopup();
        void HideUI(IUIHider hider);
        void UnhideUI(IUIHider hider);
        void CloseAllPanels();
        void ResetActiveInputCallers();
        void ResetViewCache();
        bool TryProcessBackButtonClick();
    }

    public interface IUIHider
    {
        
    }
}