﻿using CargoCult.Template.Injectro.Core;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace CargoCult.Template.Common.Input.AdditionalComponents
{
    public class InputHorizontalJoyStick : InputCaller, IPointerDownHandler, IPointerUpHandler, IDragHandler, IEndDragHandler,
        IPostResolved
    {
        public Image Base;
        public Image Thumb;

        public InputType Horizontal;

        public bool InvertX;

        public float MovementRange = 100f;

        private RectTransform thumbRectTransform;
        private Vector3 thumbStartPosition;
        private RectTransform baseRectTransform;
        
        public void PostResolved()
        {
            thumbRectTransform = Thumb.GetComponent<RectTransform>();
            thumbStartPosition = thumbRectTransform.localPosition;
            baseRectTransform = Base.GetComponent<RectTransform>();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            RaiseInputs(CalcInput(CalcThumbPosition(eventData)));
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            ResetToDefaultState();
        }

        public void OnDrag(PointerEventData eventData)
        {
            RaiseInputs(CalcInput(CalcThumbPosition(eventData)));
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            ResetToDefaultState();
        }

        public override void ResetToDefaultState()
        {
            thumbRectTransform.localPosition = thumbStartPosition;
            RaiseInput(Horizontal, 0);
        }

        private void OnDisable()
        {
            ResetToDefaultState();
        }

        private Vector3 CalcInput(Vector3 thumbLocalPosition)
        {
            Vector3 input = Vector3.zero;

            input.x = Mathf.Clamp(thumbLocalPosition.x, -MovementRange, MovementRange);
            input.y = Mathf.Clamp(thumbLocalPosition.y, -MovementRange, MovementRange);
            input.x /= MovementRange;
            input.y /= MovementRange;

            return input;
        }

        private Vector3 CalcThumbPosition(PointerEventData data)
        {
            float localTouchPos = Mathf.Clamp(baseRectTransform.InverseTransformPoint(data.position).x, -MovementRange,
                MovementRange);
            thumbRectTransform.localPosition = new Vector3(localTouchPos, 0);
            return thumbRectTransform.localPosition;
        }

        private void RaiseInputs(Vector2 input)
        {
            int xCounter = InvertX ? -1 : 1;
            RaiseInput(Horizontal, input.x * xCounter);
        }
    }
}