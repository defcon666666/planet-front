﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityImput = UnityEngine.Input;

namespace CargoCult.Template.Common.Input.AdditionalComponents
{
    public class PinchPad : InputCaller, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField] private InputType inputType;
        
        private bool touching;
        private float deltaMagnitudeDiff;
        
        public void OnPointerDown(PointerEventData eventData)
        {
            touching = true;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            touching = false;
        }

        private void Update()
        {
            if (touching && UnityImput.touchCount == 2)
            {
                Touch touchZero = UnityImput.GetTouch(0);
                Touch touchOne = UnityImput.GetTouch(1);

                Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

                deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;
            }
            else
            {
                deltaMagnitudeDiff = 0;
            }
            
            RaiseInput(inputType, deltaMagnitudeDiff);
        }
    }
}
