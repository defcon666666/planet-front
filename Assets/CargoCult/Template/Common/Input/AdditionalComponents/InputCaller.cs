﻿using CargoCult.Template.Injectro.Core;
using UnityEngine;

namespace CargoCult.Template.Common.Input.AdditionalComponents
{
    public class InputCaller : MonoBehaviour, IBean
    {
        public float Sensitivity;

        [Inject] protected IInputManager inputManager;
        
        public virtual void ResetToDefaultState()
        {
        
        }

        protected virtual void RaiseInput(InputType type, float value)
        {
            if (inputManager.GetStandaloneInputsStatus() && DisableInStandaloneInputs())
            {
                return;
            }

            inputManager.RaiseInputEvent(type, value * Sensitivity);
        }

        protected virtual void RaiseInput(InputType type, bool value)
        {
            if (inputManager.GetStandaloneInputsStatus() && DisableInStandaloneInputs())
            {
                return;
            }

            inputManager.RaiseLogicalInputEvent(type, value);
        }

        protected virtual bool DisableInStandaloneInputs()
        {
            return true;
        }
    }
}