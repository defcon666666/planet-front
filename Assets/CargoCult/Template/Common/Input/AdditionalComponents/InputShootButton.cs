using UnityEngine.EventSystems;

namespace CargoCult.Template.Common.Input.AdditionalComponents
{
    public class InputShootButton : InputButton
    {
        public InputTouchPad TouchPad;

        public InputShootButton()
        {
            RaiseOnUp = true;
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            base.OnPointerDown(eventData);
            
            if (TouchPad == null)
            {
                return;
            }

            ExecuteEvents.dragHandler(TouchPad, eventData);
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            base.OnPointerUp(eventData);
            
            if (TouchPad == null)
            {
                return;
            }

            ExecuteEvents.endDragHandler(TouchPad, eventData);
        }
    }
}