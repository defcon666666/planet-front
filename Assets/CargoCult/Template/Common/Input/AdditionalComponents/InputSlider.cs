﻿using System;
using CargoCult.Template.Injectro.Core;
using UnityEngine;
using UnityEngine.UI;

namespace CargoCult.Template.Common.Input.AdditionalComponents
{
    public class InputSlider : InputCaller, IPostResolved
    {
        [SerializeField] private Slider slider;
        [SerializeField] private InputType inputType;
        private float currentValue = 0;

        public void PostResolved()
        {
            slider.onValueChanged.AddListener(UpdateChangeValue);
            slider.value = currentValue;
            inputManager.SubscribeToInputEvent(inputType, SetInput);
        }

        private void UpdateChangeValue(float value)
        {
            currentValue = value;
            RaiseInput(inputType, value);
        }

        private void SetInput(float value)
        {
            if (Math.Abs(currentValue - value) > 0.01f)
            {
                currentValue = value;
                slider.value = currentValue;
            }
        }
    }
}
