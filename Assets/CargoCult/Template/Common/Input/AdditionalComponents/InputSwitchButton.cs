using UnityEngine;
using UnityEngine.UI;

namespace CargoCult.Template.Common.Input.AdditionalComponents
{
    public class InputSwitchButton : InputButton
    {
        [SerializeField] private Image enable;
        [SerializeField] private Image disable;

        private bool status;

        public void SetEnable(bool isEnable)
        {
            status = isEnable;
            enable.gameObject.SetActive(status);
            disable.gameObject.SetActive(!status);
        }
    }
}