﻿using CargoCult.Template.Global.Preference;
using CargoCult.Template.Injectro.Core;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CargoCult.Template.Common.Input.AdditionalComponents
{
    public class InputTouchPad : InputCaller, IDragHandler, IEndDragHandler, IInitializePotentialDragHandler, IPointerUpHandler, IPointerDownHandler
    {
        public InputType HorizontalType;
        public InputType VecrticalType;
        [SerializeField] private InputType downAndUpInputType = InputType.None;
        [SerializeField] private bool activeInStandalone;

        private PointerEventData pointerData;
        private Vector3 lastPosition;
        private float maxScreenSide;

        [Inject] private IPreferenceManager preferenceManager;
        
        public void OnPointerUp(PointerEventData eventData)
        {
            if (downAndUpInputType != InputType.None)
            {
                RaiseInput(downAndUpInputType, 0);
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (downAndUpInputType != InputType.None)
            {
                RaiseInput(downAndUpInputType, 1);
            }
        }
        
        public void OnDrag(PointerEventData eventData)
        {
            if (pointerData == null)
            {
                pointerData = eventData;
                lastPosition = pointerData.position;
            }
        }
    
        public void OnEndDrag(PointerEventData eventData)
        {
            if (eventData == pointerData)
            {
                ResetToDefaultState();
            }
        }

        public void OnInitializePotentialDrag(PointerEventData eventData)
        {
            eventData.useDragThreshold = false;
        }

        public override void ResetToDefaultState()
        {
            pointerData = null;
            RaiseInput(HorizontalType, 0);
            RaiseInput(VecrticalType, 0);
        }

        protected override bool DisableInStandaloneInputs()
        {
            return !activeInStandalone;
        }

        private void Awake()
        {
            maxScreenSide = Mathf.Max(Screen.width, Screen.height);
        }

        private void Update()
        {
            if (pointerData == null)
                return;

            Vector3 input = pointerData.position;
            Vector3 delta = (input - lastPosition) / maxScreenSide;
            lastPosition = input;

            RaiseInput(HorizontalType, -delta.x);
            RaiseInput(VecrticalType, -delta.y);
        }
        
        private void OnDisable()
        {
            ResetToDefaultState();
        }
    }
}