﻿using UnityEngine.EventSystems;

namespace CargoCult.Template.Common.Input.AdditionalComponents
{
    public class InputButton : InputCaller, IPointerDownHandler, IPointerUpHandler
    {
        public InputType Type;
        public bool RaiseOnUp;

        private PointerEventData pointerEventData;

        public virtual void OnPointerDown(PointerEventData eventData)
        {
            pointerEventData = eventData;
            RaiseValue(true);
        }

        public virtual void OnPointerUp(PointerEventData eventData)
        {
            if (RaiseOnUp)
                RaiseValue(false);
            pointerEventData = null;
        }

        public override void ResetToDefaultState()
        {
            if (pointerEventData != null)
            {
                OnPointerUp(pointerEventData);
            }
        }

        protected override bool DisableInStandaloneInputs()
        {
            return false;
        }

        private void OnDisable()
        {
            ResetToDefaultState();
        }

        private void RaiseValue(bool down)
        {
            if (Sensitivity == 0)
            {
                RaiseInput(Type, down);
            }
            else
            {
                RaiseInput(Type, down ? 1 : 0);
            }
        }
    }
}