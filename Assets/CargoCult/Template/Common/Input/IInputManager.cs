﻿using System;
using CargoCult.Template.Injectro.Core;

namespace CargoCult.Template.Common.Input
{
    public enum InputType
    {
        None = -1,
        Jump = 0,
        Horizontal = 1,
        Vertical = 2,
        Fire1 = 3,
        Interact = 4,
    }

    public interface IInputManager : IBean
    {
        bool GetStandaloneInputsStatus();
        void SetStandaloneInputsStatus(bool status);
        void SubscribeToInputEvent(InputType type, Action<float> action, bool callWithLastInput = false);
        void UnsubscribeFromInputEvent(InputType type, Action<float> action);
        void RaiseInputEvent(InputType type, float value);
        void RaiseLogicalInputEvent(InputType type, bool value);
    }
}