﻿using System;
using CargoCult.Template.Global.Preference;
using CargoCult.Template.Injectro.Core;
using UnityEngine;

namespace CargoCult.Template.Common.Input
{
    public class Input
    {
        public InputType Type;
        private Action<float> actions;
        private float lastValue;

        public Input(InputType type)
        {
            Type = type;
        }

        public void Raise(float value)
        {
            lastValue = value;

            if (actions != null)
            {
                actions(value);
            }
        }

        public void Subscribe(Action<float> action, bool callWithLastInput)
        {
            actions += action;

            if (callWithLastInput)
            {
                action(lastValue);
            }
        }

        public void Unsubscribe(Action<float> action)
        {
            if (action != null && actions != null)
            {
                actions -= action;
            }
        }

        public float GetLastValue()
        {
            return lastValue;
        }
    }

    [Singleton]
    [PostResolveDependency(typeof(IPreferenceManager))]
    public class InputManager : MonoBehaviour, IInputManager, IPostResolved
    {
        private Input[] inputs;

        private bool useStandaloneInputs;       

        public void PostResolved()
        {
            if (inputs == null || inputs.Length == 0)
                FillInputsArray();         
        }

        public bool GetStandaloneInputsStatus()
        {
            return useStandaloneInputs;
        }

        public void SetStandaloneInputsStatus(bool status)
        {
            useStandaloneInputs = status;
        }

        public void SubscribeToInputEvent(InputType type, Action<float> action, bool callWithLastInput = false)
        {
            if (inputs == null || inputs.Length == 0)
                FillInputsArray();

            inputs[(int)type].Subscribe(action, callWithLastInput);
        }

        public void UnsubscribeFromInputEvent(InputType type, Action<float> action)
        {
            inputs[(int)type].Unsubscribe(action);
        }

        public void RaiseInputEvent(InputType type, float value)
        {
            float result = Mathf.Clamp(value, -1, 1);
            Input input = inputs[(int)type];
            if (result != input.GetLastValue())
            {
                input.Raise(result);
            }
        }

        public void RaiseLogicalInputEvent(InputType type, bool value)
        {
            float result = value ? 1f : 0f;
            Input input = inputs[(int)type];
            if (result != input.GetLastValue())
            {
                input.Raise(result);
            }
        }

        private void FillInputsArray()
        {
            Array values = Enum.GetValues(typeof(InputType));

            InputType lastType = InputType.None;
            
            for (int i = 0; i < values.Length; i++)
            {
                InputType type = (InputType) values.GetValue(i);
                if (type > lastType)
                {
                    lastType = type;
                }
            }
            
            inputs = new Input[(int)lastType + 1];
            
            for (int i = 0; i < values.Length; i++)
            {
                InputType type = (InputType) values.GetValue(i);
                
                if (type == InputType.None)
                {
                    continue;
                }
                
                inputs[(int)type] = new Input(type);
            }
        }
    }
}