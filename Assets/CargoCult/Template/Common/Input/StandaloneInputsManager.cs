﻿using System;
using System.Collections.Generic;
using CargoCult.Template.Injectro.Core;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace CargoCult.Template.Common.Input
{
    public class StandaloneInputsManager : MonoBehaviour, IPostResolved
    {
        private const string Key = "UseStandaloneInputs";

        private static readonly List<InputType> AxisInputs = new List<InputType>
        {
            InputType.Horizontal,
            InputType.Vertical,
        };

        private static StandaloneInputsManager instance;

        public float Sensivity;

        [Inject] private IInputManager inputManager;

        private readonly Dictionary<InputType, float> lastValues = new Dictionary<InputType, float>();

#if UNITY_EDITOR
        [MenuItem("Editor Tools/Inputs Manager/Switch inputs &i")]
        public static void SwitchInputs()
        {
            bool savedUseStandaloneInputs = EditorPrefs.GetBool(Key, false);
            savedUseStandaloneInputs = !savedUseStandaloneInputs;
            EditorPrefs.SetBool(Key, savedUseStandaloneInputs);
            string message = string.Format("Now you use {0} inputs.",
                savedUseStandaloneInputs ? "Standalone" : "Mobile");
            EditorUtility.DisplayDialog("Change inputs.", message, "Ok");

            if (instance != null)
                instance.ChangeUseStandaloneInputsStatus(savedUseStandaloneInputs);
        }
#endif

        public void PostResolved()
        {
            bool isStandaloneInputs = false;
            
#if UNITY_EDITOR
            instance = this;
            isStandaloneInputs = EditorPrefs.GetBool(Key, false);
#endif
            
#if UNITY_STANDALONE_WIN
            isStandaloneInputs = true;
#endif
            
            ChangeUseStandaloneInputsStatus(isStandaloneInputs);
            FillDictionary();

        }

        public void ChangeUseStandaloneInputsStatus(bool status)
        {
            inputManager.SetStandaloneInputsStatus(status);
        }

#if UNITY_EDITOR || UNITY_STANDALONE_WIN
        private void Update()
        {
            if (!inputManager.GetStandaloneInputsStatus())
                return;

            foreach (InputType inputType in Enum.GetValues(typeof(InputType)))
            {
                if (inputType == InputType.None)
                {
                    continue;
                }
                
                float inputValue = UnityEngine.Input.GetAxis(inputType.ToString());
                inputValue = Mathf.Clamp(inputValue, -1, 1);
                if (AxisInputs.Contains(inputType))
                {
                    inputValue = Mathf.Lerp(lastValues[inputType], inputValue, 0.1f);
                    inputManager.RaiseInputEvent(inputType, inputValue * Sensivity);
                }
                else
                {
                    inputManager.RaiseInputEvent(inputType, inputValue);
                }

                lastValues[inputType] = inputValue;
            }
        }
#endif

        private void FillDictionary()
        {
            foreach (InputType value in Enum.GetValues(typeof(InputType)))
            {
                lastValues.Add(value, 0);
            }
        }
    }
}