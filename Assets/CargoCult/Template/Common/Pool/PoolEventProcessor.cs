﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace CargoCult.Template.Common.Pool
{
    public class PoolEventProcessor
    {
        private readonly IDictionary<GameObject, List<PoolEvent>> beforeReturnToPoolEvent = new Dictionary<GameObject, List<PoolEvent>>();
        
        public delegate void PoolEvent(GameObject poolingObject);

        public void AddBeforeReturnToPoolEvent(GameObject gameObject, PoolEvent poolEvent)
        {
            List<PoolEvent> events;
            if (!beforeReturnToPoolEvent.ContainsKey(gameObject))
            {
                events = new List<PoolEvent>();                
                beforeReturnToPoolEvent.Add(gameObject, events);
            }
            else
            {
                events = beforeReturnToPoolEvent[gameObject];
            }

            if (!events.Contains(poolEvent))
            {
                events.Add(poolEvent);
            }
        }

        public void InvokeBeforeReturnToPoolEvents(GameObject gameObject)
        {
            List<PoolEvent> events;
            beforeReturnToPoolEvent.TryGetValue(gameObject, out events);
            if (events != null && events.Count > 0)
            {
                for (int i = 0; i < events.Count; i++)
                {
                    PoolEvent poolEvent = events[events.Count - 1 - i];
                    if (poolEvent != null)
                    {
                        try
                        {
                            poolEvent.Invoke(gameObject);
                        }
                        catch (Exception e)
                        {
                            Debug.LogError(e.Message + "\n" + e.StackTrace);
                        }
                    }
                        
                }
                events.Clear();
            }
        }

    }
}