﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
#if UNITY_EDITOR
#endif

namespace CargoCult.Template.Common.Pool
{
    [CreateAssetMenu(menuName = "Pool/PreinitConfig")]
    public class PreinitConfig : ScriptableObject
    {
        public List<PreinitInfo> PreinitList;

#if UNITY_EDITOR
        public Dictionary<GameObject, PoolCounter> PoolCounters = new Dictionary<GameObject, PoolCounter>();
        public bool IgnoreFixWarning;

        public void ReturnHandler(GameObject prefab)
        {
            if (PoolCounters.ContainsKey(prefab))
            {
                PoolCounters[prefab].CurrentCount--;
            }
        }

        public void GetHandler(GameObject prefab)
        {
            if (PrefabUtility.IsPartOfRegularPrefab(prefab))
            {
                PoolCounter counter = null;
                if (!PoolCounters.TryGetValue(prefab, out counter))
                {
                    counter = new PoolCounter();
                    PoolCounters.Add(prefab, counter);
                }

                counter.CurrentCount++;
            }
        }

        private void OnEnable()
        {
            EditorApplication.playModeStateChanged += PlayModeStateHandler;
        }

        private void OnDisable()
        {
            EditorApplication.playModeStateChanged -= PlayModeStateHandler;
        }

        private void PlayModeStateHandler(PlayModeStateChange state)
        {
            if (state == PlayModeStateChange.EnteredEditMode && !IgnoreFixWarning)
            {
                CheckPreinit();
            }
        }

        private void CheckPreinit()
        {
            bool badPreinit = false;
            foreach (KeyValuePair<GameObject, PoolCounter> kvp in PoolCounters)
            {
                if (!PreinitList.Exists(info => info.Prefab == kvp.Key))
                {
                    badPreinit = true;
                }

                for (int i = 0; i < PreinitList.Count; i++)
                {
                    if (kvp.Key != PreinitList[i].Prefab || PreinitList[i].Count >= kvp.Value.MaxCount)
                    {
                        continue;
                    }

                    badPreinit = true;
                }
            }

            if (badPreinit)
            {
                EditorUtility.DisplayDialog("Preinit config requires your attention!",
                    $"WTF. Config: {name} has some errore! Fixid it!",
                    "I fix it now, man");
            }
        }

        public class PoolCounter
        {
            private int _currentCount;

            public int CurrentCount
            {
                get { return _currentCount; }
                set
                {
                    _currentCount = value;
                    MaxCount = Mathf.Max(MaxCount, _currentCount);
                }
            }

            public int MaxCount;
        }
#endif
    }

    [Serializable]
    public class PreinitInfo
    {
        public GameObject Prefab;
        public int Count;
    }
}