﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace CargoCult.Template.Common.Pool.Editor
{
    [CustomEditor(typeof(PreinitConfig))]
    public class PreinitConfigEditor : UnityEditor.Editor
    {
        private const string Nolabel = "NoLabel";
        private List<PreinitInfo> curPrefabList;
        private string curTab;

        private GUIStyle boxStyle;

        public override void OnInspectorGUI()
        {
            if (boxStyle == null)
            {
                boxStyle = "HelpBox";
            }

            PreinitConfig config = (PreinitConfig) target;
            config.IgnoreFixWarning = EditorGUILayout.Toggle("Ignore fix error Notify", config.IgnoreFixWarning);
            Dictionary<string, List<PreinitInfo>> tabs = new Dictionary<string, List<PreinitInfo>>();
            List<PreinitInfo> emptyPrefabs = new List<PreinitInfo>();

            for (int i = 0; i < config.PreinitList.Count; i++)
            {
                if (null == config.PreinitList[i].Prefab)
                {
                    emptyPrefabs.Add(config.PreinitList[i]);
                    continue;
                }

                string[] labels = AssetDatabase.GetLabels(config.PreinitList[i].Prefab);

                if (labels.Length == 0)
                {
                    if (!tabs.ContainsKey(Nolabel))
                    {
                        tabs.Add(Nolabel, new List<PreinitInfo>());
                    }

                    tabs[Nolabel].Add(config.PreinitList[i]);
                }

                for (int j = 0; j < labels.Length; j++)
                {
                    if (!tabs.ContainsKey(labels[j]))
                    {
                        tabs.Add(labels[j], new List<PreinitInfo>());
                    }

                    tabs[labels[j]].Add(config.PreinitList[i]);
                }
            }

            if (!string.IsNullOrEmpty(curTab))
            {
                if (curTab == "All")
                {
                    curPrefabList = config.PreinitList;
                }
                else
                {
                    curPrefabList = tabs[curTab];
                }
            }

            foreach (KeyValuePair<string, List<PreinitInfo>> kvp in tabs)
            {
                if (GUILayout.Button(kvp.Key))
                {
                    curPrefabList = kvp.Value;
                    curTab = kvp.Key;
                }
            }

            if (GUILayout.Button("All"))
            {
                curPrefabList = config.PreinitList;
                curTab = "All";
            }

            if (null != curPrefabList)
            {
                int toDeleteIndex = -1;
                curPrefabList.Sort((o1, o2) => String.Compare(null != o1.Prefab ? o1.Prefab.name : string.Empty,
                    null != o2.Prefab ? o2.Prefab.name : string.Empty, StringComparison.Ordinal));
                for (int i = 0; i < curPrefabList.Count; i++)
                {
                    EditorGUILayout.BeginHorizontal();
                    curPrefabList[i].Prefab =
                        EditorGUILayout.ObjectField(curPrefabList[i].Prefab, typeof(GameObject), false) as GameObject;
                    curPrefabList[i].Count = EditorGUILayout.IntField(curPrefabList[i].Count, GUILayout.MaxWidth(50));
                    if (GUILayout.Button("X", GUILayout.Width(20)) && EditorUtility.DisplayDialog("Delete object from preinit list?", "Delete object from preinit list?", "Yes", "Nooo!"))
                    {
                        toDeleteIndex = i;
                    }

                    EditorGUILayout.EndHorizontal();
                }

                if (toDeleteIndex >= 0)
                {
                    config.PreinitList.Remove(curPrefabList[toDeleteIndex]);
                    curPrefabList.RemoveAt(toDeleteIndex);
                }
            }

            if (GUILayout.Button("AddNew"))
            {
                PreinitInfo info = new PreinitInfo();
                config.PreinitList.Add(info);
                emptyPrefabs.Add(info);
            }

            for (int i = 0; i < emptyPrefabs.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();
                emptyPrefabs[i].Prefab =
                    EditorGUILayout.ObjectField(emptyPrefabs[i].Prefab, typeof(GameObject), false) as GameObject;
                emptyPrefabs[i].Count = EditorGUILayout.IntField(emptyPrefabs[i].Count, GUILayout.MaxWidth(50));
                EditorGUILayout.EndHorizontal();
            }

            if (GUILayout.Button("Remove Empty"))
            {
                for (int i = config.PreinitList.Count - 1; i >= 0; i--)
                {
                    if (config.PreinitList[i] == null || config.PreinitList[i].Prefab == null)
                    {
                        config.PreinitList.RemoveAt(i);
                    }
                }
            }

            EditorGUILayout.BeginVertical();
            if (config.PoolCounters.Count > 0 && !Application.isPlaying)
            {
                foreach (KeyValuePair<GameObject, PreinitConfig.PoolCounter> kvp in config.PoolCounters)
                {
                    if (!config.PreinitList.Exists(info => info.Prefab == kvp.Key))
                    {
                        EditorGUILayout.BeginVertical(boxStyle);
                        using (new EditorGUI.DisabledScope(true))
                        {
                            EditorGUILayout.ObjectField(kvp.Key, typeof(GameObject), false);
                        }

                        EditorGUILayout.LabelField($"Need add to preinit count by {kvp.Value.MaxCount}");
                        if (GUILayout.Button("Fix"))
                        {
                            Undo.RecordObject(config, "Fix");
                            PreinitInfo info = new PreinitInfo()
                            {
                                Count = kvp.Value.MaxCount,
                                Prefab = kvp.Key
                            };
                            config.PreinitList.Add(info);
                        }

                        EditorGUILayout.EndVertical();
                    }

                    for (int i = 0; i < config.PreinitList.Count; i++)
                    {
                        if (kvp.Key != config.PreinitList[i].Prefab || config.PreinitList[i].Count >= kvp.Value.MaxCount)
                        {
                            continue;
                        }

                        int preinitDelta = kvp.Value.MaxCount - config.PreinitList[i].Count;

                        EditorGUILayout.BeginVertical(boxStyle);
                        using (new EditorGUI.DisabledScope(true))
                        {
                            EditorGUILayout.ObjectField(config.PreinitList[i].Prefab, typeof(GameObject), false);
                        }

                        EditorGUILayout.LabelField($"Need to increase preinit count by {preinitDelta}");
                        if (GUILayout.Button("Fix"))
                        {
                            Undo.RecordObject(config, "Fix");
                            config.PreinitList[i].Count += preinitDelta;
                        }

                        EditorGUILayout.EndVertical();
                    }
                }
            }

            EditorGUILayout.EndVertical();

            EditorUtility.SetDirty(target);

            serializedObject.ApplyModifiedProperties();
        }
    }
}