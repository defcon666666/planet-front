﻿using CargoCult.Template.Injectro.Core;
using UnityEngine;

namespace CargoCult.Template.Common.Pool
{
    public interface IPoolManager : IBean
    {
        void InitPoolingPrefab(GameObject prefab, int initialCount = 2);
        T GetFromPool<T>(T prefabComponent) where T : Component;
        T GetFromPool<T>(T prefabComponent, Vector3 position, Quaternion rotation) where T : Component;
        GameObject GetFromPool(GameObject prefab);
        GameObject GetFromPool(GameObject prefab, Vector3 position, Quaternion rotation);
        bool ReturnToPool(Component objComponent);
        bool ReturnToPool(GameObject obj);
        bool ReturnToPoolWithDelay(Component objComponent, float timeDelay);
        bool ReturnToPoolWithDelay(GameObject obj, float timeDelay);
        bool AddBeforeReturnEvent(GameObject obj, PoolEventProcessor.PoolEvent poolEvent);
        GameObject PrefabOf(GameObject obj);
    }
}
