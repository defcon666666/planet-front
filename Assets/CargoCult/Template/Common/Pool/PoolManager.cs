﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CargoCult.Template.Common.SlowUpdate;
using CargoCult.Template.Global.Quality;
using CargoCult.Template.Injectro.Core;
using UnityEngine;

namespace CargoCult.Template.Common.Pool
{
    [Singleton]
    public class PoolManager : MonoBehaviour, IPoolManager, IPostResolved
    {
        private const int SavePreinitCoef = 2; 
        
        [Inject] private IQualityManager qualityManager;
        
        private readonly IDictionary<string, List<GameObject>> storage = new Dictionary<string, List<GameObject>>();
        private readonly IDictionary<GameObject, string> itemsInUse = new Dictionary<GameObject, string>();
        private readonly IDictionary<string, PoolConfig> poolConfigsByName = new Dictionary<string, PoolConfig>();
        private readonly IDictionary<string, PoolFillRequest> fillRequests = new Dictionary<string, PoolFillRequest>();
        private readonly PoolEventProcessor eventProcessor = new PoolEventProcessor();

        [SerializeField] private Vector3 createPoint;
        [SerializeField] private PreinitConfig preinitConfig;
        
        private SlowUpdateProc autoFillUpdateProc;

        public void PostResolved()
        {
            autoFillUpdateProc = new SlowUpdateProc(AutoFillSlowUpdate, 0.6f);
            
            if (!preinitConfig)
            {
                return;
            }

            int qualitySettingsReduction = qualityManager.GetConfigsCount() >= 0 
                    ? qualityManager.GetConfigsCount() + 1
                    : SavePreinitCoef;
            int currentQualitySettings = qualityManager.GetCurrentConfigIndex();

            if (currentQualitySettings >= 0)
            {
                qualitySettingsReduction /= (currentQualitySettings + 1);
            }
            
            for (int i = 0; i < preinitConfig.PreinitList.Count; i++)
            {
                int reqCount = Math.Max(preinitConfig.PreinitList[i].Count / qualitySettingsReduction, 1);
                InitPoolingPrefab(preinitConfig.PreinitList[i].Prefab, reqCount);
            }
        }

        public void InitPoolingPrefab(GameObject prefab, int initialCount = 2)
        {
            if (!storage.ContainsKey(prefab.name) && initialCount > 0)
            {
                if (prefab.name.Contains("(Clone)"))
                    throw new Exception("Prefab based objects are only allowed");

                PoolConfig config = new PoolConfig
                {
                    Prefab = prefab,
                    InitialCount = initialCount
                };
                Component[] components = prefab.GetComponents<Component>();
                for (int i = 0; i < components.Length; i++)
                {
                    Component component = components[i];
                    if (component is IInitable)
                    {
                        config.InitableComponents.Add(component.GetType());
                    }
                }
                InitCustomConfig(config);
            }
        }

        public T GetFromPool<T>(T prefabComponent) where T : Component
        {
            return GetFromPool(prefabComponent.gameObject, createPoint, Quaternion.identity).GetComponent<T>();
        }

        public T GetFromPool<T>(T prefabComponent, Vector3 position, Quaternion rotation) where T : Component
        {
            return GetFromPool(prefabComponent.gameObject, position, rotation).GetComponent<T>();
        }

        public GameObject GetFromPool(GameObject prefab)
        {
            return GetFromPool(prefab, createPoint, Quaternion.identity);
        }

        public GameObject GetFromPool(GameObject prefab, Vector3 position, Quaternion rotation)
        {
#if UNITY_EDITOR
            preinitConfig?.GetHandler(prefab);
#endif
            
            InitPoolingPrefab(prefab);
            return GetFromPoolNotInitable(prefab.name, position, rotation);
        }

        public bool ReturnToPool(Component objComponent)
        {
            return ReturnToPool(objComponent.gameObject);
        }

        public bool ReturnToPool(GameObject obj)
        {
            if (obj != null && itemsInUse.ContainsKey(obj))
            {
                
                eventProcessor.InvokeBeforeReturnToPoolEvents(obj);
                obj.SetActive(false);
                string poolName = itemsInUse[obj];
                PoolConfig config = poolConfigsByName[poolName];
#if UNITY_EDITOR
                preinitConfig?.ReturnHandler(config.Prefab);
#endif
                config.ItemsInUse -= 1;

                foreach (Type type in config.InitableComponents)
                {
                    Component[] initableComponents = obj.GetComponents(type);
                    if (initableComponents == null)
                        continue;

                    for (int i = 0; i < initableComponents.Length; i++)
                    {
                        Component component = initableComponents[i];
                        IInitable initable = component as IInitable;
                        if (initable != null)
                        {
                            initable.DeInit();
                        }
                    }
                }
                itemsInUse.Remove(obj);
                storage[poolName].Add(obj);
                RectTransform rect = obj.transform as RectTransform;
                if (rect != null)
                    rect.SetParent(config.Wrapper.transform, false);
                else
                {
                    obj.transform.parent = config.Wrapper.transform;
                }
                obj.transform.position = createPoint;
                return true;
            }

            if (obj != null && obj.transform.IsChildOf(transform))
                return true;

            return false;
        }

        public bool ReturnToPoolWithDelay(Component objComponent, float timeDelay)
        {
            return ReturnToPoolWithDelay(objComponent.gameObject, timeDelay);
        }

        public bool ReturnToPoolWithDelay(GameObject obj, float timeDelay)
        {
            if (itemsInUse.ContainsKey(obj))
            {
                StartCoroutine(ReturnToPoolCoroutine(obj, timeDelay));
                return true;
            }
            return false;
        }

        public bool AddBeforeReturnEvent(GameObject obj, PoolEventProcessor.PoolEvent poolEvent)
        {
            if (obj == null)
                return false;

            if (itemsInUse.ContainsKey(obj))
            {
                eventProcessor.AddBeforeReturnToPoolEvent(obj, poolEvent);
                return true;
            }
            return false;
        }

        public GameObject PrefabOf(GameObject obj)
        {
            if (obj != null && itemsInUse.ContainsKey(obj))
            {
                string poolName = itemsInUse[obj];
                PoolConfig config = poolConfigsByName[poolName];
                return config.Prefab;
            }

            return null;
        }

        private void Update()
        {
            if (autoFillUpdateProc != null)
                autoFillUpdateProc.ProceedOnFixedUpdate();
        }

        private GameObject GetFromPoolNotInitable(string poolName, Vector3 position, Quaternion rotation)
        {
            if (storage.ContainsKey(poolName))
            {
                List<GameObject> vacantObjects = storage[poolName];
                PoolConfig config = poolConfigsByName[poolName];

                if (vacantObjects.Count == 0)
                {
                    PoolFillRequest request = new PoolFillRequest
                    {
                        Config = config,
                        VacantObjects = vacantObjects,
                        ToFillCount = 1
                    };
                    CompleteRequest(request);
                }

                if (vacantObjects.Count > 0)
                {
                    GameObject vacantObject = vacantObjects[vacantObjects.Count - 1];
                    RectTransform rect = vacantObject.transform as RectTransform;
                    if (rect != null)
                        rect.SetParent(null, false);
                    else
                        vacantObject.transform.parent = null;
                    vacantObject.transform.position = position;
                    vacantObject.transform.rotation = rotation;
                    itemsInUse.Add(vacantObject, poolName);
                    vacantObjects.Remove(vacantObject);

                    if (vacantObjects.Count == 0)
                    {
                        if (!fillRequests.ContainsKey(poolName))
                        {
                            PoolFillRequest request = new PoolFillRequest
                            {
                                Config = config,
                                VacantObjects = vacantObjects,
                                ToFillCount = 3
                            };
                            fillRequests.Add(poolName, request);
                        }
                    }

                    config.ItemsInUse += 1;
                    vacantObject.SetActive(true);
                    for (int i = 0; i < config.InitableComponents.Count; i++)
                    {
                        Type type = config.InitableComponents[i];
                        Component[] initableComponents = vacantObject.GetComponents(type);
                        if (initableComponents == null)
                            continue;

                        for (int j = 0; j < initableComponents.Length; j++)
                        {
                            Component component = initableComponents[j];
                            IInitable initable = component as IInitable;
                            if (initable != null)
                            {
                                initable.Init();
                            }
                        }
                    }
                    return vacantObject;
                }
            }

            throw new Exception("No such pool named " + poolName + " and quality " + qualityManager.GetConfigsCount());
        }

        private IEnumerator ReturnToPoolCoroutine(GameObject obj, float timeDelay)
        {
            yield return new WaitForSeconds(timeDelay);
            ReturnToPool(obj);
        }

        private void InitCustomConfig(PoolConfig config)
        {
            string poolName = config.Prefab.name;
            GameObject wrapper = new GameObject(poolName + "_Pool");
            config.Wrapper = wrapper;
            RectTransform rect = wrapper.transform as RectTransform;
            if (rect != null)
                rect.SetParent(transform, false);
            else
                wrapper.transform.parent = transform;
            wrapper.transform.position = createPoint;

            List<GameObject> list = new List<GameObject>(config.InitialCount);
            storage.Add(poolName, list);
            for (int i = 0; i < config.InitialCount; i++)
            {
                var o = InitObject(config);
                list.Add(o);
            }

            poolConfigsByName.Add(poolName, config);
        }

        private GameObject InitObject(PoolConfig config)
        {
            GameObject o = Instantiate(config.Prefab);
            RectTransform rect = o.transform as RectTransform;
            if (rect != null)
                rect.SetParent(config.Wrapper.transform, false);
            else
                o.transform.parent = config.Wrapper.transform;
            o.transform.position = createPoint;
            o.SetActive(false);
            return o;
        }

        private int CompleteRequest(PoolFillRequest request)
        {
            List<GameObject> vacantObjects = request.VacantObjects;
            PoolConfig config = request.Config;

            if (config.ItemsInUse < config.MaximumCount)
            {
                int newCount = Mathf.Min(config.MaximumCount, vacantObjects.Count + config.ItemsInUse + 1);
                int spawnedCount = 0;
                for (int i = 0; i < newCount - (config.ItemsInUse + vacantObjects.Count); i++)
                {
                    GameObject o = InitObject(config);
                    vacantObjects.Add(o);
                    spawnedCount++;
                }
                return spawnedCount;
            }
            
            Debug.LogWarning(String.Format("PoolManager: pool, named {0} exceed maximum object count {1}",
                config.Prefab.name, config.MaximumCount));
            
            return 0;
        }

        private void AutoFillSlowUpdate()
        {
            if (fillRequests.Count == 0)
                return;

            KeyValuePair<string, PoolFillRequest> pair = fillRequests.First();
            
            PoolFillRequest request = pair.Value;
            int spawnedCount = CompleteRequest(request);
            request.ToFillCount -= spawnedCount;
            if (request.ToFillCount <= 0)
            {
                fillRequests.Remove(pair.Key);
            }
        }

        [Serializable]
        private class PoolConfig
        {
            public GameObject Prefab;
            public int InitialCount = 2;
            public int MaximumCount = 100;
            public readonly List<Type> InitableComponents = new List<Type>();
            private GameObject wrapper;
            private int itemsInUse;

            public GameObject Wrapper
            {
                get { return wrapper; }
                set { wrapper = value; }
            }

            public int ItemsInUse
            {
                get { return itemsInUse; }
                set { itemsInUse = value; }
            }
        }

        private class PoolFillRequest
        {
            public List<GameObject> VacantObjects;
            public PoolConfig Config;
            public int ToFillCount;
        }
    }
}