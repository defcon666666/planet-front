﻿namespace CargoCult.Template.Common.Pool
{
    public interface IInitable
    {
        void Init();
        void DeInit();
    }
}