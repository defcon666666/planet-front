﻿using System;
using System.Reflection;
using UnityEditor;

namespace CargoCult.Template.Common.Editor
{
    public static class GameViewUtils
    {
        private static readonly object GameViewSizesInstance;
        private static readonly MethodInfo CurrentGroup;

        static GameViewUtils()
        {
            Type sizesType = typeof(UnityEditor.Editor).Assembly.GetType("UnityEditor.GameViewSizes");
            Type singleType = typeof(ScriptableSingleton<>).MakeGenericType(sizesType);
            PropertyInfo instanceProp = singleType.GetProperty("instance");
            CurrentGroup = sizesType.GetMethod("GetGroup");
            if (instanceProp != null)
            {
                GameViewSizesInstance = instanceProp.GetValue(null, null);                
            }
        }

        public enum GameViewSizeType
        {
            AspectRatio,
            FixedResolution
        }
        
        public static void SetSize(int index)
        {
            Type gvWndType = typeof(UnityEditor.Editor).Assembly.GetType("UnityEditor.GameView");
            PropertyInfo selectedSizeIndexProp = gvWndType.GetProperty("selectedSizeIndex", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            EditorWindow gvWnd = EditorWindow.GetWindow(gvWndType);
            if (selectedSizeIndexProp != null)
            {
                selectedSizeIndexProp.SetValue(gvWnd, index, null);
            }
        }

        public static void AddCustomSize(GameViewSizeType viewSizeType, GameViewSizeGroupType sizeGroupType, int width, int height, string text)
        {
            object group = GetGroup(sizeGroupType);
            MethodInfo addCustomSize = CurrentGroup.ReturnType.GetMethod("AddCustomSize");
            Type gvsType = typeof(UnityEditor.Editor).Assembly.GetType("UnityEditor.GameViewSize");
            ConstructorInfo constructor = gvsType.GetConstructor(new[] {typeof(UnityEditor.Editor).Assembly.GetType("UnityEditor.GameViewSizeType"), typeof(int), typeof(int), typeof(string)});
            if (constructor != null)
            {
                object newSize = constructor.Invoke(new object[] {(int) viewSizeType, width, height, text});
                if (addCustomSize != null) 
                {
                    addCustomSize.Invoke(group, new[] {newSize});
                }
            }
        }

        public static int GetCurrentSizeIndex()
        {
            Type gvWndType = typeof(UnityEditor.Editor).Assembly.GetType("UnityEditor.GameView");
            PropertyInfo selectedSizeIndexProp = gvWndType.GetProperty("selectedSizeIndex", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            EditorWindow gvWnd = EditorWindow.GetWindow(gvWndType);
            if (selectedSizeIndexProp != null)
            {
                return (int) selectedSizeIndexProp.GetValue(gvWnd);
            }
            else
            {
                return 0;
            }
        }

        public static bool SizeExists(GameViewSizeGroupType sizeGroupType, string text)
        {
            return FindSize(sizeGroupType, text) != -1;
        }

        public static int FindSize(GameViewSizeGroupType sizeGroupType, string text)
        {
            object group = GetGroup(sizeGroupType);
            MethodInfo getDisplayTexts = group.GetType().GetMethod("GetDisplayTexts");
            if (getDisplayTexts != null)
            {
                string[] displayTexts = getDisplayTexts.Invoke(group, null) as string[];
                if (displayTexts != null)
                {
                    for (int i = 0; i < displayTexts.Length; i++)
                    {
                        string display = displayTexts[i];
                        int pren = display.IndexOf('(');
                        if (pren != -1)
                            display = display.Substring(0,
                                pren - 1);
                        if (display == text)
                            return i;
                    }
                }
            }

            return -1;
        }

        public static bool SizeExists(GameViewSizeGroupType sizeGroupType, int width, int height)
        {
            return FindSize(sizeGroupType, width, height) != -1;
        }

        public static int FindSize(GameViewSizeGroupType sizeGroupType, int width, int height)
        {
            object group = GetGroup(sizeGroupType);
            Type groupType = group.GetType();
            MethodInfo getBuiltinCount = groupType.GetMethod("GetBuiltinCount");
            MethodInfo getCustomCount = groupType.GetMethod("GetCustomCount");
            if (getBuiltinCount != null && getCustomCount != null)
            {
                int sizesCount = (int) getBuiltinCount.Invoke(group, null) + (int) getCustomCount.Invoke(group, null);
                MethodInfo getGameViewSize = groupType.GetMethod("GetGameViewSize");
                if (getGameViewSize != null)
                {
                    Type gvsType = getGameViewSize.ReturnType;
                    PropertyInfo widthProp = gvsType.GetProperty("width");
                    PropertyInfo heightProp = gvsType.GetProperty("height");
                    object[] indexValue = new object[1];
                    for (int i = 0; i < sizesCount; i++)
                    {
                        indexValue[0] = i;
                        object size = getGameViewSize.Invoke(group, indexValue);
                        if (widthProp != null && heightProp != null)
                        {
                            int sizeWidth = (int) widthProp.GetValue(size, null);
                            int sizeHeight = (int) heightProp.GetValue(size, null);
                            if (sizeWidth == width && sizeHeight == height)
                            {
                                return i;
                            }
                        }
                    }
                }
            }

            return -1;
        }

        static object GetGroup(GameViewSizeGroupType type)
        {
            return CurrentGroup.Invoke(GameViewSizesInstance, new object[] {(int) type});
        }
        
        public static GameViewSizeGroupType GetCurrentGroupType()
        {
            var getCurrentGroupTypeProp = GameViewSizesInstance.GetType().GetProperty("currentGroupType");
            if (getCurrentGroupTypeProp != null)
            {
                return (GameViewSizeGroupType) (int) getCurrentGroupTypeProp.GetValue(GameViewSizesInstance, null);
            }
            else
            {
                return GameViewSizeGroupType.Standalone;
            }
        }
    }
    
}