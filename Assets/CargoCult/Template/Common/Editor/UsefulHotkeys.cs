﻿using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;

namespace CargoCult.Template.Common.Editor
{
    [InitializeOnLoad]
    public static class UsefulHotkeys
    {
        private static readonly Tuple<int, int>[] ScreenshotAdditionalResolutions = 
        {
            new Tuple<int, int>(1280, 800),
            new Tuple<int, int>(2208, 1242),
            new Tuple<int, int>(2732, 2048),
        };
        
        static UsefulHotkeys()
        {
            FieldInfo info = typeof(EditorApplication).GetField("globalEventHandler", BindingFlags.Static | BindingFlags.NonPublic);
            if (info != null)
            {
                EditorApplication.CallbackFunction value = (EditorApplication.CallbackFunction) info.GetValue(null);
                value += KeyHandler;
                info.SetValue(null, value);
            }
        }
        
        private static void KeyHandler()
        {
            Event currentEvent = Event.current;

            if (!currentEvent.isKey || currentEvent.type != EventType.KeyDown)
            {
                return;
            }

            switch (currentEvent.keyCode)
            {
                case KeyCode.Keypad0:
                {
                    CreateScreenshot();
                    break;
                }
                
                case KeyCode.KeypadMinus:
                {
                    OpenScreenshotsFolder();
                    break;
                }

                case KeyCode.KeypadPeriod:
                {
                    SwitchPause();
                    break;
                }
            }
        }

        [MenuItem("Editor Tools/Hotkeys/Create Screenshot _NUMPAD0")]
        private static void CreateScreenshot()
        {
            GameObject coroutineRunner = new GameObject();
            coroutineRunner.AddComponent<Image>().StartCoroutine(ScreenshotProcess(coroutineRunner));
        }

        [MenuItem("Editor Tools/Hotkeys/Open Screenshots Folder _NUMPADMINUS")]
        private static void OpenScreenshotsFolder()
        {
            string folderPath = Application.persistentDataPath + "/Screenshots/";
            
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            
            Process.Start(folderPath);
        }

        [MenuItem("Editor Tools/Hotkeys/Pause _NUMPADDOT")]
        private static void SwitchPause()
        {
            EditorApplication.isPaused = !EditorApplication.isPaused;
        }
        
        [MenuItem("Editor Tools/Hotkeys/Select Player _NUMPAD1", true)]
        private static bool SelectPlayerValidate()
        {
            return EditorApplication.isPlaying;
        }

        private static IEnumerator ScreenshotProcess(GameObject coroutineRunner)
        {
            float timeScale = Time.timeScale;
            Time.timeScale = 0;
            
            yield return new WaitForEndOfFrame();
            
            string folderPath = Application.persistentDataPath + "/Screenshots/";

            bool needOrignalScreenshot = true;

            foreach (Tuple<int,int> additionalResolution in ScreenshotAdditionalResolutions)
            {
                if (additionalResolution.Item1 == Screen.width && additionalResolution.Item2 == Screen.height)
                {
                    needOrignalScreenshot = false;
                    break;
                }
            }

            if (needOrignalScreenshot)
            {
                string resolutionName = Screen.width + "x" + Screen.height;
                string resolutionFolderPath = folderPath + resolutionName + "/";

                PerformScreenshot(resolutionFolderPath);
            }

            for (int i = 0; i < ScreenshotAdditionalResolutions.Length; i++)
            {
                Tuple<int, int> resolution = ScreenshotAdditionalResolutions[i];
                string resolutionName = resolution.Item1 + "x" + resolution.Item2;
                string resolutionFolderPath = folderPath + resolutionName + "/";

                yield return PerformScreenshot(resolutionFolderPath, resolution.Item1, resolution.Item2);
            }

            if (coroutineRunner != null)
            {
                Object.DestroyImmediate(coroutineRunner);
            }

            Time.timeScale = timeScale;
        }

        private static void PerformScreenshot(string folderPath)
        {
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }

            int filesInFolder = Directory.GetFiles(folderPath).Length;

            string date = DateTime.Now.ToString("dd_MM_yyyy_HH_mm");
            
            string fileName = Application.productName + "_" + date + "_(" + filesInFolder + ")";
            fileName = fileName.Replace(" ", "_");
            fileName = fileName.Replace(".", "_");
            fileName = fileName.Replace(":", "_");
            fileName += ".jpg";

            Texture2D texture = ScreenCapture.CaptureScreenshotAsTexture();
            byte[] bytes = texture.EncodeToJPG();

            string path = folderPath + fileName;

            try
            {
                File.WriteAllBytes(path, bytes);
                Debug.Log($"Create Screenshot with name '{fileName}' saved at path '{folderPath}'");
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                Debug.Log($"Impossible to create screenshot with name '{fileName}' saved at path '{folderPath}'");
            }
            finally
            {
                if (texture != null)
                {
                    Object.DestroyImmediate(texture);
                }
            }
        }
        
        private static IEnumerator PerformScreenshot(string folderPath, int width, int height)
        {
            GameViewSizeGroupType platform = GameViewUtils.GetCurrentGroupType();

            int currentSizeIndex = GameViewUtils.GetCurrentSizeIndex();
            
            string resolutionName = width + "x" + height;
            
            if (!GameViewUtils.SizeExists(platform, width, height))
            {
                GameViewUtils.AddCustomSize(GameViewUtils.GameViewSizeType.FixedResolution, platform, width, height, resolutionName);
            }
            
            GameViewUtils.SetSize(GameViewUtils.FindSize(platform, width, height));
            
            yield return new WaitForEndOfFrame();
            
            PerformScreenshot(folderPath);
            
            GameViewUtils.SetSize(currentSizeIndex);
            
            yield return new WaitForEndOfFrame();
        }
    }
}
