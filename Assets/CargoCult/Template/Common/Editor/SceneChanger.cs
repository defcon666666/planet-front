﻿using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CargoCult.Template.Common.Editor
{
    /// <summary>
    /// SceneSwitchWindow class.
    /// </summary>
    public class SceneChanger : EditorWindow
    {
        /// <summary>
        /// Tracks scroll position.
        /// </summary>
        private Vector2 scrollPos;

        /// <summary>
        /// Initialize window state.
        /// </summary>
        [MenuItem("Editor Tools/Scene Switch Window")]
        private static void Init()
        {
            // EditorWindow.GetWindow() will return the open instance of the specified window or create a new
            // instance if it can't find one. The second parameter is a flag for creating the window as a
            // Utility window; Utility windows cannot be docked like the Scene and Game view windows.
            SceneChanger window = (SceneChanger)GetWindow(typeof(SceneChanger), false, "Scene Switch Window");
            window.position = new Rect(window.position.xMin + 100f, window.position.yMin + 100f, 200f, 400f);
        }

        /// <summary>
        /// Called on GUI events.
        /// </summary>
        private void OnGUI()
        {
            EditorGUILayout.BeginVertical();
            scrollPos = EditorGUILayout.BeginScrollView(scrollPos, false, false);

            GUILayout.Label("Available scenes", EditorStyles.boldLabel);

            List<string> scenes = new List<string>();
            int sceneCountInBuildSettings = EditorSceneManager.sceneCountInBuildSettings;
            for (int i = 0; i < sceneCountInBuildSettings; i++)
            {
                string scenePathByBuildIndex = SceneUtility.GetScenePathByBuildIndex(i);
                scenes.Add(scenePathByBuildIndex);
            }

            for (int i = 0; i < scenes.Count; i++)
            {
                string scene = scenes[i];
                {
                    string sceneName = Path.GetFileNameWithoutExtension(scene);
                    bool pressed = GUILayout.Button(i + ": " + sceneName, new GUIStyle(GUI.skin.GetStyle("Button")) { alignment = TextAnchor.MiddleLeft });
                    if (pressed)
                    {
                        if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
                        {
                            EditorSceneManager.OpenScene(scene);
                        }
                    }
                }
            }

            EditorGUILayout.EndScrollView();
            EditorGUILayout.EndVertical();
        }
    }
}
