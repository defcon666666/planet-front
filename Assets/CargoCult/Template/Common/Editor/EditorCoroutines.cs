﻿using System.Collections;
using UnityEditor;

namespace CargoCult.Template.Common.Editor
{
    public class EditorCoroutine
    {
        private readonly IEnumerator coroutine;
        
        EditorCoroutine(IEnumerator routine)
        {
            coroutine = routine;
        }
        
        public static EditorCoroutine StartCoroutine(IEnumerator routine)
        {
            EditorCoroutine coroutine = new EditorCoroutine(routine);
            coroutine.Start();

            return coroutine;
        }
        
        public void Stop()
        {
            if (EditorApplication.update != null)
                EditorApplication.update -= EditorUpdate;
        }
        
        private void Start()
        {
            EditorApplication.update += EditorUpdate;
        }
        
        private void EditorUpdate()
        {
            if (coroutine.MoveNext() == false)
                Stop();
        }
    }
}
