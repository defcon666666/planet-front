﻿using System;
using System.Collections.Generic;
using CargoCult.Template.Global.LoadScene;
using UnityEngine;

namespace CargoCult.Template.Common.EventController
{
    public enum EventContextType
    {
        Scene,
        Global,
    }
    
    public abstract class BaseEvent
    {
    }
    
    public interface IEventController <T, TS> where T : BaseEvent where TS : struct
    {
        void AddListener<TA>(Action<TS> action, EventContextType eventContextType = EventContextType.Scene) where TA : T;
        void RemoveListener<TR>(Action<TS> action, EventContextType eventContextType = EventContextType.Scene) where TR : T;
    }

    public class EventController<T, TS> : IEventController<T, TS> where T: BaseEvent where TS: struct 
    {
        private readonly ILoadSceneManager loadSceneManager;
        
        private readonly Dictionary<Type, Dictionary<EventContextType, List<Action<TS>>>> allEventsDictionary =
            new Dictionary<Type, Dictionary<EventContextType, List<Action<TS>>>>();

        private readonly EventContextType[] eventContextTypes;
        
        public EventController(ILoadSceneManager loadSceneManager, bool autoUnloadSceneEvents = true)
        {
            this.loadSceneManager = loadSceneManager;
            
            if (autoUnloadSceneEvents)
            {
                SubscribeOnSceneUnload();
            }

            Array values = Enum.GetValues(typeof(EventContextType));
            eventContextTypes = (EventContextType[]) values;
        }
        
        public void SubscribeOnSceneUnload()
        {
            loadSceneManager.AddListener<UnloadSceneEvent>(OnSceneUnload, EventContextType.Global);
        }
        
        public void AddListener<TA>(Action<TS> action, EventContextType eventContextType = EventContextType.Scene) where TA : T
        {
            List<Action<TS>> eventList = GetEventList(typeof(TA), eventContextType);
            
            if (eventList.Contains(action))
            {
                Debug.LogWarning("Multiple subscribe!");
                return;
            }

            eventList.Add(action);
        }

        public void RemoveListener<TR>(Action<TS> action, EventContextType eventContextType = EventContextType.Scene) where TR : T
        {
            List<Action<TS>> eventList = GetEventList(typeof(TR), eventContextType);

            if (!eventList.Contains(action))
            {
                Debug.LogWarning("Remove unhandled action!");
                return;
            }

            eventList.Remove(action);
        }

        public void RaiseEvent<TR>(TS eventParametrs) where TR : T
        {
            Type eventTypeToRaise = typeof(TR);
            
            Dictionary<EventContextType, List<Action<TS>>> contextTypeEvents;
            if (!allEventsDictionary.TryGetValue(eventTypeToRaise, out contextTypeEvents))
            {
                return;
            }

            for (int i = 0; i < eventContextTypes.Length; i++)
            {
                EventContextType eventContextType = eventContextTypes[i];
                List<Action<TS>> eventsList;
                if (!contextTypeEvents.TryGetValue(eventContextType, out eventsList))
                {
                    continue;
                }
                
                for (int j = eventsList.Count - 1; j >= 0; j--)
                {
                    j = Mathf.Clamp(j, 0, eventsList.Count - 1);
                    
                    Action<TS> action = eventsList[j];
                    if (action != null)
                    {
                        action(eventParametrs);
                    }
                }
            }
        }
        
        private List<Action<TS>> GetEventList(Type eventType, EventContextType eventContextType)
        {
            Dictionary<EventContextType, List<Action<TS>>> contextTypeEvents;
            if (!allEventsDictionary.TryGetValue(eventType, out contextTypeEvents))
            {
                contextTypeEvents = new Dictionary<EventContextType, List<Action<TS>>>();
                allEventsDictionary.Add(eventType, contextTypeEvents);
            }

            List<Action<TS>> eventList;
            if (!contextTypeEvents.TryGetValue(eventContextType, out eventList))
            {
                eventList = new List<Action<TS>>();
                contextTypeEvents.Add(eventContextType, eventList);
            }
            return eventList;
        }

        private void OnSceneUnload(SceneEventParametrs eventParametrs)
        {
            foreach (KeyValuePair<Type,Dictionary<EventContextType,List<Action<TS>>>> valuePair in allEventsDictionary)
            {
                List<Action<TS>> eventsList;
                if (!valuePair.Value.TryGetValue(EventContextType.Scene, out eventsList))
                    continue;
                
                eventsList.Clear();
            }
        }
    }
}