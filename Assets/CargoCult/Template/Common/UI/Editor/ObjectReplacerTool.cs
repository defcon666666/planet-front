﻿using System;
using System.Collections;
using System.Collections.Generic;
using CargoCult.Template.Common.Editor;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace CargoCult.Template.Common.UI.Editor
{
    public class ObjectReplacerTool : EditorWindow
    {
        private readonly List<ObjectReplacePair> objectReplacePairs = new List<ObjectReplacePair>
        {
            new ObjectReplacePair()
        };
        private float progress;
        private EditorCoroutine processCoroutine;
        private Vector2 scrollPos;

        [MenuItem("Editor Tools/Object Replacer")]
        public static void ShowWindow()
        {
            ObjectReplacerTool window = GetWindow<ObjectReplacerTool>();
            window.Show();
        }

        private void OnGUI()
        {
            scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
            EditorGUILayout.BeginHorizontal();
            {
                for (int i = 0; i < objectReplacePairs.Count; i++)
                {
                    ObjectReplacePair pair = objectReplacePairs[i];
                                       
                    EditorGUILayout.BeginVertical(GUILayout.Width(200), GUILayout.MaxHeight(float.MaxValue));
                    {
                        EditorGUILayout.LabelField("Target object");
                        pair.TargetObject = EditorGUILayout.ObjectField("", pair.TargetObject, typeof(Object), false);
                        
                        EditorGUILayout.LabelField("New object");
                        pair.NewObject = EditorGUILayout.ObjectField("", pair.NewObject, typeof(Object), false);

                        EditorGUILayout.LabelField("Use of:");
                        
                        pair.ScrollPos = EditorGUILayout.BeginScrollView(pair.ScrollPos);
                        {
                            foreach (ObjectUsage usage in pair.Usages)
                            {
                                if (usage.UsingObject != null)
                                {
                                    EditorGUILayout.ObjectField(usage.UsingObject, typeof(Object), true);
                                }

                                if (!string.IsNullOrEmpty(usage.SceneObjectUsing))
                                {
                                    EditorGUILayout.TextField(usage.SceneObjectUsing);
                                }
                            }
                        }
                        EditorGUILayout.EndScrollView();
                        
                        if (GUILayout.Button("-"))
                        {
                            objectReplacePairs.RemoveAt(i);
                            return;
                        }
                    }
                    EditorGUILayout.EndVertical();
                    
                    EditorGUILayout.LabelField("", GUI.skin.verticalSlider, GUILayout.ExpandHeight(true), GUILayout.Width(10));
                }
                
                if (GUILayout.Button("+", GUILayout.Width(200), GUILayout.MaxHeight(float.MaxValue)))
                {
                    objectReplacePairs.Add(new ObjectReplacePair());
                }
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndScrollView();

            if (GUILayout.Button("Find usages"))
            {
                processCoroutine = EditorCoroutine.StartCoroutine(FindProcess());
            }

            if (GUILayout.Button("Find usages and replace"))
            {
                processCoroutine = EditorCoroutine.StartCoroutine(FindAndReplaceProcess());
            }
        }
        
        private IEnumerator FindProcess()
        {
            foreach (ObjectReplacePair replacePair in objectReplacePairs)
            {
                replacePair.Usages.Clear();
            }
            
            List<Object> replaceMap = new List<Object>();

            foreach (ObjectReplacePair replacePair in objectReplacePairs)
            {
                if (replacePair.TargetObject == null)
                {
                    Debug.LogError("Some of object is null!");
                    continue;
                }

                if (replaceMap.Contains(replacePair.TargetObject))
                {
                    Debug.LogError("Object is repeated!");
                    continue;
                }

                replaceMap.Add(replacePair.TargetObject);
            }

            bool finished = false;
            
            Dictionary<Object, List<ObjectUsage>> usagesDict = new Dictionary<Object, List<ObjectUsage>>();

            EditorCoroutine replaceCoroutine = EditorCoroutine.StartCoroutine(
                ReplacerUtils.FindObjectsInAssets(replaceMap, usagesDict,  f => { progress = f; }, () => { finished = true; }));

            while (!finished)
            {
                if (EditorUtility.DisplayCancelableProgressBar("Process", "Search process", progress))
                {
                    replaceCoroutine.Stop();
                    processCoroutine.Stop();
                    break;
                }

                yield return new WaitForSecondsRealtime(0.02f);
            }

            EditorUtility.ClearProgressBar();

            foreach (KeyValuePair<Object,List<ObjectUsage>> pair in usagesDict)
            {
                foreach (ObjectReplacePair replacePair in objectReplacePairs)
                {
                    if (replacePair.TargetObject == pair.Key)
                    {
                        replacePair.Usages.AddRange(pair.Value);
                        break;
                    }
                }
            }
        }

        private IEnumerator FindAndReplaceProcess()
        {
            Dictionary<Object, Object> replaceMap = new Dictionary<Object, Object>();

            foreach (ObjectReplacePair replacePair in objectReplacePairs)
            {
                if (replacePair.TargetObject == null || replacePair.NewObject == null)
                {
                    Debug.LogError("Some of object is null!");
                    continue;
                }

                if (replacePair.TargetObject.GetType() != replacePair.NewObject.GetType())
                {
                    Debug.LogError("Object types are not the same!");
                    continue;
                }

                if (replaceMap.ContainsKey(replacePair.TargetObject))
                {
                    Debug.LogError("Replaced object is repeated!");
                    continue;
                }

                replaceMap.Add(replacePair.TargetObject, replacePair.NewObject);
            }

            bool finished = false;

            EditorCoroutine replaceCoroutine = EditorCoroutine.StartCoroutine(
                ReplacerUtils.ReplaceFieldsInAssets(replaceMap, f => { progress = f; }, () => { finished = true; }));

            while (!finished)
            {
                if (EditorUtility.DisplayCancelableProgressBar("Process", "Replacement process", progress))
                {
                    replaceCoroutine.Stop();
                    processCoroutine.Stop();
                    EditorUtility.ClearProgressBar();
                    yield break;
                }

                yield return new WaitForSecondsRealtime(0.02f);
            }

            EditorUtility.ClearProgressBar();
        }

        [Serializable]
        private class ObjectReplacePair
        {
            public Object TargetObject;
            public Object NewObject;
            public Vector2 ScrollPos;
            public List<ObjectUsage> Usages = new List<ObjectUsage>();
        }
    }
    
    public class ObjectUsage
    {
        public Object UsingObject;
        public string SceneObjectUsing;
    }
}
