﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

namespace CargoCult.Template.Common.UI.Editor
{
    public class ReplacerUtils
    {
        public static IEnumerator ReplaceFieldsInAssets(Dictionary<Object, Object> replaceMap, Action<float> progressCallback, Action finishCallback)
        {
            List<string> files = new List<string>();
            
            FindFilesRecurse(Application.dataPath, ref files);

            for (int i = 0; i < files.Count; i++)
            {
                string assetPath = files[i].Replace(Application.dataPath, "");
                assetPath = "Assets" + assetPath;

                if (assetPath.EndsWith(".prefab"))
                {
                    ReplaceFieldsInPrefab(assetPath, replaceMap);
                }

                if (assetPath.EndsWith(".asset"))
                {
                    ReplaceFieldInScriptableObject(assetPath, replaceMap);
                }
                
                if (assetPath.EndsWith(".unity"))
                {
                    ReplaceFieldsOnScene(assetPath, replaceMap);
                }

                yield return new WaitForSecondsRealtime(0.02f);

                if (progressCallback != null)
                {
                    progressCallback(i / (float) files.Count);
                }
            }

            AssetDatabase.SaveAssets();

            if (finishCallback != null)
            {
                finishCallback();
            }
        }

        public static IEnumerator FindObjectsInAssets(List<Object> objects, Dictionary<Object, List<ObjectUsage>> dictForFill, Action<float> progressCallback, Action finishCallback)        
        {
            foreach (Object obj in objects)
            {
                dictForFill.Add(obj, new List<ObjectUsage>());
            }
            
            List<string> files = new List<string>();
            
            FindFilesRecurse(Application.dataPath, ref files);

            for (int i = 0; i < files.Count; i++)
            {
                string assetPath = files[i].Replace(Application.dataPath, "");
                assetPath = "Assets" + assetPath;

                if (assetPath.EndsWith(".prefab"))
                {
                    foreach (Object obj in objects)
                    {
                        if (FindObjectInPrefab(assetPath, obj))
                        {
                            dictForFill[obj].Add(new ObjectUsage
                            {
                                UsingObject = AssetDatabase.LoadAssetAtPath<Object>(assetPath),
                            });
                        }
                    }
                }

                if (assetPath.EndsWith(".asset"))
                {
                    foreach (Object obj in objects)
                    {
                        if (FindObjectInScriptableObject(assetPath, obj))
                        {
                            dictForFill[obj].Add(new ObjectUsage
                            {
                                UsingObject = AssetDatabase.LoadAssetAtPath<Object>(assetPath),
                            });
                        }
                    }
                }
                
                if (assetPath.EndsWith(".unity"))
                {
                    foreach (Object obj in objects)
                    {
                        List<string> refs = new List<string>();
                        if (FindObjectOnScene(assetPath, obj, ref refs))
                        {
                            foreach (string s in refs)
                            {
                                dictForFill[obj].Add(new ObjectUsage
                                {
                                    SceneObjectUsing = s,
                                });
                            }
                        }
                    }
                }

                yield return new WaitForSecondsRealtime(0.02f);

                if (progressCallback != null)
                {
                    progressCallback(i / (float) files.Count);
                }
            }

            AssetDatabase.SaveAssets();

            if (finishCallback != null)
            {
                finishCallback();
            }

        }

        private static void FindFilesRecurse(string directoryPath, ref List<string> listToFill)
        {
            listToFill.AddRange(Directory.GetFiles(directoryPath));

            foreach (string directory in Directory.GetDirectories(directoryPath))
            {
                FindFilesRecurse(directory, ref listToFill);
            }
        }

        private static bool FindObjectInPrefab(string assetPath, Object obj)
        {
            GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(assetPath);
            if (prefab)
            {               
                foreach (Component component in prefab.GetComponentsInChildren<Component>(true))
                {
                    if (component != null)
                    {
                        if (FindObjectOnSerializedObject(new SerializedObject(component), obj))
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private static void ReplaceFieldsInPrefab(string assetPath, Dictionary<Object, Object> replaceMap)
        {
            GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(assetPath);
            if (prefab)
            {
                bool haveChanges = false;
                
                foreach (Component component in prefab.GetComponentsInChildren<Component>(true))
                {
                    if (component != null)
                    {
                        haveChanges |= ChangeFieldsOnSerializedObject(new SerializedObject(component), replaceMap);
                    }
                }

                if (haveChanges)
                {
                    Debug.Log(string.Format("Fields on prefab {0} was modified.", prefab.name), prefab);
                    EditorUtility.SetDirty(prefab);
                }
            }
        }

        private static bool FindObjectInScriptableObject(string assetPath, Object obj)
        {
            ScriptableObject scriptableObject = AssetDatabase.LoadAssetAtPath<ScriptableObject>(assetPath);
            if (scriptableObject)
            {
                if (FindObjectOnSerializedObject(new SerializedObject(scriptableObject), obj))
                {
                    return true;
                }
            }

            return false;
        }
        
        private static void ReplaceFieldInScriptableObject(string assetPath, Dictionary<Object, Object> replaceMap)
        {
            ScriptableObject scriptableObject = AssetDatabase.LoadAssetAtPath<ScriptableObject>(assetPath);
            if (scriptableObject)
            {
                bool haveChanges = ChangeFieldsOnSerializedObject(new SerializedObject(scriptableObject), replaceMap);

                if (haveChanges)
                {
                    Debug.Log(string.Format("Fields on scriptable object {0} was modified.", scriptableObject.name), scriptableObject);
                    EditorUtility.SetDirty(scriptableObject);
                }
            }
        }

        private static bool FindObjectOnScene(string assetPath, Object obj, ref List<string> objNames)
        {
            Scene scene = EditorSceneManager.OpenScene(assetPath, OpenSceneMode.Additive);

            GameObject[] gameObjects = scene.GetRootGameObjects();
            for (int i = 0; i < gameObjects.Length; i++)
            {
                GameObject rootGameObject = gameObjects[i];
                                
                foreach (Component component in rootGameObject.GetComponentsInChildren<Component>(true))
                {
                    if (component == null)
                    {
                        continue;
                    }
                    
                    if (!PrefabUtility.IsPartOfAnyPrefab(component))
                    {
                        if (FindObjectOnSerializedObject(new SerializedObject(component), obj))
                        {
                            objNames.Add(scene.name + " / " + component.gameObject.name + " / " + component.GetType());
                       }
                    }
                }
            }

            EditorSceneManager.CloseScene(scene, true);

            return objNames.Count > 0;
        }
        
        private static void ReplaceFieldsOnScene(string assetPath, Dictionary<Object, Object> replaceMap)
        {
            Scene scene = EditorSceneManager.OpenScene(assetPath, OpenSceneMode.Additive);

            bool haveChanges = false;
            
            GameObject[] gameObjects = scene.GetRootGameObjects();
            for (int i = 0; i < gameObjects.Length; i++)
            {
                GameObject rootGameObject = gameObjects[i];
                
                bool goChanged = false;
                
                foreach (Component component in rootGameObject.GetComponentsInChildren<Component>(true))
                {
                    if (component == null)
                    {
                        continue;
                    }
                    
                    if (!PrefabUtility.IsPartOfAnyPrefab(component))
                    {
                        goChanged |= ChangeFieldsOnSerializedObject(new SerializedObject(component), replaceMap);
                    }
                }

                if (goChanged)
                {
                    EditorUtility.SetDirty(rootGameObject);
                    haveChanges = true;
                }
            }

            if (haveChanges)
            {
                EditorSceneManager.SaveScene(scene);
                Debug.Log(string.Format("Fields on scene {0} was modified.", scene.name));
            }

            EditorSceneManager.CloseScene(scene, true);
        }

        private static bool FindObjectOnSerializedObject(SerializedObject so, Object obj)
        {
            if (so == null)
            {
                return false;
            }
            
            SerializedProperty property = so.GetIterator();
            while (property.Next(true))
            {
                if (!property.type.Contains("PPtr"))
                {
                    continue;
                }
                
                Object value = property.objectReferenceValue;

                if (value != null && value.GetType() == obj.GetType() && obj.Equals(value))
                {
                    return true;
                }

                if (property.isArray)
                {
                    for (int i = 0; i < property.arraySize; i++)
                    {
                        SerializedProperty arrayElement = property.GetArrayElementAtIndex(i);
                        value = arrayElement.objectReferenceValue;
                        if (value != null && value.GetType() == obj.GetType() && obj.Equals(value))
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }
        
        private static bool ChangeFieldsOnSerializedObject(SerializedObject so, Dictionary<Object, Object> replaceMap)
        {
            bool haveChanges = false;

            if (so == null)
            {
                return false;
            }
            
            SerializedProperty property = so.GetIterator();
            while (property.Next(true))
            {
                if (!property.type.Contains("PPtr"))
                {
                    continue;
                }
                
                Object value = property.objectReferenceValue;

                if (value != null && value.GetType() == replaceMap.First().Key.GetType() && replaceMap.ContainsKey(value))
                {
                    property.objectReferenceValue = replaceMap[value];
                    haveChanges = true;
                }

                if (property.isArray)
                {
                    for (int i = 0; i < property.arraySize; i++)
                    {
                        SerializedProperty arrayElement = property.GetArrayElementAtIndex(i);
                        value = arrayElement.objectReferenceValue;
                        if (value != null && value.GetType() == replaceMap.First().Key.GetType() && replaceMap.ContainsKey(value))
                        {
                            arrayElement.objectReferenceValue = replaceMap[value];
                            haveChanges = true;
                        }
                    }
                }
            }

            if (haveChanges)
            {
                so.ApplyModifiedProperties();
            }
            
            return haveChanges;
        }
    }
}