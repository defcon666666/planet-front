﻿namespace CargoCult.Template.Common.KeysContrainers
{
    public static class OptionsKeysContainer
    {
        public const string MasterVolumeKey = "MasterVolume";
        public const string SoundVolumeKey = "SoundVolume";
        public const string MusicVolumeKey = "MusicVolume";
        public const string CurrentLanguage = "LocalizationCurrentLanguage";
        public const string QualitySettingsIndexKey = "QualitySettingsIndexKey";
    }
    
    public static class AdsKeysContainer
    {
        public const string IsAdsFree = "IsAdsFree";
        public const string FullScreenAdsSwitchOffTime = "FullScreenAdsSwitchOffTime";
    }
}
