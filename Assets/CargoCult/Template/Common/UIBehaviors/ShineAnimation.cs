﻿using UnityEngine;

namespace CargoCult.Template.Common.UIBehaviors
{
    [RequireComponent(typeof(UIColorShine))]
    public class ShineAnimation : MonoBehaviour
    {
        //ANIMATION FOR X AND Y, OR X IF isUniform set to false 
        public AnimationCurve animCurve;

        [Tooltip("Animation speed multiplier")]
        public float speed = 1;

        private float t;
        private UIColorShine m_UIColorShine;

        void OnEnable()
        {
            t = 0;
            m_UIColorShine = GetComponent<UIColorShine>();
        }


        private void Update()
        {
            t += speed * Time.unscaledDeltaTime;
            m_UIColorShine.shinePositon = animCurve.Evaluate(t);
        }
    }
}