﻿using UnityEngine;

namespace CargoCult.Template.Common.UIBehaviors.UIAnimationBehaviors
{
    public enum MoveAxis
    {
        X = 0,
        Y = 1
    }

    [CreateAssetMenu(fileName = "UIElementPresentationConfig", menuName = "UI/Create UIElement presentation config", order = 1)]
    public class PresentationConfig : ScriptableObject
    {
        [SerializeField] private Configuration config;

        public void IncreaseAnimationProgress(ref float progress, float delta)
        {
            progress += config.AnimationSpeed * delta;
        }

        public void DecreaseAnimationProgress(ref float progress, float delta)
        {
            progress -= config.AnimationSpeed * delta;
        }

        public float EvaluateAnimation(float value)
        {
            return config.AnimationCurve.Evaluate(value);
        }

        public float EvaluateAlpha(float value)
        {
            return config.AlphaCurve.Evaluate(value);
        }

        public float DeltaPosition()
        {
            return config.DeltaPosition;
        }

        public MoveAxis Axis()
        {
            return config.Axis;
        }

        public bool IsLoop()
        {
            return config.Loop;
        }

        [System.Serializable]
        private struct Configuration
        {
            public AnimationCurve AlphaCurve;
            public AnimationCurve AnimationCurve;
            public float AnimationSpeed;
            public MoveAxis Axis;
            public float DeltaPosition;
            public bool Loop;
        }
    }
}
