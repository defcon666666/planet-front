﻿using UnityEngine;

namespace CargoCult.Template.Common.UIBehaviors.UIAnimationBehaviors
{
    [RequireComponent(typeof(CanvasGroup))]
    public class PresentationUIComponent : MonoBehaviour
    {
        [SerializeField] private CanvasGroup animationTarget;
        [SerializeField] private PresentationConfig config;

        private float animationProgress = 0;
        private float delay;
        private bool wasInit = false;
        private bool hasRect = false;
        private RectTransform targetRectTransform;

        private RectTransform TargetRectTransform
        {
            get
            {
                if (!wasInit)
                {
                    Init();
                }

                return targetRectTransform;
            }
        }

        public bool HasRect
        {
            get 
            { 
                if (!wasInit)
                {
                    Init();
                }

                return hasRect;
            }
        }

        public void SetActivationDelay(float value)
        {
            delay = value;
        }

        private void OnEnable()
        {
            if (animationTarget == null)
            {
                animationTarget = GetComponent<CanvasGroup>();
            }
            animationProgress = 0f;
            animationTarget.alpha = 0;
            delay = 0f;
        }

        private void Update()
        {
            float delta = Time.unscaledDeltaTime;
            if (delay > 0f)
            {
                delay -= delta;
                SetAlpha(0);
                return;
            }

            if (config.IsLoop() || animationProgress <= 1)
            {
                config.IncreaseAnimationProgress(ref animationProgress, delta);
            }

            SetAnimationProgress(config.EvaluateAnimation(animationProgress), config.EvaluateAlpha(animationProgress));
        }

        private void SetAnimationProgress(float positionValue, float alphaValue)
        {
            SetAlpha(alphaValue);
            
            Vector3 position = Vector3.zero;
            switch (config.Axis())
            {
                case MoveAxis.X:
                    position.x = config.DeltaPosition() * (1 - positionValue);
                    break;
                case MoveAxis.Y:
                    position.y = config.DeltaPosition() * (1 - positionValue);
                    break;
            }

            if (HasRect)
            {
                TargetRectTransform.anchoredPosition = position;
            }
            else
            {
                animationTarget.transform.localPosition = position;
            }
        }

        private void SetAlpha(float value)
        {
            if (animationTarget.alpha == value)
                return;
            
            animationTarget.alpha = value;
        }

        private void Init()
        {
            hasRect = animationTarget.transform is RectTransform;
            if (hasRect)
            {
                targetRectTransform = animationTarget.transform as RectTransform;
            }

            wasInit = true;
        }
    }
}
