﻿using UnityEngine;
using UnityEngine.UI;

namespace CargoCult.Template.Common.UIBehaviors
{
    public enum LightMode
    {
        UnLit,
        BumpLit,
    }

    /// <summary>
    /// 
    /// </summary>
    public abstract class UISpriteColorBase : MonoBehaviour
    {
        protected MaskableGraphic uispriteRenderer;

        /// <summary>
        /// Shader path.
        /// </summary>
        //protected abstract string ShaderPath { get; }
        public Shader m_Shader;


        private void OnEnable()
        {
            uispriteRenderer = this.gameObject.GetComponent<MaskableGraphic>();
            if (uispriteRenderer != null)
            {
                CreateMaterial();

                UISpriteColorBase spriteColorBase = this.gameObject.GetComponent<UISpriteColorBase>();

                Initialize();
            }
            else
            {
                Debug.LogWarning(string.Format("'{0}' without UISpriteRenderer, disabled.", this.GetType().ToString()));

                this.enabled = false;
            }
        }

        private void OnDisable()
        {
            if (uispriteRenderer != null && uispriteRenderer.material != null &&
                string.CompareOrdinal(uispriteRenderer.material.name, @"UI/Default") != 0)
            {
                uispriteRenderer.material = null;
            }
        }

        private void Update()
        {
            if (uispriteRenderer == null)
                uispriteRenderer = this.gameObject.GetComponent<MaskableGraphic>();

            if (uispriteRenderer != null && uispriteRenderer.material != null)
            {
                UpdateShader();
            }
        }

        /// <summary>
        /// Create the material.
        /// </summary>
        protected void CreateMaterial()
        {
            string effectName = this.GetType().ToString().Replace(@"UISpriteColorBase.", string.Empty);


            if (m_Shader == null)
            {
                Debug.LogWarning(string.Format("Failed to load '{0}', {1} disabled.", "Shader", effectName));

                this.enabled = false;
            }
            else if (m_Shader.isSupported == false)
            {
                Debug.LogWarning(string.Format("Shader '{0}' not supported, {1} disabled.", "Shader", effectName));

                this.enabled = false;
            }
            else
            {
                if (uispriteRenderer == null)
                    uispriteRenderer = this.gameObject.GetComponent<MaskableGraphic>();

                bool pixelSnap = false;
                Color tint = Color.white;
                Vector2 mainTextureOffset = Vector2.zero;
                Vector2 mainTextureScale = Vector2.one;
                Vector2 bumpTextureOffset = Vector2.zero;
                Vector2 bumpTextureScale = Vector2.one;
                bool bumpMap = false;

                if (uispriteRenderer.material != null)
                {
                    // Pixel snap from the editor.
                    pixelSnap = uispriteRenderer.material.IsKeywordEnabled(@"PIXELSNAP_ON");

                    // Color tint from the editor.
                    tint = uispriteRenderer.material.color;

                    // Texture matrix from the editor.
                    mainTextureOffset = uispriteRenderer.material.GetTextureOffset(@"_MainTex");
                    mainTextureScale = uispriteRenderer.material.GetTextureScale(@"_MainTex");
                    bumpTextureOffset = Vector2.zero;
                    bumpTextureScale = Vector2.one;

                    bumpMap = uispriteRenderer.material.IsKeywordEnabled(@"_BumpMap");
                    if (bumpMap == true)
                    {
                        bumpTextureOffset = uispriteRenderer.material.GetTextureOffset(@"_BumpMap");
                        bumpTextureScale = uispriteRenderer.material.GetTextureScale(@"_BumpMap");
                    }
                }

                uispriteRenderer.material = new Material(m_Shader);
                uispriteRenderer.material.name = string.Format("UISprite/{0}", effectName);

                if (pixelSnap == true)
                {
                    uispriteRenderer.material.SetFloat(@"PixelSnap", 1.0f);
                    uispriteRenderer.material.EnableKeyword(@"PIXELSNAP_ON");
                }

                uispriteRenderer.material.SetColor(@"_Color", tint);
                uispriteRenderer.material.SetTextureOffset(@"_MainTex", mainTextureOffset);
                uispriteRenderer.material.SetTextureScale(@"_MainTex", mainTextureScale);

                if (bumpMap == true)
                {
                    uispriteRenderer.material.SetTextureOffset(@"_BumpMap", bumpTextureOffset);
                    uispriteRenderer.material.SetTextureScale(@"_BumpMap", bumpTextureScale);
                }
            }
        }

        /// <summary>
        /// Initialize the effect.
        /// </summary>
        protected virtual void Initialize()
        {
        }

        /// <summary>
        /// Send values to shader.
        /// </summary>
        protected abstract void UpdateShader();
    }
}