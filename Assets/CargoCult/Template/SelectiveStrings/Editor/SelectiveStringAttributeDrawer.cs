﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace CargoCult.Template.SelectiveStrings.Editor
{
    [CustomPropertyDrawer(typeof(SelectiveStringAttribute))]
    public class SelectiveStringAttributeDrawer : PropertyDrawer
    {
        private const float lablePerSymbolWidth = 7f;
        private const float buttonWidth = 66;
        private const float columnSpace = 6;
        private float baseHeight;
        private Type type;
        private FieldInfo attFieldInfo;
        private object someValue;
        private string[] containerTags;
        private List<string> allTags = new List<string>();

        private Color errorColor = new Color(1f, 0.11f, 0.08f, 0.5f);
        private Color warningColor = new Color(1f, 0.9f, 0.17f, 0.5f);

        private SerializedProperty controlledProperty;

        private string propertyName;
        private float lableWidth;
        private Rect labelRect;
        private Rect fieldRect;

        private GUIStyle labelStyle = EditorStyles.label;
        private GUIStyle fieldStyle = EditorStyles.textField;

        //Необходимо для избегания замыканий в случае обработки массива.
        private class PropertyPathPack
        {
            public SerializedProperty property;
            public string path;

            public PropertyPathPack(SerializedProperty newProperty, string newPath)
            {
                property = newProperty;
                path = newPath;
            }
        }

        // Draw the property inside the given rect
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SelectiveStringAttribute selectiveStringAttribute = attribute as SelectiveStringAttribute;

            float originWidth = position.width;
            position = EditorGUI.IndentedRect(position);

            int indentLevel = EditorGUI.indentLevel;
            float indent = originWidth - position.width;
            EditorGUI.indentLevel = 0;

            propertyName = label.text;
            lableWidth = EditorGUIUtility.labelWidth - indent;
            labelRect = new Rect(position.x, position.y, lableWidth, baseHeight);
            fieldRect = new Rect(position.x + lableWidth, position.y, position.width - lableWidth, baseHeight);

            labelStyle = EditorStyles.label;
            fieldStyle = EditorStyles.textField;

            if (property.propertyType == SerializedPropertyType.String)
            {
                controlledProperty = property;
                type = typeof(SelectiveStringTagsContainer);
                allTags.Clear();

                if (selectiveStringAttribute.ValidTagsContainerName == null)
                {
                    ShowMessage(position, errorColor, "ERROR: SelectiveString attribute shouldn't have empty string as parametr.");
                    return;
                }

                if (selectiveStringAttribute.ValidTagsContainerName.Length == 0)
                {
                    ShowMessage(position, errorColor, "ERROR: SelectiveString attribute shouldn't have empty string as parametr.");
                    return;
                }

                foreach (string tag in selectiveStringAttribute.ValidTagsContainerName)
                {
                    if (string.IsNullOrEmpty(tag))
                    {
                        ShowMessage(position, errorColor, "ERROR: SelectiveString attribute shouldn't have empty string as parametr.");
                        return;
                    }

                    attFieldInfo = type.GetField(tag);

                    if (attFieldInfo == null)
                    {
                        ShowMessage(position, errorColor, "ERROR: Tags container with name \""
                                                          + selectiveStringAttribute.ValidTagsContainerName + "\" wasn't found in \"" +
                                                          type.Name + "\".");
                        return;
                    }

                    someValue = attFieldInfo.GetValue(null);
                    containerTags = someValue as string[];

                    if (containerTags == null)
                    {
                        ShowMessage(position, errorColor, "ERROR: Field \"" + selectiveStringAttribute.ValidTagsContainerName +
                                                          "\" must be a static string array.");
                        return;
                    }

                    allTags.AddRange(containerTags);
                }

                float buttonOffsetX = lableWidth + columnSpace;
                float fieldOffsetX = buttonOffsetX + buttonWidth + columnSpace;


                Rect buttonRect = new Rect(position.x + buttonOffsetX, position.y, buttonWidth, baseHeight);
                fieldRect = new Rect(position.x + fieldOffsetX, position.y, position.width - fieldOffsetX, baseHeight);

                if (property.isInstantiatedPrefab && property.prefabOverride)
                    labelStyle.fontStyle = fieldStyle.fontStyle = FontStyle.Bold;

                string showingValue = controlledProperty.stringValue;

                if (!allTags.Contains(controlledProperty.stringValue))
                {
                    showingValue += " <<< (Tag wasn't found. Probably, it's obsolete)";
                    EditorGUI.DrawRect(position, warningColor);
                }

                EditorGUI.LabelField(labelRect, propertyName, labelStyle);
                EditorGUI.SelectableLabel(fieldRect, showingValue, fieldStyle);

                if (EditorGUI.DropdownButton(buttonRect, new GUIContent("CHANGE"), FocusType.Passive))
                {
                    GenericMenu menu = new GenericMenu();

                    for (int i = 0; i < allTags.Count; i++)
                    {
                        AddMenuItem(menu, allTags[i]);
                    }

                    menu.ShowAsContext();
                }
            }
            else
            {
                ShowMessage(position, warningColor, "WARNING: Use SelectiveString attribute with string fields only.");
            }

            labelStyle.fontStyle = fieldStyle.fontStyle = FontStyle.Normal;
            EditorGUI.indentLevel = indentLevel;
        }

        void AddMenuItem(GenericMenu someMenu, string menuPath)
        {
            someMenu.AddItem(new GUIContent(menuPath), controlledProperty.stringValue.Equals(menuPath),
                OnStringSelected, new PropertyPathPack(controlledProperty, menuPath));
            //Использовать PropertyPathPack необходимо во избежание замыкания. В ином случае, controlledProperty всегда 
            //будет указывать на последнее поле массива <<KrOost
        }

        void OnStringSelected(object incObj)
        {
            PropertyPathPack propertyPathPack = (PropertyPathPack)incObj;
            propertyPathPack.property.stringValue = propertyPathPack.path;
            propertyPathPack.property.serializedObject.ApplyModifiedProperties();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            baseHeight = base.GetPropertyHeight(property, label);
            return baseHeight;
        }

        private void ShowMessage(Rect position, Color backgroundColor, string message)
        {
            labelStyle.fontStyle = fieldStyle.fontStyle = FontStyle.Normal;
            EditorGUI.DrawRect(position, backgroundColor);
            EditorGUI.LabelField(labelRect, propertyName);
            EditorGUI.SelectableLabel(fieldRect, message);
        }
    }
}
