﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace CargoCult.Template.SelectiveStrings.Editor
{
    public static class SelectiveStringEditorUtils
    {
        public static void DrawSelectiveStringField(string label, string[] containerNames, string currentValue, Action<string> onValueChange)
        {
            List<string> allTags = new List<string>(); 
            
            foreach (string tag in containerNames)
            {
                FieldInfo attFieldInfo = typeof(SelectiveStringTagsContainer).GetField(tag);
                object someValue = attFieldInfo.GetValue(null);
                string[] containerTags = (string[]) someValue;
                allTags.AddRange(containerTags);
            }

            EditorGUILayout.BeginHorizontal();
            {
                if (!string.IsNullOrEmpty(label))
                {
                    EditorGUILayout.LabelField(label, GUILayout.Width(80));
                }

                EditorGUILayout.LabelField(currentValue, new GUIStyle(EditorStyles.textField), GUILayout.Width(200));

                if (EditorGUILayout.DropdownButton(new GUIContent("CHANGE"), FocusType.Passive, GUILayout.Width(80)))
                {
                    GenericMenu menu = new GenericMenu();

                    for (int i = 0; i < allTags.Count; i++)
                    {
                        string menuPath = allTags[i];
                        menu.AddItem(new GUIContent(menuPath), currentValue == menuPath, () => onValueChange(menuPath));
                    }

                    menu.ShowAsContext();
                }
            }
            EditorGUILayout.EndHorizontal();
        }

        public static bool IsSelectiveString(string str, string[] containerNames)
        {
            foreach (string tag in containerNames)
            {
                FieldInfo attFieldInfo = typeof(SelectiveStringTagsContainer).GetField(tag);
                object someValue = attFieldInfo.GetValue(null);
                string[] containerTags = (string[]) someValue;
                foreach (string containerTag in containerTags)
                {
                    if (containerTag == str)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}