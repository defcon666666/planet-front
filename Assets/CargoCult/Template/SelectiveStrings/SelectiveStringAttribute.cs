﻿using UnityEngine;

namespace CargoCult.Template.SelectiveStrings
{
    /// <summary>
    /// See SelectiveStringAttributeDrawer class for more detail. KrOost
    /// </summary>
    public class SelectiveStringAttribute : PropertyAttribute
    {
        public readonly string[] ValidTagsContainerName;

        /// <summary>
        /// Parameter - name of any static string array from SelectiveStringTagsContainer
        /// </summary>
        /// <param name="tagsContainerName">Name of any static string array from SelectiveStringTagsContainer</param>
        public SelectiveStringAttribute(params string[] tagsContainerName)
        {
            ValidTagsContainerName = tagsContainerName;
        }
    }
}
