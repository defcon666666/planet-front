﻿using CargoCult.Template.Global.Sound;

namespace CargoCult.Template.SelectiveStrings
{
    public class SelectiveStringTagsContainer
    {
        public static readonly string[] SoundTypes =
        {
            SoundType.None,
            SoundType.ButtonClick,
            SoundType.BackgroundMusic,
            SoundType.WinSound,
            SoundType.LooseSound,
            SoundType.OpenDoor,
            SoundType.LaunchMissile,
            SoundType.CoinUp,
            SoundType.ShipDead,
            SoundType.ShipDamage,
            SoundType.PlanetDead,
            SoundType.PlanetDamage,
            SoundType.UpgradeUp
        };
        
        public static readonly string[] BaseSceneNames =
        {
            "Global",
            "Menu",
            "Game",
        };
    }
}
