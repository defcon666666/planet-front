﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Object = System.Object;

namespace CargoCult.Template.Injectro.Core
{
    public class SceneContext : BaseInjectroContext
    {
        public static SceneContext Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (SyncRoot)
                    {
                        if (instance == null)
                        {
                            throw new InjectroException("SceneContext is not initialized");                                                       
                        }
                        return instance;
                    }
                }
                return instance;
            }
        }
        [Space(10)]
        public string GlobalContextSceneName = string.Empty;
        
        private static SceneContext instance;
        private static readonly object SyncRoot = new Object();

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else if (!instance.Equals(this))
            {
                throw new InjectroException("Injectro Context duplication");
            }
            try
            {
                SetGlobalContext(GlobalContext.Instance.SingletonContext);
            }
            catch (NotInitializedException)
            {
                if (!GlobalContextSceneName.Equals(string.Empty))
                {
                    SceneManager.LoadScene(GlobalContextSceneName, LoadSceneMode.Additive);
                    
                    Debug.LogWarningFormat("Loading additive scene ({0})", GlobalContextSceneName);

                    if (SceneManager.GetSceneByName(GlobalContextSceneName).IsValid())
                    {
                        SceneManager.sceneLoaded += OnSceneLoaded;
                        return;
                    }
                }
                
                if (IsDebug)
                {
                    Debug.Log("Working without Global Injectro Context");
                }
            }
            
            InitBeans();
        }
        
        private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            if (!GlobalContextSceneName.Equals(scene.name))
                return;
            
            SceneManager.sceneLoaded -= OnSceneLoaded;
            
            try
            {
                SetGlobalContext(GlobalContext.Instance.SingletonContext);
            }
            catch (NotInitializedException)
            {
                if (IsDebug)
                {
                    Debug.Log("Working without Global Injectro Context");
                }
            }
            
            InitBeans();
            
            if (IsDebug)
            {
                Debug.LogWarningFormat(
                    "Global context and additive scene ({0}) was created by local context (on scene {1})" +
                    "\nThat shoudn't happen in properly executed project",
                    GlobalContextSceneName, SceneManager.GetActiveScene().name);
            }
        }
        
        private void OnDestroy()
        {
            if (Equals(instance))
            {
                instance = null;
            }
        }
    }
}