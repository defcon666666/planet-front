﻿using System;

namespace CargoCult.Template.Injectro.Core
{
    public class InjectroException : Exception
    {
        public InjectroException()
        {            
        }

        public InjectroException(string message) : base(message)
        {
        }

        public InjectroException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}