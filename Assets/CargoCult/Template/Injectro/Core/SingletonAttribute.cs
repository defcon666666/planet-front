﻿using System;

namespace CargoCult.Template.Injectro.Core
{
    [AttributeUsage(AttributeTargets.Class)]
    public class SingletonAttribute : Attribute
    {
        
    }
}