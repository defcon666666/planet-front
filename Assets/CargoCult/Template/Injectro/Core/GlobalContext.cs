﻿using UnityEngine;
using Object = System.Object;

namespace CargoCult.Template.Injectro.Core
{
    public class GlobalContext : BaseInjectroContext
    {
        public static GlobalContext Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (SyncRoot)
                    {
                        if (instance == null)
                        {
                            throw new NotInitializedException("GlobalContext is not initialized");                                                       
                        }
                        return instance;
                    }
                }
                return instance;
            }
        }

        private static GlobalContext instance;
        private static readonly object SyncRoot = new Object();

        protected override void ScannedSingletonDestroyOnLoad(GameObject sceneGameObject)
        {
            DontDestroyOnLoad(sceneGameObject);
        }

        private void Awake()
        {
            if (instance != null)
            {
                Destroy(gameObject);
                return;
            }
            instance = this;

            foreach (GameObject singletonPreset in SingletonBeanPresetObjects)
            {
                DontDestroyOnLoad(singletonPreset);
            }
            
            InitBeans();
            DontDestroyOnLoad(gameObject);
        }
        
        private void OnDestroy()
        {
            if (Equals(instance))
            {
                instance = null;
            }
        }
    }        
}