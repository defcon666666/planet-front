﻿namespace CargoCult.Template.Injectro.Core
{
    public interface IPostInject : ISubBean
    {
        void PostInject();
    }
}