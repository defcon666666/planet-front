﻿using System;

namespace CargoCult.Template.Injectro.Core
{
    public class NotInitializedException : InjectroException   {
        public NotInitializedException()
        {            
        }

        public NotInitializedException(string message) : base(message)
        {
        }

        public NotInitializedException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}