﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace CargoCult.Template.Injectro.Core
{
    public class ContextEventProcessor
    {
        public delegate void ContextEvent(GameObject poolingObject, IBean bean);

        private class EventBeanPair
        {
            public readonly ContextEvent ContextEvent;
            public readonly IBean Bean;

            public EventBeanPair(ContextEvent contextEvent, IBean bean)
            {
                ContextEvent = contextEvent;
                Bean = bean;
            }

            public override bool Equals(object obj) 
            {
                return obj is EventBeanPair && this == (EventBeanPair)obj;
            }
            public override int GetHashCode() 
            {
                return ContextEvent.GetHashCode() ^ Bean.GetHashCode();
            }
            public static bool operator ==(EventBeanPair x, EventBeanPair y) 
            {
                if (ReferenceEquals(x, y))
                {
                    return true;
                }

                if (((object)x == null) || ((object)y == null))
                {
                    return false;
                }
                
                return x.ContextEvent == y.ContextEvent && x.Bean == y.Bean;
            }
            public static bool operator !=(EventBeanPair x, EventBeanPair y) 
            {
                return !(x == y);
            }
        }

        private readonly IDictionary<GameObject, List<EventBeanPair>> beforeReturnToContextEvent = new Dictionary<GameObject, List<EventBeanPair>>();

        public void AddEvent(GameObject gameObject, ContextEvent contextEvent, IBean bean)
        {
            List<EventBeanPair> eventBeanPairs;
            if (!beforeReturnToContextEvent.ContainsKey(gameObject))
            {
                eventBeanPairs = new List<EventBeanPair>();                
                beforeReturnToContextEvent.Add(gameObject, eventBeanPairs);
            }
            else
            {
                eventBeanPairs = beforeReturnToContextEvent[gameObject];
            }

            EventBeanPair eventBeanPair = new EventBeanPair(contextEvent, bean);
            if (!eventBeanPairs.Contains(eventBeanPair))
            {
                eventBeanPairs.Add(eventBeanPair);
            }
        }

        public void InvokeEvents(GameObject gameObject)
        {
            List<EventBeanPair> eventBeanPairs;
            beforeReturnToContextEvent.TryGetValue(gameObject, out eventBeanPairs);
            if (eventBeanPairs != null && eventBeanPairs.Count > 0)
            {
                for (int i = 0; i < eventBeanPairs.Count; i++)
                {
                    EventBeanPair contextEventPair = eventBeanPairs[eventBeanPairs.Count - 1 - i];
                    if (contextEventPair != null)
                    {
                        try
                        {
                            contextEventPair.ContextEvent.Invoke(gameObject, contextEventPair.Bean);
                        }
                        catch (Exception e)
                        {
                            Debug.LogError(e.Message + "\n" + e.StackTrace);
                        }
                    }
                        
                }
                eventBeanPairs.Clear();
            }
        }
    }

}
