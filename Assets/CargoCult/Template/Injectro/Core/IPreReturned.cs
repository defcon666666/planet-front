﻿namespace CargoCult.Template.Injectro.Core
{
    public interface IPreReturned : IBean
    {
        /// <summary>
        /// Calls just before returning bean to the pool. Bean is not disabled yet.
        /// </summary>
        void PreReturned();
    }
}