﻿using System;

namespace CargoCult.Template.Injectro.Core
{
    public class NoSuchBeanException : InjectroException   {
        public NoSuchBeanException()
        {            
        }

        public NoSuchBeanException(string message) : base(message)
        {
        }

        public NoSuchBeanException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}