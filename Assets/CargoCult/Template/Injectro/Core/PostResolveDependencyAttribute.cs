﻿using System;

namespace CargoCult.Template.Injectro.Core
{
    [AttributeUsage(AttributeTargets.Class)]
    public class PostResolveDependencyAttribute : Attribute
    {

        public Type[] Dependencies { get; }

        public PostResolveDependencyAttribute(params Type[] dependencies)
        {
            this.Dependencies = dependencies;
        }
    }
}