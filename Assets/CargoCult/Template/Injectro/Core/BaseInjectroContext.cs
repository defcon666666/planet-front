﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using UnityEngine;
using Object = System.Object;

namespace CargoCult.Template.Injectro.Core
{
    public class BaseInjectroContext : MonoBehaviour, InjectroContext
    {
        private static readonly HashSet<Type> IgnoredTypes = new HashSet<Type>(new[]
        {
            typeof(MonoBehaviour),
            typeof(Component),            
            typeof(IPreReturned),            
            typeof(IPostResolved),            
            typeof(IKnowALoss),            
            typeof(IBean),            
        });

        private static readonly BindingFlags IocFieldsFlags = 
            BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;
        
        [Serializable]
        public class PoolConfig
        {
            public GameObject Prefab;
            public int InitialCount = 2;
            public int MaximumCount = 100;
            public HashSet<Type> IBeanComponents = new HashSet<Type>();
            public List<Type> BeanTypes = new List<Type>();
            private GameObject wrapper;
            private int itemsInUse;

            public GameObject Wrapper
            {
                get { return wrapper; }
                set { wrapper = value; }
            }

            public int ItemsInUse
            {
                get { return itemsInUse; }
                set { itemsInUse = value; }
            }
        }
        
        public MultiMap<Type, IBean> SingletonContext
        {
            get { return singletonContext; }
        }

        public List<GameObject> SingletonBeanPrefabs;        
        public List<GameObject> SingletonBeanPresetObjects;
        
        public List<GameObject> PrototypeBeanPrefabs;
        [Space(10)]
        public bool IsTestContext;
        public bool IsDebug;

        private int itemInUseCount;

        private IDictionary<string, List<GameObject>> storage = new Dictionary<string, List<GameObject>>();
        private IDictionary<GameObject, string> itemsInUse = new Dictionary<GameObject, string>();       
        
        private IDictionary<string, PoolConfig> poolConfigsByName = new Dictionary<string, PoolConfig>();
        
        private ContextEventProcessor beforReturnEventProcessor = new ContextEventProcessor();
        
        private IDictionary<IBean, GameObject> prototypeBeansToOwnGameObject = new Dictionary<IBean, GameObject>();
        
        private IDictionary<GameObject, MultiMap<Type, IBean>> gameObjectToLocalContexts = new Dictionary<GameObject, MultiMap<Type, IBean>>();
        private IDictionary<GameObject, LinkedList<GameObject>> masterToChild = new Dictionary<GameObject, LinkedList<GameObject>>();
        private IDictionary<GameObject, GameObject> childToMaster = new Dictionary<GameObject, GameObject>();
              
        private MultiMap<Type, PoolConfig> beanToPoolConfig = new MultiMap<Type, PoolConfig>();
        private MultiMap<Type, IBean> singletonContext = new MultiMap<Type, IBean>();
        private HashSet<IBean> singletonBeans = new HashSet<IBean>();
        private MultiMap<Type, IBean> globalContext = new MultiMap<Type, IBean>();

        public void MakeBeanKnowALoss<T>(T bean, IBean lossingBean) where T: IBean, IKnowALoss
        {
            MakeBeanKnowALoss(bean, GetOwnGameObject(lossingBean), lossingBean);
        }
        
        public void MakeBeanKnowALoss<T>(T bean, GameObject lossingObject, IBean lossingBean = null) where T: IBean, IKnowALoss
        {
            GameObject ownGameObject = GetOwnGameObject(bean);            
            AddBeforeReturnEvent(lossingObject, (returningObject, returningBean) =>
            {
                if (ownGameObject == null || itemsInUse.ContainsKey(ownGameObject))
                {
                    bean.KnowALoss(returningObject, returningBean);
                }
            }, lossingBean);            
        }

        public T FarBean<T>(IBean closestMasterBean = null) where T : IBean
        {
            T result = (T)(IBean)null; 
            FindFarBeans((T bean) =>
            {
                result = bean;
            }, closestMasterBean);
            
            return result;
        }

        public ICollection<T> FarBeans<T>(IBean closestMasterBean = null) where T : IBean
        {
            ICollection<T> result = new List<T>(); 
            FindFarBeans((T bean) =>
            {
                result.Add(bean);
            }, closestMasterBean, false);
            
            return result;
        }
        
        public T GetBean<T>(IBean contextBean) where T : IBean
        {
            return GetBean<T>(GetOwnGameObject(contextBean));            
        }
        
        public T GetBean<T>(GameObject ownBeanGameObject = null) where T : IBean
        {
            return (T) GetBean(typeof(T), ownBeanGameObject);
        }
        
        public ICollection<T> GetBeans<T>(IBean contextBean)  where T : IBean
        {
            GameObject ownGameObject = GetOwnGameObject(contextBean);                       
            return GetBeans<T>(ownGameObject); 
        }
        
        public ICollection<T> GetBeans<T>(GameObject ownBeanGameObject = null) where T : IBean
        {
            return GetBeans<T>(typeof(T), ownBeanGameObject);
        } 

        public GameObject GetOwnGameObject(IBean contextBean)
        {
            GameObject ownGameObject = null;
            if (contextBean != null)
            {
                prototypeBeansToOwnGameObject.TryGetValue(contextBean, out ownGameObject);
            }
            return ownGameObject;
        }

        public T InjectDependenciesTo<T>() where T : ISubBean
        {
            return InjectDependenciesTo<T>(typeof(T));
        }

        public T InjectDependenciesTo<T>(Type beanType) where T : ISubBean
        {
            if (typeof(MonoBehaviour).IsAssignableFrom(beanType))
            {
                throw new InjectroException("Prototype MonoBehaviour beans not supported.");
            }

            T bean = (T) Activator.CreateInstance(beanType);

            return InjectDependenciesTo<T>(bean);
        }
        
        public T InjectDependenciesTo<T>(T obj) where T : ISubBean
        {
            InjectBeanFields(obj, null);
            if (obj is IPostInject postResolvedBean)
            {
                postResolvedBean.PostInject();
            }

            return (T) obj;
        }

        public T CreatePrototype<T>(IBean masterBean, Transform parent = null) where T : IBean
        {
            return CreatePrototype<T>(Vector3.zero, Quaternion.identity, masterBean, parent);
        }

        public T CreatePrototype<T>(Vector3 position, Quaternion rotation, IBean masterBean,  Transform parent = null) where T : IBean
        {                      
            return CreatePrototype<T>(position, rotation, parent, GetOwnGameObject(masterBean));    
        }

        public T CreatePrototype<T>(Transform parent = null, GameObject masterGameObject = null) where T : IBean
        {
            return CreatePrototype<T>(Vector3.zero, Quaternion.identity, parent, masterGameObject);
        }

        public T CreatePrototypeFromPrefab<T>(GameObject prefab, Transform parent, IBean masterBean) where T : IBean
        {
            return CreatePrototypeFromPrefab<T>(prefab, parent, GetOwnGameObject(masterBean));
        }

        public T CreatePrototypeFromPrefab<T>(GameObject prefab, Transform parent = null, GameObject masterGameObject = null) where T : IBean
        {
            return CreatePrototypeFromPrefab<T>(prefab, Vector3.zero, Quaternion.identity, parent, masterGameObject);
        }

        public T CreatePrototypeFromPrefab<T>(GameObject prefab, Vector3 position, Quaternion rotation, Transform parent,
            IBean masterBean = null) where T : IBean
        {
            return CreatePrototypeFromPrefab<T>(prefab, Vector3.zero, Quaternion.identity, parent, GetOwnGameObject(masterBean));
        }

        public T CreatePrototypeFromPrefab<T>(
            GameObject prefab, 
            Vector3 position, 
            Quaternion rotation, 
            Transform parent = null,
            GameObject masterGameObject = null) 
            where T : IBean
        {
            PoolConfig config;
            if (!poolConfigsByName.TryGetValue(prefab.name, out config))
            {
                config = InitPoolingPrefab(prefab);
                InitCustomConfig(config);                
            }
            return CreatePrototype<T>(position, rotation, parent, masterGameObject, config);
        }
        
        public T CreatePrototype<T>(
            Vector3 position, 
            Quaternion rotation, 
            Transform parent = null,
            GameObject masterGameObject = null, 
            PoolConfig poolConfig = null) 
            where T : IBean
        {
            if (poolConfig == null)
            {
                if (!beanToPoolConfig.TryGetValue(typeof(T), out poolConfig))
                {
                    LogAdditionalInfo();
                    throw new NoSuchBeanException("Cannot allocate prefab with provided IBean: " + typeof(T));
                }
                
                if (beanToPoolConfig.ValuesCount(typeof(T)) > 1)
                {
                    Debug.LogErrorFormat("Injectro: Resolving duplicate prefabs for type: {0}", typeof(T));
                }
            }
            return GetFromPool<T>(poolConfig.Prefab.name, position, rotation, parent, masterGameObject);
        }               

        public PoolConfig InitPoolingPrefab(Component prefab, int initialCount = 2)            
        {
            return InitPoolingPrefab(prefab.gameObject, initialCount);
        }

        public PoolConfig InitPoolingPrefab(GameObject prefab, int initialCount = 2)
        {
            if (prefab.name.Contains("(Clone)"))
            {
                throw new InjectroException("Prefab based objects are only allowed");
            }

            PoolConfig config = new PoolConfig
            {
                Prefab = prefab,
                InitialCount = initialCount
            };

            Component[] components = prefab.GetComponentsInChildren<Component>(true);
            foreach (Component component in components)
            {
                if (!(component is IBean))
                {
                    continue;
                }
                config.IBeanComponents.Add(component.GetType());
                foreach (Type beanType in GetBeanTypes((IBean)component))
                {
                    beanToPoolConfig.Add(beanType, config);
                    config.BeanTypes.Add(beanType);
                }
            }                
            return config;
        }

        public bool AddBeforeReturnEvent(IBean bean, ContextEventProcessor.ContextEvent contextEvent)
        {
            if (bean == null)
                return false;

            GameObject ownGameObject = GetOwnGameObject(bean);
            if (ownGameObject == null)
                return false;

            return AddBeforeReturnEvent(ownGameObject, contextEvent, bean);
        }

        public bool AddBeforeReturnEvent(GameObject ownGameObject, ContextEventProcessor.ContextEvent contextEvent, IBean bean = null)
        {
            if (ownGameObject == null)
                return false;
            
            if (itemsInUse.ContainsKey(ownGameObject))
            {
                beforReturnEventProcessor.AddEvent(ownGameObject, contextEvent, bean);
                return true;
            }
            return false;
        }
        
        public bool ReturnBean(GameObject beanObject)
        {                        
            if (beanObject != null && itemsInUse.ContainsKey(beanObject))
            {
                string poolName = itemsInUse[beanObject];
                PoolConfig config = poolConfigsByName[poolName];
                
                LinkedList<IBean> beans = new LinkedList<IBean>();
                foreach (Type type in config.IBeanComponents)
                {
                    Component[] components = beanObject.GetComponentsInChildren(type, true);
                    if (components == null)
                        continue;                        

                    foreach (Component component in components)
                    {
                        IBean bean = component as IBean;
                        beans.AddLast(bean);
                    }
                }

                // It is important to call PreReturned() on each bean first, and then start to clear them
                foreach (IBean bean in beans)
                {
                    IPreReturned preReturnedBean = bean as IPreReturned;
                    if (preReturnedBean != null)
                    {
                        preReturnedBean.PreReturned();
                    }
                }
                
                ReturnSlaveBeans(beanObject);

                beforReturnEventProcessor.InvokeEvents(beanObject);

                beanObject.SetActive(false);
                config.ItemsInUse -= 1;


                
                foreach (IBean bean in beans)
                {
                    ClearBeanFields(bean);
                    prototypeBeansToOwnGameObject.Remove(bean);
                }

                gameObjectToLocalContexts.Remove(beanObject);
                
                itemsInUse.Remove(beanObject);

                storage[poolName].Add(beanObject);
                RectTransform rect = beanObject.transform as RectTransform;
                if (rect != null)
                    rect.SetParent(config.Wrapper.transform, false);
                else
                {
                    beanObject.transform.parent = config.Wrapper.transform;
                }
                beanObject.transform.position = Vector3.zero;
                itemInUseCount--;
                
                
                return true;
            }
            return false;
        }

        public PoolConfig GetConfig(GameObject prefab)
        {
            return GetConfig(prefab.name);
        }

        public PoolConfig GetConfig(string poolName)
        {
            PoolConfig config;
            poolConfigsByName.TryGetValue(poolName, out config);
            return null;
        }

        public bool ReturnBean(IBean bean)
        {
            return ReturnBean(GetOwnGameObject(bean));
        }        

        public bool ReturnBeanWithDelay(IBean bean, float timeDelay)
        {
            return ReturnBeanWithDelay(GetOwnGameObject(bean), timeDelay);
        }

        public bool ReturnBeanWithDelay(GameObject o, float timeDelay)
        {
            if (itemsInUse.ContainsKey(o))
            {
                StartCoroutine(ReturnBeanEnumerator(o, timeDelay));
                return true;
            }
            return false;
        }

        public GameObject PrefabOf(GameObject pooledObject)
        {
            if (pooledObject != null && itemsInUse.ContainsKey(pooledObject))
            {
                string poolName = itemsInUse[pooledObject];
                PoolConfig config = poolConfigsByName[poolName];
                return config.Prefab;
            }

            return null;
        }

        public bool IsTest()
        {
            return IsTestContext;
        }
        
        public StringBuilder DebugContext()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Singltones: \n");
            foreach (Type singletonContextKey in singletonContext.Keys)
            {
                sb.Append("\t").Append(singletonContextKey).Append(": \n");

                foreach (IBean bean in singletonContext.ValuesByKey(singletonContextKey))
                {
                    sb.Append("\t \t").AppendFormat("{0} (id = {1}),", bean, ((Component)bean).GetInstanceID());
                    sb.Append("\n");
                }
                
            }
            
            sb.Append("Prototypes: \n");

            foreach (KeyValuePair<GameObject,MultiMap<Type,IBean>> objectToContext in gameObjectToLocalContexts)
            {              
                sb.Append("\t")
                    .AppendFormat("{0} (id = {1}),",objectToContext.Key.name, objectToContext.Key.GetInstanceID())
                    .Append(": \n");
                foreach (Type type in objectToContext.Value.Keys)
                {
                    sb.Append("\t \t").Append(type).Append(": \n");
                    foreach (IBean bean in objectToContext.Value.ValuesByKey(type))
                    {
                        sb.Append("\t \t \t").AppendFormat("{0} (id = {1}),", bean, ((Component)bean).GetInstanceID());
                        sb.Append("\n");
                    }                    
                }
            }

            sb.Append("Context tree: \n");
            foreach (GameObject o in gameObjectToLocalContexts.Keys)
            {
                GameObject master;
                if (!childToMaster.TryGetValue(o, out master) || !gameObjectToLocalContexts.Keys.Contains(master))
                {
                    DebugDependencyTree(sb, o, 1);
                }
            }

            return sb;
        }

        protected virtual GameObject InstatiateGameObject(
            GameObject prefab,
            Vector3 position, 
            Quaternion rotation, 
            Transform parent)
        {
            return Instantiate(prefab, position, rotation, parent);
        }  
        
        protected void InitBeans()
        {
            InitPrototypeConfigs();
            InitSingletons();            
        }

        protected void SetGlobalContext(MultiMap<Type, IBean> context)
        {
            if (globalContext.Count == 0)
            {
                globalContext = context;
            }
            else
            {
                throw new InjectroException("GlobalContext is already set");;
            }
        }
        
        protected virtual void ScannedSingletonDestroyOnLoad(GameObject sceneGameObject)
        {
            // to be overriden
        }
        
        private T GetFromPool<T>(
            string poolName, 
            Vector3 position, 
            Quaternion rotation, 
            Transform parent = null, 
            GameObject masterBeanGameObject = null) 
            where T : IBean
        {
            if (!storage.ContainsKey(poolName))
            {
                throw new InjectroException("No such pool named " + poolName);
            }
            IBean returnBean = null;
            List<GameObject> vacantObjects = storage[poolName];

            PoolConfig config = poolConfigsByName[poolName];
            if (vacantObjects.Count == 0)
            {
                if (config.ItemsInUse < config.MaximumCount)
                {
                    int newCount = Mathf.Min(config.MaximumCount, config.ItemsInUse * 2);
                    for (int i = 0; i < newCount - config.ItemsInUse; i++)
                    {
                        GameObject o = InitObject(config);
                        vacantObjects.Add(o);
                    }
                }
                else
                {
                    if (IsDebug)
                    {
                        Debug.LogWarning(String.Format("InjectroContext: context, named {0} exceed maximum object count {1}", poolName, config.MaximumCount));
                    }
                    
                    return (T)(IBean)null;
                }
            }

            if (vacantObjects.Count > 0)
            {
                GameObject vacantObject = vacantObjects[vacantObjects.Count - 1];
                vacantObject.transform.SetParent(parent, true);
                vacantObject.transform.position = position;
                vacantObject.transform.rotation = rotation;
                itemsInUse.Add(vacantObject, poolName);
                   
                vacantObjects.Remove(vacantObject);
                config.ItemsInUse += 1;                

                MultiMap<Type, IBean> localContext = new MultiMap<Type, IBean>();                                               
                gameObjectToLocalContexts.Add(vacantObject, localContext);
                
                if (masterBeanGameObject != null)
                {
                    LinkedList<GameObject> childList;
                    if (!masterToChild.TryGetValue(masterBeanGameObject, out childList))
                    {
                        childList = new LinkedList<GameObject>();
                        masterToChild.Add(masterBeanGameObject, childList);
                    }
                    childList.AddLast(vacantObject);
                    childToMaster.Add(vacantObject, masterBeanGameObject);                   
                }
                List<IBean> objectBeans = new List<IBean>();
                foreach (Type type in config.IBeanComponents)
                {
                    Component[] components = vacantObject.GetComponentsInChildren(type, true);
                    if (components == null)
                        continue;                        

                    foreach (Component component in components)
                    {
                        IBean beanComponent = component as IBean;
                        objectBeans.Add(beanComponent); 
                        prototypeBeansToOwnGameObject.Add(beanComponent, vacantObject);
                        
                        foreach (Type baseType in config.BeanTypes)
                        {
                            if (!baseType.IsInstanceOfType(component))
                                continue;
                            
                            localContext.Add(baseType, beanComponent);
                            if (baseType == typeof(T))
                            {
                                returnBean = beanComponent;
                            }
                        }                    
                    }
                }

                foreach (IBean bean in objectBeans)
                {
                    InjectBeanFields(bean);
                }

                foreach (IBean bean in objectBeans)
                {
                    IPostResolved postResolvedBean = bean as IPostResolved;
                    if (postResolvedBean != null)
                    {
                        postResolvedBean.PostResolved();                    
                    }
                }
                
                itemInUseCount++;
                
                // It is important to activate object after fields are injected and after PostResolved is invoked. 
                // Activation will lead to Start() invokation.
                vacantObject.SetActive(true);  
                return (T)returnBean;
            }

            throw new InjectroException("No such pool named " + poolName);
        }
        
        private void DebugDependencyTree(StringBuilder sb, GameObject indexObject, int deep)
        {
            for (int i = 0; i < deep; i++)
            {
                sb.Append("\t");
            }
            sb.AppendFormat("{0} (id = {1}),\n", indexObject.name, indexObject.GetInstanceID());
            LinkedList<GameObject> children;
            if (masterToChild.TryGetValue(indexObject, out children) )
            {
                foreach (GameObject o in children)
                {
                    DebugDependencyTree(sb, o, deep + 1);                
                }
            }            
        }

        private void InitPrototypeConfigs()
        {
            foreach (GameObject prefab in PrototypeBeanPrefabs)
            {
                PoolConfig config = InitPoolingPrefab(prefab);
                InitCustomConfig(config);
            }
        }

        private void InitSingletons()
        {
            List<GameObject> createdSingletonsGameObjects = new List<GameObject>();
            foreach (GameObject beanPrefab in SingletonBeanPrefabs)
            {
                GameObject beanPrefabGameObject = InstatiateGameObject(beanPrefab.gameObject, Vector3.zero, Quaternion.identity, gameObject.transform);
                
                if (beanPrefab.gameObject.activeSelf)
                {
                    Debug.LogErrorFormat("Prefab {0} .activeSelf should be set to false. Otherwise invokation order will be violated.", beanPrefab.gameObject.name);
                }

                createdSingletonsGameObjects.Add(beanPrefabGameObject);
            }

            AddToSingletonContext(this); // Add the context itself like a singleton bean. To be able to [Inject] context while you are inside of context.
            
            List<GameObject> gameObjectsToScan = new List<GameObject>(gameObject.scene.GetRootGameObjects());
            gameObjectsToScan.Remove(this.gameObject);

            List<GameObject> beanGameObjects = new List<GameObject>();
            beanGameObjects.AddRange(SingletonBeanPresetObjects);
            beanGameObjects.AddRange(createdSingletonsGameObjects);

            foreach (GameObject beanGameObject in beanGameObjects)
            {
                gameObjectsToScan.Remove(beanGameObject);
                
                if (beanGameObject.gameObject.activeSelf)
                {
                    Debug.LogErrorFormat("Singleton's {0} .activeSelf should be set to false. Otherwise invokation order will be violated.", beanGameObject.gameObject.name);
                }

                Component[] components = beanGameObject.GetComponentsInChildren<Component>(true);
                foreach (Component component in components)
                {                           
                    if (component is IBean)
                    {
                        AddToSingletonContext(component as IBean);
                    }
                }                      
            }

            IEnumerable<GameObject> foundedBeanGameObjectsAfterScan = ScanForSingletons(gameObjectsToScan);
            beanGameObjects.AddRange(foundedBeanGameObjectsAfterScan);

            foreach (IBean bean in singletonBeans)
            {
                InjectBeanFields(bean);
            }

            PostResolve(singletonBeans);

            
            // It is important to activate object after fields are injected and after PostResolved is invoked. 
            // Activation will lead to Start() invokation.
            foreach (GameObject beanGameObject in beanGameObjects)
            {
                beanGameObject.SetActive(true);
            }
        }

        private void PostResolve(ICollection<IBean> beans)
        {
            Dictionary<Type, ICollection<Type>> beansTypesMap = new Dictionary<Type, ICollection<Type>>();
            HashSet<Type> beanTypes = new HashSet<Type>();
            
            foreach (IBean bean in beans)
            {
                ICollection<Type> bt = GetBeanTypes(bean);
                
                try
                {
                    beansTypesMap.Add(bean.GetType(), bt);
                }
                catch (ArgumentException)
                {
                    //ignore
                }
                
                foreach (Type t in bt)
                {
                    beanTypes.Add(t);
                }
            }
            ICollection<IBean> resolvedBeans = new HashSet<IBean>();
            //beans 1st level dependencies
            Dictionary<IBean, HashSet<Type>> beansDependencies = new Dictionary<IBean, HashSet<Type>>();
            //beans injections
            Dictionary<IBean, HashSet<Type>> beansInjects = new Dictionary<IBean, HashSet<Type>>();

            foreach (IBean bean in beans)
            {
                if (!(bean is IPostResolved) || globalContext.ContainsKey(bean.GetType()))
                {
                    resolvedBeans.Add(bean);
                    continue;
                }
                IPostResolved postResolvingBean = bean as IPostResolved;
                HashSet<Type> dependencies = GetDependencies(postResolvingBean);
                if (null == dependencies)
                {
                    ICollection<FieldInfo> fieldsWithAttribute = GetFieldsWithAttribute(postResolvingBean.GetType(), IocFieldsFlags, typeof(InjectAttribute));
                    HashSet<Type> injects = new HashSet<Type>();
                    foreach (FieldInfo fieldInfo in fieldsWithAttribute)
                    {
                        injects.Add(fieldInfo.FieldType);
                    }
                    
                    if (0 == injects.Count)
                    {
                        resolvedBeans.Add(postResolvingBean);
                    }
                    else
                    {
                        HashSet<Type> intersects = new HashSet<Type>();
                        foreach (Type inject in injects)
                        {
                            foreach (Type beanType in beanTypes)
                            {
                                if (inject == beanType)
                                {
                                    intersects.Add(beanType);
                                    break;
                                }
                            }
                        }
                        beansInjects.Add(postResolvingBean, intersects);
                    }
                }
                else
                {
                    HashSet<Type> intersects = new HashSet<Type>();
                    foreach (Type dependency in dependencies)
                    {
                        foreach (Type beanType in beanTypes)
                        {
                            if (dependency == beanType)
                            {
                                intersects.Add(dependency);
                                break;
                            }
                        }
                    }
                    beansDependencies.Add(postResolvingBean, intersects);
                }
            }
            
            //resolve beans without dependencies
            foreach (IBean resolvedBean in resolvedBeans)
            {
                IPostResolved prBean = resolvedBean as IPostResolved;
                if (prBean != null)
                {
                    prBean.PostResolved();
                }
            }

            while (0 != resolvedBeans.Count)
            {
                foreach (IBean bean in resolvedBeans)
                {
                    ICollection<Type> types = null;
                    if (!beansTypesMap.TryGetValue(bean.GetType(), out types))
                    {
                        throw new InjectroException("Should not happened.");
                    }
                    beansDependencies.Remove(bean);
                    foreach (KeyValuePair<IBean,HashSet<Type>> item in beansDependencies)
                    {
                        item.Value.RemoveWhere(itemValue => types.Contains(itemValue));
                    }
                    
                    beansInjects.Remove(bean);
                    foreach (KeyValuePair<IBean,HashSet<Type>> item in beansInjects)
                    {
                        item.Value.RemoveWhere(itemValue => types.Contains(itemValue));
                    }
                }
                //resolvedBeans all resolved and removes from dependencies, so clean it
                resolvedBeans.Clear();
                
                //replace beans without dependencies from beansDependencies to resolvedBeans
                HashSet<IBean> beansWd = new HashSet<IBean>();
                foreach (KeyValuePair<IBean,HashSet<Type>> beansDependency in beansDependencies)
                {
                    beansWd.Add(beansDependency.Key);
                }
                foreach (KeyValuePair<IBean,HashSet<Type>> pair in beansInjects)
                {
                    beansWd.Add(pair.Key);
                }
                foreach (IBean bean in beansWd)
                {
                    HashSet<Type> dependencies = null;
                    HashSet<Type> injects = null;
                    if (beansDependencies.TryGetValue(bean, out dependencies))
                    {//directly setted depencies
                        if (null == dependencies || 0 == dependencies.Count)
                        {
                            resolvedBeans.Add(bean);
                        }
                    }
                    else if (beansInjects.TryGetValue(bean, out injects) && 0 == injects.Count)
                    {//no directly setted depencies -> look for injects
                        resolvedBeans.Add(bean);
                    }
                }
                
                //resolve beans without dependencies
                foreach (IBean bean in resolvedBeans)
                {
                    IPostResolved prBean = bean as IPostResolved;
                    if (prBean != null)
                    {
                        prBean.PostResolved();
                    }
                }
            }
            if (0 != beansDependencies.Count)
            {
                string dependenciesMessage = "";
                foreach (KeyValuePair<IBean,HashSet<Type>> dependency in beansDependencies)
                {
                    dependenciesMessage += dependency.Key + ", ";
                }
                throw new InjectroException($"Can not post resolve beans: [{dependenciesMessage}].\nDependencies loop found.");
            }
            if (0 != beansInjects.Count)
            {
                string injectsMessage = "";
                foreach (KeyValuePair<IBean,HashSet<Type>> inject in beansInjects)
                {
                    injectsMessage += inject.Key + ", ";
                }
                
                if (IsDebug)
                {
                    Debug.LogWarning($"Can not post resolve beans: [{injectsMessage}].\nInjects loop found.");
                }
            }
        }

        private ICollection<FieldInfo> GetFieldsWithAttribute(Type type, BindingFlags flags, Type attribute)
        {
            HashSet<FieldInfo> fields = new HashSet<FieldInfo>();
            FieldInfo[] fieldInfos = type.GetFields(flags);
            for (int i = 0; i < fieldInfos.Length; i++)
            {
                FieldInfo fieldInfo = fieldInfos[i];
                if (attribute.IsInstanceOfType(Attribute.GetCustomAttribute(fieldInfo, attribute, false)))
                {
                    fields.Add(fieldInfo);
                }
            }
            return fields;
        }
        
        public T CastExamp1<T>(object input) {   
            return (T) input;   
        }
        
        private HashSet<Type> GetDependencies(IBean bean)
        {
            if (null == (bean as IPostResolved))
            {
                return null;
            }
            HashSet<Type> types = null;
            object[] customAttributes = bean.GetType().GetCustomAttributes(typeof(PostResolveDependencyAttribute), true);
            foreach (object dependency in customAttributes)
            {
                PostResolveDependencyAttribute postResolveDependencyAttribute = dependency as PostResolveDependencyAttribute;
                if (null != postResolveDependencyAttribute && null != postResolveDependencyAttribute.Dependencies)
                {
                    if (null == types)
                    {
                        types = new HashSet<Type>();
                    }
                    for (int i = 0; i < postResolveDependencyAttribute.Dependencies.Length; i++)
                    {
                        Type type = postResolveDependencyAttribute.Dependencies[i];
                        types.Add(type);
                    }
                }
            }
            return types;
        }

        private List<GameObject> ScanForSingletons(List<GameObject> gameObjectsToScan)
        {
            List<GameObject> foundBeanGameObjects = new List<GameObject>();
            foreach (GameObject scanObject in gameObjectsToScan)
            {
                Component[] topComponents = scanObject.GetComponents<Component>();
                bool isSingletonBean = false;
                foreach (Component beanComponent in topComponents)
                {
                    if (beanComponent == null)
                    {
                        throw new MissingComponentException(
                            String.Format("GameObject ({0}) have some missing components", scanObject));
                    }
                    
                    SingletonAttribute customAttribute = (SingletonAttribute)Attribute.GetCustomAttribute(
                        beanComponent.GetType(), 
                        typeof(SingletonAttribute), false);
                    
                    if (customAttribute != null)
                    {
                        isSingletonBean = true;
                        break;                        
                    }
                }
                
                if (isSingletonBean)
                {
                    foundBeanGameObjects.Add(scanObject);
                    Component[] components = scanObject.GetComponentsInChildren<Component>();
                    foreach (Component beanComponent in components)
                    {
                        if (typeof(IBean).IsAssignableFrom(beanComponent.GetType()) &&
                            !singletonBeans.Contains((IBean) beanComponent))
                        {
                            AddToSingletonContext((IBean) beanComponent);
                        }
                    }
                    
                    ScannedSingletonDestroyOnLoad(scanObject);
                    
                }
            }
            return foundBeanGameObjects;
        }

        private ICollection<Type> GetBeanTypes(IBean bean)
        {
            Type currentType = bean.GetType();            
            List<Type> baseTypes = new List<Type>(currentType.GetInterfaces()); 
            while (currentType != null 
                   && !IgnoredTypes.Contains(currentType)
                   && typeof(IBean).IsAssignableFrom(currentType))
            {
                baseTypes.Add(currentType);
                currentType = currentType.BaseType;
            }

            for (int i = baseTypes.Count - 1; i <= 0; i--)
            {
                Type baseType = baseTypes[i];
                if (IgnoredTypes.Contains(baseType) || typeof(IBean).IsAssignableFrom(baseType))
                {
                    baseTypes.Remove(baseType);
                }
            }

            return baseTypes;
        }
        
        private void AddToSingletonContext(IBean bean)
        {
            foreach (Type beanType in GetBeanTypes(bean))
            {
                singletonContext.Add(beanType, bean);
                singletonBeans.Add(bean);
            }
        }

        private void InjectBeanFields(IBean bean)
        {
            InjectBeanFields(bean, bean);
        }

        private void InjectBeanFields(object bean, IBean contextBean)
        {
            Type currentType = bean.GetType();
            ICollection<FieldInfo> fieldInfos = GetFieldsWithAttribute(currentType, IocFieldsFlags, typeof(InjectAttribute));
            
            while (currentType != null 
                   && (typeof(IBean).IsAssignableFrom(currentType) || typeof(ISubBean).IsAssignableFrom(currentType)))
            {
                foreach (FieldInfo fieldInfo in fieldInfos)
                {
                    InjectAttribute customAttribute = (InjectAttribute)Attribute.GetCustomAttribute(fieldInfo, typeof(InjectAttribute), false);
    
                    if (customAttribute != null)
                    {
                        Type injectType = fieldInfo.FieldType;
                        object fieldValue;
                        
                        if (injectType.IsGenericType && (injectType.GetGenericTypeDefinition() == typeof(ICollection<>) || injectType.GetGenericTypeDefinition() == typeof(IList<>)))
                        {
                            //
                            Type genericArgument = injectType.GetGenericArguments()[0];  
                            
                            if (IsDebug)
                            {
                                Debug.LogFormat("Processing bean = {0}, [Inject]ing = {1}", bean, genericArgument);
                            }
                            
                            MethodInfo method = typeof(BaseInjectroContext).GetMethod("GetBeans", new[] {typeof(IBean)});
                            MethodInfo generic = method.MakeGenericMethod(genericArgument);
                            fieldValue = generic.Invoke(this, new[] {(object)bean});                        
                        }
                        else if (injectType.IsArray)
                        {
                            if (IsDebug)
                            {
                                Debug.LogFormat("Processing bean = {0}, [Inject]ing = {1}", bean, injectType.GetElementType());
                            }
                            
                            ICollection<IBean> collection = GetBeans(injectType.GetElementType(), contextBean);
                            Array array = Array.CreateInstance(injectType.GetElementType(), collection.Count);
                            int i = 0;
                            foreach (IBean bean1 in collection)
                            {
                                array.SetValue(bean1, i++);                            
                            }
                            fieldValue = array;
                        }
                        else
                        {
                            if (IsDebug)
                            {
                                Debug.LogFormat("Processing bean = {0}, [Inject]ing = {1}", bean, injectType);
                            }
                            
                            fieldValue = GetBean(injectType, contextBean);
                        }
                        
                        fieldInfo.SetValue(bean, fieldValue);                        
                    }
                }                
                
                currentType = currentType.BaseType;
                if (currentType != null)
                {
                    fieldInfos = currentType.GetFields(BindingFlags.Instance | BindingFlags.NonPublic);
                }
            }
        }       
        
        private void ClearBeanFields(IBean bean) // TODO do it DRY
        {
            ICollection<FieldInfo> fieldsWithAttribute = GetFieldsWithAttribute(bean.GetType(), IocFieldsFlags, typeof(InjectAttribute));
            foreach (FieldInfo fieldInfo in fieldsWithAttribute)
            {
                fieldInfo.SetValue(bean, null);
            }
        }

        private void InitCustomConfig(PoolConfig config)
        {
            string poolName = config.Prefab.name;
            GameObject wrapper = new GameObject(poolName + "_Pool");
            config.Wrapper = wrapper;
            wrapper.transform.SetParent(transform, false);

            wrapper.transform.position = Vector3.zero;

            List<GameObject> list = new List<GameObject>(config.InitialCount);
            storage.Add(poolName, list);

            for (int i = 0; i < config.InitialCount; i++)
            {
                var o = InitObject(config);
                list.Add(o);
            }            

            poolConfigsByName.Add(poolName, config);
        }

        private GameObject InitObject(PoolConfig config)
        {           
            GameObject o = InstatiateGameObject(config.Prefab, Vector3.zero, Quaternion.identity, config.Wrapper.transform);
            if (config.Prefab.activeSelf)
            {
                Debug.LogErrorFormat("Prefab {0} .activeSelf should be set to false. Otherwise invokation order will be violated.", config.Prefab.name);
                
                o.SetActive(false);
            }  
            return o;
        }
        
        private void FindFarBeans<T>(Action<T> action, IBean closestMasterBean = null, bool singleResult = true) where T : IBean
        {
            if (!beanToPoolConfig.ContainsKey(typeof(T)))     
            {
                throw new NoSuchBeanException("Cannot find related pool config to bean type: " + typeof(T));
            }
            ICollection<PoolConfig> poolConfigs = beanToPoolConfig.Values;
            foreach (KeyValuePair<GameObject,string> pair in itemsInUse)
            {
                GameObject indexGameObject = pair.Key;
                
                while (true)
                {
                    MultiMap<Type, IBean> beanLocalContext = GetBeanLocalContext(indexGameObject);
                    ICollection<T> valuesByKey = beanLocalContext.ValuesByKey<T>(typeof(T));
                    foreach (T bean in valuesByKey)
                    {
                        action.Invoke(bean);
                        if (singleResult)
                        {
                            return;
                        }
                    }
                    IBean candidateBean;
                    
                    if (indexGameObject == null 
                        || closestMasterBean != null 
                        && beanLocalContext.TryGetValue(closestMasterBean.GetType(), out candidateBean) 
                        && closestMasterBean.Equals(candidateBean))
                    {
                        break;
                    }
                    
                    childToMaster.TryGetValue(indexGameObject, out indexGameObject);
                }                                   
            }            
        }

        private Object GetBean(Type type, IBean contextBean = null)
        {
            GameObject ownGameObject = GetOwnGameObject(contextBean);
            return GetBean(type, ownGameObject);
        }
        
        private ICollection<IBean> GetBeans(Type type, IBean contextBean = null)
        {
            GameObject ownGameObject = GetOwnGameObject(contextBean);                       
            return GetBeans<IBean>(type, ownGameObject); 
        }
        
        private Object GetBean(Type type, GameObject ownBeanGameObject = null)
        {
            GameObject indexBeanGameObject = ownBeanGameObject;
            IBean bean;
            if (GetBeanLocalContext(indexBeanGameObject).TryGetValue(type, out bean))
            {
                return bean;
            }               

            while (indexBeanGameObject != null)
            {
                childToMaster.TryGetValue(indexBeanGameObject, out indexBeanGameObject); 
                if (GetBeanLocalContext(indexBeanGameObject).TryGetValue(type, out bean))
                {
                    return bean;
                }               
            }
            
            if (globalContext != null && globalContext.Count > 0 && globalContext.TryGetValue(type, out bean))
            {                
                return bean;
            }
            
            LogAdditionalInfo();
            throw new NoSuchBeanException("Couldn't resolve bean by type: " + type);
        }
        
        private ICollection<T> GetBeans<T>(Type type, GameObject ownBeanGameObject = null) where T:IBean
        {
            List<T> resultBeans = new List<T>();
            GameObject indexBeanGameObject = ownBeanGameObject;
            
            ICollection<T> valuesByKey = GetBeanLocalContext(ownBeanGameObject).ValuesByKey<T>(type);
            resultBeans.AddRange(valuesByKey);                           

            while (indexBeanGameObject != null)
            {
                childToMaster.TryGetValue(indexBeanGameObject, out indexBeanGameObject);            
                valuesByKey = GetBeanLocalContext(indexBeanGameObject).ValuesByKey<T>(type);
                resultBeans.AddRange(valuesByKey);               
            }

            if (globalContext.Count > 0)
            {
                valuesByKey = globalContext.ValuesByKey<T>(type);
                resultBeans.AddRange(valuesByKey);
            }
            
            return resultBeans;                        
        }
        
        private  MultiMap<Type, IBean> GetBeanLocalContext(GameObject ownBeanGameObject = null)
        {
            if (ownBeanGameObject != null)
            {
                MultiMap<Type, IBean> gameObjectToLocalContext;
                gameObjectToLocalContexts.TryGetValue(ownBeanGameObject, out gameObjectToLocalContext);
                if (gameObjectToLocalContext != null) 
                    return gameObjectToLocalContext;
            }
            else
            {
                return singletonContext;
            }
            LogAdditionalInfo();
            throw new InjectroException("No such context for gameObject: " + ownBeanGameObject);
        }
        
        private void ReturnSlaveBeans(GameObject o)
        {
            LinkedList<GameObject> subGameObjects;
            if (masterToChild.TryGetValue(o, out subGameObjects))
            {
                foreach (GameObject subGameObject in subGameObjects)
                {
                    ReturnBean(subGameObject);
                    childToMaster.Remove(subGameObject);
                }
                masterToChild.Remove(o);
            }
        }
        
        private IEnumerator ReturnBeanEnumerator(GameObject o, float timeDelay)
        {
            yield return new WaitForSeconds(timeDelay);
            ReturnBean(o);
        }

        private void LogAdditionalInfo()
        {
            if (!IsDebug && !Debug.isDebugBuild) 
                return;
            
            Debug.LogWarning(DebugContext().ToString());
        }
    }
}