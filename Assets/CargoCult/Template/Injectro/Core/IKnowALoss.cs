﻿using UnityEngine;

namespace CargoCult.Template.Injectro.Core
{
    public interface IKnowALoss : IBean
    {
        void KnowALoss(GameObject lossGameObject, IBean lossBean);
    }
}