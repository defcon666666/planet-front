﻿namespace CargoCult.Template.Injectro.Core
{
    public interface IPostResolved : IBean
    {
        /// <summary>
        /// Calls just after bean has been resolved, all [Inject]s are applied, and ready to work. 
        /// </summary>
        void PostResolved();
    }
}