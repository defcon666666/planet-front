﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace CargoCult.Template.Injectro.Core
{
    public interface InjectroContext : IBean
    {
        void MakeBeanKnowALoss<T>(T bean, IBean lossingBean) where T: IBean, IKnowALoss;
        
        void MakeBeanKnowALoss<T>(T bean, GameObject lossingObject, IBean lossingBean = null) where T: IBean, IKnowALoss;
        
        T FarBean<T>(IBean closestMasterBean = null) where T : IBean;
        
        ICollection<T> FarBeans<T>(IBean closestMasterBean = null) where T : IBean;
        
        T GetBean<T>(IBean contextBean) where T : IBean;
        
        T GetBean<T>(GameObject ownBeanGameObject = null) where T : IBean;
        
        ICollection<T> GetBeans<T>(IBean contextBean)  where T : IBean;
        
        ICollection<T> GetBeans<T>(GameObject ownBeanGameObject = null) where T : IBean;
        
        GameObject GetOwnGameObject(IBean contextBean);

        T InjectDependenciesTo<T>() where T : ISubBean;

        T InjectDependenciesTo<T>(Type beanType) where T : ISubBean;

        T InjectDependenciesTo<T>(T obj) where T : ISubBean;
        
        T CreatePrototype<T>(IBean masterBean, Transform parent = null) where T : IBean;
        
        T CreatePrototype<T>(Vector3 position, Quaternion rotation, IBean masterBean, Transform parent = null) where T : IBean;
        
        T CreatePrototype<T>(Transform parent = null, GameObject masterGameObject = null) where T : IBean;
        
        T CreatePrototypeFromPrefab<T>(GameObject prefab, Transform parent = null,  GameObject masterGameObject = null) where T : IBean;
        
        T CreatePrototypeFromPrefab<T>(GameObject prefab, Transform parent,  IBean masterBean) where T : IBean;

        T CreatePrototypeFromPrefab<T>(
            GameObject prefab, 
            Vector3 position, 
            Quaternion rotation, 
            Transform parent = null,
            GameObject masterGameObject = null) 
            where T : IBean;

        T CreatePrototypeFromPrefab<T>(
            GameObject prefab, 
            Vector3 position, 
            Quaternion rotation, 
            Transform parent,
            IBean masterBean = null) 
            where T : IBean;

        T CreatePrototype<T>(
            Vector3 position, 
            Quaternion rotation, 
            Transform parent = null,  
            GameObject masterGameObject = null, 
            BaseInjectroContext.PoolConfig poolConfig = null) 
            where T : IBean;
        
        BaseInjectroContext.PoolConfig InitPoolingPrefab(GameObject prefab, int initialCount = 2);
        
        bool AddBeforeReturnEvent(IBean bean, ContextEventProcessor.ContextEvent contextEvent);
        
        bool AddBeforeReturnEvent(GameObject ownGameObject, ContextEventProcessor.ContextEvent contextEvent, IBean bean = null);
        
        bool ReturnBean(GameObject beanObject);
        
        bool ReturnBean(IBean bean);
        
        bool ReturnBeanWithDelay(IBean bean, float timeDelay);
        
        bool ReturnBeanWithDelay(GameObject o, float timeDelay);
        
        GameObject PrefabOf(GameObject pooledObject);

        bool IsTest();
    }
}