﻿using System;

namespace CargoCult.Template.Injectro.Core
{
    [AttributeUsage(AttributeTargets.Field)]
    public class InjectAttribute : Attribute
    {
        
    }
}