﻿using CargoCult.Template.Common.PanelManager.UI;

namespace CargoCult.Template.Local.Menu
{
    public interface IMenuUIController : IUIController
    {
        void StartClicked();
    }
}
