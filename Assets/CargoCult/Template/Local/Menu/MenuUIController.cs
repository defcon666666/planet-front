﻿using CargoCult.Template.Common.PanelManager.UI;
using CargoCult.Template.Global.LoadScene;
using CargoCult.Template.Injectro.Core;
using CargoCult.Template.SelectiveStrings;
using UnityEngine;

namespace CargoCult.Template.Local.Menu
{
    public class MenuUIController : BaseUIController, IMenuUIController
    {
        [SerializeField][SelectiveString("BaseSceneNames")]
        private string sceneToLoad;

        [Inject] private ILoadSceneManager loadSceneManager;
        
        public void StartClicked()
        {
//            loadSceneManager.LoadScene(sceneToLoad);
            
            GameManager.instance.StartGameLevel();
        }
    }
}
