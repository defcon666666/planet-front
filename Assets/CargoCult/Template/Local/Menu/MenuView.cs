﻿using CargoCult.Template.Common.PanelManager;
using CargoCult.Template.Common.PanelManager.UI;
using CargoCult.Template.Injectro.Core;
using CargoCult.Template.Local.Game;
using CargoCult.Template.Local.SettingsPanel;
using UnityEngine;
using UnityEngine.UI;

namespace CargoCult.Template.Local.Menu
{
    [Singleton][PostResolveDependency(typeof(IPanelManager))]
    public class MenuView : BaseView, IMenuView
    {
        [Inject] private IMenuUIController controller;
        [Inject] private IGameView gameView;
        [Inject] private ISettingsView settingsView;
        [Inject] private IGameInfoView gameInfoView;

        
        public override void PostResolved()
        {
            base.PostResolved();
            
            Open();

        }


        public override void Open()
        {
            base.Open();
            gameInfoView.Open();
            GameManager.instance.StartGameMenu();
        }

        public override IUIController GetCurrentController()
        {
            return null;
        }

        public override ViewType GetViewType()
        {
            return ViewType.Content;
        }

        public void StartClick()
        {
            gameView.Open();
            
            GameManager.instance.StartGameLevel();

        }

        public void SettingsClick()
        {
            settingsView.Open();
        }
    }
}
