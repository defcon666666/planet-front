﻿using CargoCult.Template.Common.PanelManager.UI;
using CargoCult.Template.Injectro.Core;
using CargoCult.Template.Local.GameResult;
using CargoCult.Template.Local.Pause;
using UnityEngine;

namespace CargoCult.Template.Local.Game
{
    public class GameView : BaseView, IGameView
    {
        [Inject] private IGameUIController controller;
        [Inject] private IPauseView pauseView;
        [Inject] private IGameResultView gameResultView;

        public UpgradeView UpgradePanel;
        public Crosshair Cross;
        public PowerSlider Power;
        public GameObject TargetSelector;
        
        

        private Player player;
        
        public override void PostResolved()
        {
            base.PostResolved();
            
            Open();
            
        }

        public override void Open()
        {
            base.Open();
            player = TurnManager.instance.Player;
        }

        public override IUIController GetCurrentController()
        {
            return controller;
        }

        public override ViewType GetViewType()
        {
            return ViewType.Content;
        }

        public void OpenControlPanel(Player player,int enemy)
        {
            Power.Init(player.MaxDamage);
            UpgradePanel.Open();
            TargetSelector.SetActive(enemy > 1);
            Cross.gameObject.SetActive(enemy>1);
        }

        public void ShowShootPower(Player player)
        {
            Power.Init(player.MaxDamage);
        }

        public void CloseControlPanel()
        {
            UpgradePanel.Close();
            TargetSelector.SetActive(false);
            Cross.gameObject.SetActive(false);
        }

        public void LeftTargetClick()
        {
            player.LeftTarget();
        }
        public void RightTargetClick()
        {
            player.RightTarget();
        }
            

        public void PauseClick()
        {
            pauseView.Open();
        }
        
    }
}
