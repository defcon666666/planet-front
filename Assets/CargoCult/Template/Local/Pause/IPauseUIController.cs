﻿using CargoCult.Template.Common.PanelManager.UI;

namespace CargoCult.Template.Local.Pause
{
    public interface IPauseUIController : IUIController
    {
        void ExitClicked();
    }
}
