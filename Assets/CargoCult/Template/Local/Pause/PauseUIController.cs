﻿using CargoCult.Template.Common.PanelManager.UI;
using CargoCult.Template.Global.LoadScene;
using CargoCult.Template.Global.TimeScale;
using CargoCult.Template.Injectro.Core;
using CargoCult.Template.SelectiveStrings;
using UnityEngine;

namespace CargoCult.Template.Local.Pause
{
    public class PauseUIController : BaseUIController, IPauseUIController, ITimeScaleChanger
    {
        [SerializeField] [SelectiveString("BaseSceneNames")]
        private string sceneToLoad;

        [Inject] private ILoadSceneManager loadSceneManager;
        [Inject] private ITimeScaleManager timeScaleManager;

        public override void OnOpen()
        {
            base.OnOpen();
            
            timeScaleManager.Pause(this);
        }

        public override void OnClose()
        {
            timeScaleManager.Resume(this);
        }
        
        public void ExitClicked()
        {
            OnClose();
            loadSceneManager.LoadScene(sceneToLoad);
        }
    }
}