﻿using CargoCult.Template.Common.PanelManager.UI;
using CargoCult.Template.Injectro.Core;
using CargoCult.Template.Local.Game;

namespace CargoCult.Template.Local.Pause
{
    public class PauseView : BaseView, IPauseView
    {
        [Inject] private IPauseUIController controller;
        [Inject] private IGameView gameView;
        
        public override IUIController GetCurrentController()
        {
            return controller;
        }

        public override ViewType GetViewType()
        {
            return ViewType.Content;
        }

        public void ResumeClick()
        {
            gameView.Open();
        }

        public void ExitClick()
        {
            controller.ExitClicked();
        }
    }
}
