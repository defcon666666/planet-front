﻿using CargoCult.Template.Common.PanelManager.UI;

namespace CargoCult.Template.Local.GameResult
{
    public interface IGameResultUIController : IUIController
    {
        void OnWin();
        void OnLoose();
        void ReplayClicked();
        void NextClicked();
        void MenuClicked();
    }
}
