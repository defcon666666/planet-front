﻿using CargoCult.Template.Common.PanelManager.UI;

namespace CargoCult.Template.Local.GameResult
{
    public interface IGameResultView : IView
    {
        void OpenForLoose();
        void OpenForWin();
    }
}
