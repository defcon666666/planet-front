﻿using CargoCult.Template.Common.PanelManager.UI;
using CargoCult.Template.Global.Ads.AdsManager;
using CargoCult.Template.Global.LoadScene;
using CargoCult.Template.Global.Sound;
using CargoCult.Template.Global.TimeScale;
using CargoCult.Template.Injectro.Core;
using CargoCult.Template.SelectiveStrings;
using UnityEngine;

namespace CargoCult.Template.Local.GameResult
{
    public class GameResultUIController : BaseUIController, IGameResultUIController, ITimeScaleChanger
    {
        [SerializeField] [SelectiveString("BaseSceneNames")]
        private string menuScene;
        
        [SerializeField] [SelectiveString("BaseSceneNames")]
        private string gameScene;

        [Inject] private ITimeScaleManager timeScaleManager;
        [Inject] private ISoundManager soundManager;
        [Inject] private ILoadSceneManager loadSceneManager;
        [Inject] private IAdsManager adsManager;

        public override void OnOpen()
        {
            base.OnOpen();
            
//            timeScaleManager.Pause(this);
//            adsManager.ShowInterstitial();
        }

        public override void OnClose()
        {
            base.OnClose();
            
//            timeScaleManager.Resume(this);
        }

        public void OnWin()
        {
            soundManager.Play(SoundType.WinSound);
            AppMetrica.Instance.ReportEvent("Win");
        }

        public void OnLoose()
        {
            soundManager.Play(SoundType.LooseSound);
            AppMetrica.Instance.ReportEvent("Loose");
        }

        public void ReplayClicked()
        {
            OnClose();
            loadSceneManager.LoadScene(gameScene);
        }

        public void NextClicked()
        {
            OnClose();
            loadSceneManager.LoadScene(gameScene);
        }

        public void MenuClicked()
        {
            OnClose();
            loadSceneManager.LoadScene(menuScene);
        }
    }
}