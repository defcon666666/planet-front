﻿using CargoCult.Template.Common.PanelManager.UI;
using CargoCult.Template.Global.Ads.Admob;
using CargoCult.Template.Global.Ads.AdsManager;
using CargoCult.Template.Global.Sound;
using CargoCult.Template.Injectro.Core;
using CargoCult.Template.Local.Menu;
using UnityEngine;

namespace CargoCult.Template.Local.GameResult
{
    public class GameResultView : BaseView, IGameResultView
    {
        [SerializeField] private GameObject winRootContent;
        [SerializeField] private GameObject looseRootContent;
        
        [Inject] private IGameResultUIController controller;
        [Inject] private IAdsManager adsManager;
        [Inject] private ISoundManager soundManager;
        [Inject] private IMenuView menu;
        
        public void OpenForLoose()
        {
            Open();
            looseRootContent.gameObject.SetActive(true);
            winRootContent.gameObject.SetActive(false);
            
            
            LevelManager.instance.AddCoinForLoose();
            soundManager.Play(SoundType.LooseSound);
            AppMetrica.Instance.ReportEvent("Loose"); //тут левел нужно отправить в аналитику
            
        }

        public void OpenForWin()
        {
            Open();
            looseRootContent.gameObject.SetActive(false);
            winRootContent.gameObject.SetActive(true);
            
            soundManager.Play(SoundType.WinSound);
            
            AppMetrica.Instance.ReportEvent("Win"); //тут левел нужно отправить в аналитику
            
            LevelManager.instance.NextLevel();
        }

        public override IUIController GetCurrentController()
        {
            return controller;
        }

        public override ViewType GetViewType()
        {
            return ViewType.Content;
        }

        public void AdsMoneyClick()
        {
            LevelManager.instance.ads.RunRewardAds(ResultRewardAds);
        }

        public void NextClick()
        {
            menu.Open();
            CalculateResultMoney();
        }
        
        private void ResultRewardAds(AdsResult adsResult)
        {
            if (adsResult == AdsResult.Finished)
            {
                menu.Open();
                LevelManager.instance.ConvertCoinToMoney(3);
            }
            else if(adsResult == AdsResult.Skipped || adsResult == AdsResult.Failed)
            {
                NextClick();
            }
        }
        
        private void CalculateResultMoney()
        {
            LevelManager.instance.ConvertCoinToMoney();
        }
    }
}
