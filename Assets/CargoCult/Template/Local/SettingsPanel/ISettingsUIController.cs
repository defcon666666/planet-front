﻿using CargoCult.Template.Common.PanelManager.UI;

namespace CargoCult.Template.Local.SettingsPanel
{
    public interface ISettingsUIController : IUIController
    {
        void SoundValueChanged(float value);
        void MusicValueChanged(float value);
        void LanguageChanged(int value);
        void QualityChanged(int value);
    }
}
