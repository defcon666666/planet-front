﻿using System.Collections.Generic;
using CargoCult.Template.Common.PanelManager.UI;

namespace CargoCult.Template.Local.SettingsPanel
{
    public interface ISettingsView : IView
    {
        void InitQualitiesDropdown(List<string> qualities, int currentIndex);
        void InitLanguagesDropdown(List<string> languages, int currentIndex);
        void SetStartValues(float soundVolume, float musicVolume);
    }
}
