﻿using System.Collections.Generic;
using CargoCult.Template.Common.PanelManager.UI;
using CargoCult.Template.Injectro.Core;
using CargoCult.Template.Local.Menu;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace CargoCult.Template.Local.SettingsPanel
{
    public class SettingsView : BaseView, ISettingsView
    {
        [SerializeField] private Slider soundSlider;
        [SerializeField] private Slider musicSlider;
        [SerializeField] private Dropdown languageDropdown;
        [SerializeField] private Dropdown qualityDropdown;
        
        [Inject] private ISettingsUIController controller;
        [Inject] private IMenuView menuView;
        
        public override IUIController GetCurrentController()
        {
            return controller;
        }

        public override ViewType GetViewType()
        {
            return ViewType.Content;
        }

        public void InitQualitiesDropdown(List<string> qualities, int currentIndex)
        {
            InitDropdown(qualityDropdown, qualities, currentIndex, QualityDropdownValueChange);
            
            qualityDropdown.RefreshShownValue();
        }
        
        public void InitLanguagesDropdown(List<string> languages, int currentIndex)
        {
            InitDropdown(languageDropdown, languages, currentIndex, LanguageDropdownValueChange);
        }

        public void SetStartValues(float soundVolume, float musicVolume)
        {
            soundSlider.value = soundVolume;
            musicSlider.value = musicVolume;
        }

        public void SoundSliderValueChange(float value)
        {
            controller.SoundValueChanged(value);
        }

        public void MusicSliderValueChange(float value)
        {
            controller.MusicValueChanged(value);
        }

        public void LanguageDropdownValueChange(int value)
        {
            controller.LanguageChanged(value);
        }

        public void QualityDropdownValueChange(int value)
        {
            controller.QualityChanged(value);
        }

        public void BackClick()
        {
            menuView.Open();
        }
        
        private void InitDropdown(Dropdown targetDropdown, List<string> items, int currentIndex, UnityAction<int> callback)
        {
            targetDropdown.onValueChanged.RemoveAllListeners();
            targetDropdown.options.Clear();

            for (int i = 0; i < items.Count; i++)
            {
                string item = items[i];
                targetDropdown.options.Add(new Dropdown.OptionData(item));
            }

            targetDropdown.value = currentIndex;

            targetDropdown.onValueChanged.AddListener(callback);
        }
    }
}
