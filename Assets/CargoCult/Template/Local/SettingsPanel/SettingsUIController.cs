﻿using System.Collections.Generic;
using CargoCult.Template.Common.PanelManager.UI;
using CargoCult.Template.Global.Localization;
using CargoCult.Template.Global.Quality;
using CargoCult.Template.Global.Sound;
using CargoCult.Template.Injectro.Core;

namespace CargoCult.Template.Local.SettingsPanel
{
    [PostResolveDependency(typeof(ILocalizationManager), typeof(IQualityManager))]
    public class SettingsUIController : BaseUIController, ISettingsUIController, IPostResolved
    {
        private readonly List<LanguageType> filledLanguages = new List<LanguageType>();

        [Inject] private ISettingsView view;
        [Inject] private ISoundManager soundManager;
        [Inject] private ILocalizationManager localizationManager;
        [Inject] private IQualityManager qualityManager;
        
        public void PostResolved()
        {
            List<string> languages = new List<string>();
            filledLanguages.AddRange(localizationManager.GetAvaibleLanguages());
            LanguageType currentLanguage = localizationManager.GetCurrentLanguageType();
            int currentLanguageIndex = 0;
            
            for (int i = 0; i < filledLanguages.Count; i++)
            {
                LanguageType avaibleLanguage = filledLanguages[i];
                string languageName = localizationManager.GetLanguageName(avaibleLanguage);
                languages.Add(languageName);

                if (currentLanguage == avaibleLanguage)
                {
                    currentLanguageIndex = i;
                }
            }

            view.InitLanguagesDropdown(languages, currentLanguageIndex);

            InitQualityDropdown();

            localizationManager.AddListener<LanguageChangedEvent>(OnLanguageChange);
        }
        
        public override void OnOpen()
        {
            float soundVolume = soundManager.GetSoundVolume();
            float musicVolume = soundManager.GetMusicVolume();
            
            view.SetStartValues(soundVolume, musicVolume);
        }

        public void SoundValueChanged(float value)
        {
            soundManager.SetSoundVolume(value);
        }

        public void MusicValueChanged(float value)
        {
            soundManager.SetMusicVolume(value);
        }

        public void LanguageChanged(int value)
        {
            localizationManager.ChangeLanguage(filledLanguages[value]);
        }

        public void QualityChanged(int value)
        {
            qualityManager.SetConfig(value);
        }
        
        private void InitQualityDropdown()
        {
            List<string> qualities = qualityManager.GetAvailableConfigsLocalizationKeys();

            for (int i = 0; i < qualities.Count; i++)
            {
                qualities[i] = localizationManager.Translate(qualities[i]);
            }

            int currentQuality = qualityManager.GetCurrentConfigIndex();

            view.InitQualitiesDropdown(qualities, currentQuality);
        }

        private void OnLanguageChange(LanguageChangedEventParametrs parametrs)
        {
            InitQualityDropdown();
        }
    }
}
