﻿using System.Collections;
using System.Collections.Generic;
using CargoCult.Template.Common.PanelManager;
using CargoCult.Template.Common.PanelManager.UI;
using CargoCult.Template.Injectro.Core;
using UnityEngine;
using UnityEngine.UI;


[Singleton][PostResolveDependency(typeof(IPanelManager),typeof(LevelManager))]
public class GameInfoView : BaseView,IGameInfoView
{
    public Text MoneyText;
    public Text LevelText;
    public Text MoneyInLevelText;
    public GameObject MoneyInLevelPanel;
    public override IUIController GetCurrentController()
    {
        return null;
    }

    public override void PostResolved()
    {
        base.PostResolved();
        Open();
    }

    public override void Open()
    {
        base.Open();
        LoadGameInfo();
        DisableMoneyInLevel();
    }

    public override ViewType GetViewType()
    {
        return ViewType.Toolbar;
    }

    public void LoadGameInfo()
    {
        MoneyText.text = PlayerPrefs.GetInt("Money", 100).ToString();
        LevelText.text = LevelManager.instance.GetActualLevelIndex().ToString();
    }

    public void ShowMoneyInLevel(int value)
    {
        MoneyInLevelPanel.SetActive(true);
        MoneyInLevelText.text = value.ToString();
    }

    public void ShowMoney(int value)
    {
        MoneyText.text = value.ToString();
    }

    public void DisableMoneyInLevel()
    {
        MoneyInLevelPanel.SetActive(false);
        MoneyInLevelText.text = "";
    }
}
