﻿using System;
using CargoCult.Template.Injectro.Core;

namespace CargoCult.Template.Global.Ads.OfflineAds
{
    public interface IOfflineAdsManager : IBean
    {
        void GoToStore(string packageName);

        void ToggleBanner(bool show);

        void ShowInterstitial(Action closeCallback);

    }
}