﻿using CargoCult.Template.Common.PanelManager.UI;
using UnityEngine;

namespace CargoCult.Template.Global.Ads.OfflineAds
{
    public interface IAdsBannerController : IUIController
    {
        string GetName(AppInfo appInfo);
        Sprite GetSplash(AppInfo appInfo);
        void Download(AppInfo appInfo);
    }
}