using CargoCult.Template.Common.PanelManager.UI;

namespace CargoCult.Template.Global.Ads.OfflineAds
{
    public interface IAdsBannerView : IView
    {
        void Open(AppInfo appInfo);
        void TogglePlaceForBanner(bool status);
    }
}