﻿using CargoCult.Template.Common.PanelManager.UI;
using CargoCult.Template.Injectro.Core;
using GoogleMobileAds.Api;
using UnityEngine;
using UnityEngine.UI;

namespace CargoCult.Template.Global.Ads.OfflineAds
{
    [Singleton]
    public class AdsBannerView : MonoBehaviour, IAdsBannerView
    {
        private const float TemplateDpi = 160f;

        [SerializeField] private RectTransform rootCanvasTransform;
        [SerializeField] private RectTransform rootPanel;
        [SerializeField] private GameObject offlineBannerPanel;
        [SerializeField] private Text title;
        [SerializeField] private Image splash;
        [SerializeField] private RectTransform wrapper;

        [Inject] private IAdsBannerController adsBannerController;

        private AppInfo appInfo;
        private Vector2 bannerSize;

        public void Open(AppInfo appInfo)
        {
            Open();
            offlineBannerPanel.gameObject.SetActive(true);
            
            if (appInfo != null)
            {
                this.appInfo = appInfo;
                title.text = adsBannerController.GetName(appInfo);
                splash.sprite = adsBannerController.GetSplash(appInfo);
            }
            
            wrapper.gameObject.SetActive(appInfo == null);
        }

        public void DownloadButtonClick()
        {
            if (appInfo != null)
            {
                adsBannerController.Download(appInfo);
            }
        }

        public void Open()
        {
            gameObject.SetActive(true);
            adsBannerController.OnOpen();
        }

        public void Close()
        {
            offlineBannerPanel.gameObject.SetActive(false);
            gameObject.SetActive(false);
            adsBannerController.OnClose();
        }

        public void OnFocus()
        {
        }

        public void OnFocusLost()
        {
        }

        public IUIController GetCurrentController()
        {
            return adsBannerController;
        }

        public GameObject GetGameObject()
        {
            return gameObject;
        }

        public ViewType GetViewType()
        {
            return ViewType.Banner;
        }

        public bool IsOverlapFocus()
        {
            return false;
        }

        public bool IsActive()
        {
            return gameObject.activeSelf;
        }

        public void ResetInputCallers()
        {
        }

        public bool CanBeClosedWithBackButton()
        {
            return false;
        }

        public void ClosedByBackButton()
        {
        }

        public void TogglePlaceForBanner(bool status)
        {
            if (status)
            {
                Open();
                rootPanel.sizeDelta = GetBannerPanelSize(status);
            }
            else
            {
                Close();
            }
        }
        
        private Vector2 GetBannerPanelSize(bool showBanner)
        {
            if (!showBanner)
            {
                return Vector2.zero;
            }

            if (bannerSize == Vector2.zero)
            {
                float bannerWidth = DpxRatio() * AdSize.Banner.Width;
                float bannerHeight = DpxRatio() * AdSize.Banner.Height;
                float x = Screen.width / bannerWidth;
                float y = Screen.height / bannerHeight;

                float canvasWidth = rootCanvasTransform.sizeDelta.x;
                float canvasHeight = rootCanvasTransform.sizeDelta.y;
                
                bannerSize = new Vector2(canvasWidth / x, canvasHeight / y);
            }
            
            return bannerSize;
        }
        
        private float DpxRatio()
        {
            if (Screen.dpi == 0)
            {
                return 1f;
            }

            return Screen.dpi / TemplateDpi;
        }
    }
}