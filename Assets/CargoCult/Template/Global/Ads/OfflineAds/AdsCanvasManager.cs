﻿using CargoCult.Template.Injectro.Core;
using UnityEngine;

namespace CargoCult.Template.Global.Ads.OfflineAds
{
    [Singleton]
    public class AdsCanvasManager : MonoBehaviour, IPostResolved
    {
        public void PostResolved()
        {
            Invoke(nameof(Activate), 0.1f);
        }

        private void Activate()
        {
            gameObject.SetActive(true);
        }
    }
}
