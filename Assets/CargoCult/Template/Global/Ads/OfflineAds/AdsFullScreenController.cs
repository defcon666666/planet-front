﻿using CargoCult.Template.Global.TimeScale;
using CargoCult.Template.Injectro.Core;
using UnityEngine;

namespace CargoCult.Template.Global.Ads.OfflineAds
{
    [Singleton]
    public class AdsFullScreenController : MonoBehaviour, IAdsFullScreenController, ITimeScaleChanger
    {
        [Inject] private IAdsFullScreenView adsFullScreenView;
        [Inject] private IOfflineAdsManager offlineAdsManager;
        [Inject] private ITimeScaleManager timeScaleManager;

        public string GetName(AppInfo appInfo)
        {
            return appInfo.AppName;
        }

        public Sprite GetSplash(AppInfo appInfo)
        {
            return appInfo.FullScreenSplash;
        }

        public Sprite GetIcon(AppInfo appInfo)
        {
            return appInfo.Icon;
        }

        public string GetFeedbacks(AppInfo appInfo)
        {
            return appInfo.FeedbackCount.ToString();
        }

        public float GetRate(AppInfo appInfo)
        {
            return appInfo.Rate;
        }

        public void Download(AppInfo appInfo)
        {
            offlineAdsManager.GoToStore(appInfo.PackageName);
        }

        public void OnOpen()
        {
            timeScaleManager.Pause(this);
        }

        public void OnClose()
        {
            timeScaleManager.Resume(this);
        }

        public void OnFocus()
        {
        }

        public void OnFocusLost()
        {
        }

        public void OnClosedByBackButton()
        {
        }
    }
}