using CargoCult.Template.Common.PanelManager.UI;

namespace CargoCult.Template.Global.Ads.OfflineAds
{
    public interface IAdsFullScreenView : IView
    {
        void OpenWithApp(AppInfo appInfo);
        void OpenWithLoading();
    }
}