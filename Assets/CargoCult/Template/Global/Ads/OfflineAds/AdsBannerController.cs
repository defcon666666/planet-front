﻿using CargoCult.Template.Injectro.Core;
using UnityEngine;

namespace CargoCult.Template.Global.Ads.OfflineAds
{
    [Singleton]
    public class AdsBannerController : MonoBehaviour, IAdsBannerController
    {
        [Inject] private IAdsBannerView adsBannerView;
        [Inject] private IOfflineAdsManager offlineAdsManager;

        public string GetName(AppInfo appInfo)
        {
            return appInfo.AppName;
        }

        public Sprite GetSplash(AppInfo appInfo)
        {
            return appInfo.BannerSplash;
        }

        public void Download(AppInfo appInfo)
        {
            offlineAdsManager.GoToStore(appInfo.PackageName);
        }

        public void OnOpen()
        {
        }

        public void OnClose()
        {
        }

        public void OnFocus()
        {
        }

        public void OnFocusLost()
        {
        }

        public void OnClosedByBackButton()
        {
        }
    }
}