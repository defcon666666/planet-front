using System;
using System.Collections.Generic;
using CargoCult.Template.Injectro.Core;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CargoCult.Template.Global.Ads.OfflineAds
{
    [Singleton]
    public class OfflineAdsManager : MonoBehaviour, IOfflineAdsManager, IPostResolved
    {
        [SerializeField] private AdsApps apps;
        [Inject] private IAdsFullScreenView adsFullScreenView;
        [Inject] private IAdsBannerView adsBannerView;

        public void PostResolved()
        {
        }

        public void ToggleBanner(bool show)
        {
            if (show)
            {
                adsBannerView.Open(GetRandomApp());
            }
            else
            {
                adsBannerView.Close();
            }
        }

        public void ShowInterstitial(Action closeCallback)
        {
            adsFullScreenView.OpenWithApp(GetRandomApp());
        }

        public void GoToStore(string packageName)
        {
            Application.OpenURL($"https://play.google.com/store/apps/details?id={packageName}");
        }

        private AppInfo GetRandomApp()
        {
            List<AppInfo> otherApps = apps.Apps.FindAll(info => Application.identifier != info.AppName);
            if (otherApps.Count == 0)
            {
                return null;
            }

            return otherApps[Random.Range(0, otherApps.Count)];
        }
    }
}