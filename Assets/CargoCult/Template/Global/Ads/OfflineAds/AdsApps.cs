using System.Collections.Generic;
using UnityEngine;

namespace CargoCult.Template.Global.Ads.OfflineAds
{
    public class AdsApps : ScriptableObject
    {
        [SerializeField] private List<AppInfo> apps = new List<AppInfo>();

        public List<AppInfo> Apps
        {
            get { return apps; }
        }
    }
}