﻿using CargoCult.Template.Common.PanelManager.UI;
using UnityEngine;

namespace CargoCult.Template.Global.Ads.OfflineAds
{
    public interface IAdsFullScreenController : IUIController
    {
        string GetName(AppInfo appInfo);
        Sprite GetSplash(AppInfo appInfo);
        Sprite GetIcon(AppInfo appInfo);
        string GetFeedbacks(AppInfo appInfo);
        float GetRate(AppInfo appInfo);
        void Download(AppInfo appInfo);
    }
}