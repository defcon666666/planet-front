﻿using CargoCult.Template.Common.PanelManager.UI;
using CargoCult.Template.Injectro.Core;
using UnityEngine;
using UnityEngine.UI;

namespace CargoCult.Template.Global.Ads.OfflineAds
{
    [Singleton]
    public class AdsFullScreenView : MonoBehaviour, IAdsFullScreenView, IPostResolved
    {
        [SerializeField] private RectTransform appRootContent;
        [SerializeField] private RectTransform loadingRootContent;
        [SerializeField] private Text title;
        [SerializeField] private Text feedback;
        [SerializeField] private Image icon;
        [SerializeField] private Image splash;
        [SerializeField] private Image[] stars;
        [SerializeField] private RectTransform wrapper;

        [Inject] private IAdsFullScreenController adsFullScreenController;

        private AppInfo appInfo;
        
        public void PostResolved()
        {
            Invoke("Close", 0.1f);
        }

        public void OpenWithApp(AppInfo appInfo)
        {
            Open();
            appRootContent.gameObject.SetActive(true);

            if (appInfo != null)
            {
                this.appInfo = appInfo;
                title.text = adsFullScreenController.GetName(appInfo);
                splash.sprite = adsFullScreenController.GetSplash(appInfo);
                icon.sprite = adsFullScreenController.GetIcon(appInfo);
                feedback.text = adsFullScreenController.GetFeedbacks(appInfo);
                for (int i = 0; i < stars.Length; i++)
                {
                    Image star = stars[i];
                    star.fillAmount = Mathf.Clamp01(adsFullScreenController.GetRate(appInfo) - i);
                }
            }
            
            wrapper.gameObject.SetActive(appInfo == null);
        }

        public void OpenWithLoading()
        {
            Open();
            loadingRootContent.gameObject.SetActive(true);
        }
        
        public void DownloadButtonClick()
        {
            if (appInfo != null)
            {
                adsFullScreenController.Download(appInfo);
            }
        }

        public void Open()
        {
            gameObject.SetActive(true);
            adsFullScreenController.OnOpen();
        }

        public void Close()
        {
            appRootContent.gameObject.SetActive(false);
            loadingRootContent.gameObject.SetActive(false);
            gameObject.SetActive(false);
            adsFullScreenController.OnClose();
        }

        public void OnFocus()
        {
        }

        public void OnFocusLost()
        {
        }

        public IUIController GetCurrentController()
        {
            return adsFullScreenController;
        }

        public GameObject GetGameObject()
        {
            return gameObject;
        }

        public ViewType GetViewType()
        {
            return ViewType.PopUp;
        }

        public bool IsOverlapFocus()
        {
            return true;
        }

        public bool IsActive()
        {
            return gameObject.activeSelf;
        }

        public void ResetInputCallers()
        {
        }

        public bool CanBeClosedWithBackButton()
        {
            return false;
        }

        public void ClosedByBackButton()
        {
        }
    }
}