using UnityEngine;

namespace CargoCult.Template.Global.Ads.OfflineAds
{
    public class AppInfo : ScriptableObject
    {
        [SerializeField] private string appName;
        [SerializeField] private int feedbackCount;
        [SerializeField] private float rate;
        [SerializeField] private Sprite icon;
        [SerializeField] private Sprite fullScreenSplash;
        [SerializeField] private Sprite bannerSplash;
        [SerializeField] private string packageName;

        public string AppName
        {
            get { return appName; }
        }

        public int FeedbackCount
        {
            get { return feedbackCount; }
        }

        public float Rate
        {
            get { return rate; }
        }

        public Sprite Icon
        {
            get { return icon; }
        }

        public Sprite FullScreenSplash
        {
            get { return fullScreenSplash; }
        }

        public Sprite BannerSplash
        {
            get { return bannerSplash; }
        }

        public string PackageName
        {
            get { return packageName; }
        }
    }
}