﻿using System;
using CargoCult.Template.Global.Ads.Admob;
using CargoCult.Template.Injectro.Core;

namespace CargoCult.Template.Global.Ads.AdsManager
{
    public interface IAdsManager: IBean
    {
        void ToggleBanner(bool show);
        
        void ShowInterstitial();
        bool IsInterstitialReady();
        
        void RunRewardAds(Action<AdsResult> callback = null);
        bool IsRewardReady();

        void MakeAdsFree();
        
    }
}

