﻿using System;
using CargoCult.Template.Common.KeysContrainers;
using CargoCult.Template.Global.Ads.Admob;
using CargoCult.Template.Global.Ads.OfflineAds;
using CargoCult.Template.Global.MainThread;
using CargoCult.Template.Global.Preference;
using CargoCult.Template.Injectro.Core;
using GoogleMobileAds.Api;
using UnityEngine;

namespace CargoCult.Template.Global.Ads.AdsManager
{
    [Singleton]
    public class AdsManager : MonoBehaviour, IAdsManager, IPostResolved
    {
        private const float InterstitialShowMinTime = 30f;
        private const float DefaultRewardShowMinTime = 5f;
        private const float RewardFirstTime = 5f;
        private const float FailCheckDeltaTime = 30f;
        private const double NoAdsCooldownInDays = 7;
        private const float BannerActivationTime = 0.2f;
        private const float AdditionalTimeForInterstitial = 30f;

        [Inject] private IPreferenceManager preferenceManager;
        [Inject] private IMainThreadExecutor mainThreadExecutor;
        [Inject] private IOfflineAdsManager offlineAdsManager;
        [Inject] private IAdsFullScreenView adsFullScreenView;
        [Inject] private IAdsBannerView adsBannerView;

        [Space][SerializeField] 
        private string addMobBannerIdAndroid;
        [SerializeField] private string addMobInterstitialIdAndroid;
        [SerializeField] private string addMobRewardedIdAndroid;
        [Space][SerializeField] 
        private string addMobBannerIdIos;
        [SerializeField] private string addMobInterstitialIdIos;
        [SerializeField] private string addMobRewardedIdIos;
        [SerializeField] private string testDeviceID;
        
        private AdMob adMob;
        private RewardBasedVideoAd rewardBasedVideo;
        private bool isNoAds = false;
        private bool bannerToggled = false;
        private float interstitialLastTime = 0F;
        private float rewardLastTime = RewardFirstTime - DefaultRewardShowMinTime;
        private float remoteRewardShowTime;
        private float lastFailCheckTime = 0f;
        private float showBannerRequestTime = -1;

        public void PostResolved()
        {
            isNoAds = preferenceManager.LoadValue(AdsKeysContainer.IsAdsFree, false);
            if (isNoAds)
            {
                TimeSpan difference = DateTime.Now - preferenceManager.LoadValue(AdsKeysContainer.FullScreenAdsSwitchOffTime, DateTime.MinValue);
                if (difference.TotalDays >= NoAdsCooldownInDays)
                {
                    isNoAds = false;
                    preferenceManager.SaveValue(AdsKeysContainer.IsAdsFree,false);
                }
            }
            
            adMob = new AdMob(GetAddMobBannerId(), GetAddMobInterstitialId(), GetAddMobRewardedId(), testDeviceID);
            remoteRewardShowTime = DefaultRewardShowMinTime;
            
            ToggleBanner(true);
        }

        public void ToggleBanner(bool show)
        {
            if (show)
            {
                if (showBannerRequestTime < 0)
                {
                    showBannerRequestTime = Time.unscaledTime;
                }
            }
            else
            {
                showBannerRequestTime = -1;
            }
        }
        
        public void ShowInterstitial()
        {
            if (isNoAds || !IsInterstitialTimeOk())
            {
                return;
            }

            interstitialLastTime = Time.unscaledTime;
            if (adMob.IsInterstitialReady())
            {
                adsFullScreenView.OpenWithLoading();
                adMob.ShowInterstitial(() => { mainThreadExecutor.Run(OnCloseAd); });
                return;
            }

            if (!IsInternetAvailable())
            {
                offlineAdsManager.ShowInterstitial(OnCloseAd);
            }
        }

        public bool IsInterstitialReady()
        {
            return !isNoAds && IsInterstitialTimeOk() && (!IsInternetAvailable() || adMob.IsInterstitialReady());
        }

        public void RunRewardAds(Action<AdsResult> callback)
        {
            if (!IsRewardReady())
            {
                return;
            }
            rewardLastTime = Time.unscaledTime;
            
            adsFullScreenView.OpenWithLoading();
            
            adMob.RunReward(MainThreadCallBack(callback));
        }

        public bool IsRewardReady()
        {
            return IsRewardTimeOk() && adMob.IsRewardReady();
        }

        public void MakeAdsFree()
        {
            preferenceManager.SaveValue(AdsKeysContainer.FullScreenAdsSwitchOffTime, DateTime.Now);
            if (isNoAds)
            {
                return;
            }
            isNoAds = true;
            preferenceManager.SaveValue(AdsKeysContainer.IsAdsFree, true);

            if (bannerToggled)
            {
                ToggleBanner(false);
            }
        }

        private void Update()
        {
            ProcessBannerUpdate();
            FailCheck();
        }

        private void CorrectInterstitialShowTime(AdsResult result)
        {
            if (result == AdsResult.Finished || result == AdsResult.Skipped)
            {
                if (InterstitialShowMinTime - (Time.unscaledTime - interstitialLastTime) < AdditionalTimeForInterstitial)
                {
                    interstitialLastTime = Time.unscaledTime;
                    interstitialLastTime -= InterstitialShowMinTime - AdditionalTimeForInterstitial;
                }
            }
        }

        private void ProcessBannerUpdate()
        {
            bool showBanner = showBannerRequestTime > -1 && Time.unscaledTime - showBannerRequestTime > BannerActivationTime;
            
            if (showBanner == bannerToggled)
            {
                return;
            }

            bannerToggled = showBanner;

            if (isNoAds)
            {
                adMob.ToggleBanner(false);
                adsBannerView.TogglePlaceForBanner(false);
                return;
            }

            if (showBanner)
            {
                if (IsInternetAvailable())
                {
                    adMob.ToggleBanner(true);
                    offlineAdsManager.ToggleBanner(false);
                }
                else
                {
                    adMob.ToggleBanner(false);
                    offlineAdsManager.ToggleBanner(true);
                }
            }
            else
            {
                adMob.ToggleBanner(false);
                offlineAdsManager.ToggleBanner(false);
            }

            adsBannerView.TogglePlaceForBanner(showBanner);
        }

        private bool IsInternetAvailable()
        {
            return Application.internetReachability != NetworkReachability.NotReachable;
        }

        private void FailCheck()
        {
            if (lastFailCheckTime + FailCheckDeltaTime > Time.unscaledTime)
            {
                return;
            }

            adMob.CheckLoadFail();
            
            if (bannerToggled && adMob.IsBannerFailed())
            {
                offlineAdsManager.ToggleBanner(true);
            }
            
            lastFailCheckTime = Time.unscaledTime;
        }

       
        private Action<AdsResult> MainThreadCallBack(Action<AdsResult> callback)
        {
            if (null == callback)
            {
                return result => { };
            }

            return result => mainThreadExecutor.Run(() =>
            {
                OnCloseAd();
                
                CorrectInterstitialShowTime(result);
                
                callback(result);
            });
        }

        private bool IsInterstitialTimeOk()
        {
            return Time.unscaledTime - interstitialLastTime > InterstitialShowMinTime;
        }
        
        private bool IsRewardTimeOk()
        {
            return Time.unscaledTime - rewardLastTime > remoteRewardShowTime;
        }

        private void OnCloseAd()
        {
            adsFullScreenView.Close();
        }

        private string GetAddMobBannerId()
        {
            return 
#if UNITY_STANDALONE_WIN
                Application.platform.ToString()
#elif UNITY_ANDROID
                addMobBannerIdAndroid
#elif UNITY_IOS
                    AddMobBannerIdIos
                #endif
                ;
        }

        private string GetAddMobInterstitialId()
        {
            return 
#if UNITY_STANDALONE_WIN
                Application.platform.ToString()
#elif UNITY_ANDROID
                addMobInterstitialIdAndroid
#elif UNITY_IOS
                    AddMobInterstitialIdIos
                #endif
                ;
        }

        private string GetAddMobRewardedId()
        {
            return
#if UNITY_STANDALONE_WIN
                Application.platform.ToString()
#elif UNITY_ANDROID
                addMobRewardedIdAndroid
#elif UNITY_IOS
                    AddMobRewardedIdIos
                #endif
                ;
        }
    }
}