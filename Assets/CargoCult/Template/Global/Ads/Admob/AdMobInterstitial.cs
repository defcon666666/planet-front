﻿using System;
using GoogleMobileAds.Api;

namespace CargoCult.Template.Global.Ads.Admob
{
    internal class AdMobInterstitial : AbstractAdMob
    {
        private readonly string id;
        private readonly string testDeviceId;
        private InterstitialAd interstitial;
        private Action currentCloseCallback;

        public AdMobInterstitial(string id, string testDeviceId = null)
        {
            this.id = id;
            this.testDeviceId = testDeviceId;
            LoadNew();
        }

        public void Show(Action closeCallback)
        {
            currentCloseCallback = closeCallback;
            interstitial.Show();
        }

        public bool IsReady()
        {
            return interstitial.IsLoaded();
        }

        public void LoadNew()
        {
            isFailedToLoad = false;
            interstitial = new InterstitialAd(id);
            interstitial.OnAdClosed += OnAdClosed;
            interstitial.OnAdFailedToLoad += OnAdFailedToLoad;
            interstitial.LoadAd(CreateAdRequest(testDeviceId));
        }

        private void OnAdFailedToLoad(object sender, AdFailedToLoadEventArgs e)
        {
            interstitial.Destroy();
            isFailedToLoad = true;
            AdClosedResult();
        }

        private void OnAdClosed(object sender, EventArgs e)
        {
            interstitial.Destroy();
            LoadNew();
            AdClosedResult();
        }

        private void AdClosedResult()
        {
            if (currentCloseCallback != null)
            {
                currentCloseCallback();
                currentCloseCallback = null;
            }
        }
    }
}