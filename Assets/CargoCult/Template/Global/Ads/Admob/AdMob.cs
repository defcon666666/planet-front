﻿using System;
using GoogleMobileAds.Api;

namespace CargoCult.Template.Global.Ads.Admob
{
    internal class AdMob
    {
        private readonly AdMobBanner adMobBanner;
        private readonly AdMobInterstitial adMobInterstitial;
        private readonly AdMobRewardVideo adMobRewardVideo;

        public AdMob(string bannerId, string interstitialId, string rewardId, string testDeviceId = null)
        {
            MobileAds.Initialize(status => {});
            MobileAds.SetiOSAppPauseOnBackground(true);
            adMobBanner = new AdMobBanner(bannerId, testDeviceId);
            adMobInterstitial = new AdMobInterstitial(interstitialId, testDeviceId);
            adMobRewardVideo = new AdMobRewardVideo(rewardId, testDeviceId);
        }

        public void ToggleBanner(bool show)
        {
            if (adMobBanner != null)
            {
                adMobBanner.Toggle(show);
            }
        }

        public void ShowInterstitial(Action closeCallback)
        {
            adMobInterstitial.Show(closeCallback);
        }

        public bool IsInterstitialReady()
        {
            return adMobInterstitial.IsReady();
        }

        public bool IsRewardReady()
        {
            return adMobRewardVideo.IsReady();
        }

        public void RunReward(Action<AdsResult> callback)
        {
            adMobRewardVideo.Run(callback);
        }
        
        public void RequestReward()
        {
            adMobRewardVideo.LoadNew();
        }

        public void CheckLoadFail()
        {
            if (adMobInterstitial.IsFailedToLoad())
            {
                adMobInterstitial.LoadNew();
            }
            if (adMobRewardVideo.IsFailedToLoad())
            {
                adMobRewardVideo.LoadNew();
            }
        }

        public bool IsBannerFailed()
        {
            return adMobBanner.IsFailedToLoad();
        }
    }
}