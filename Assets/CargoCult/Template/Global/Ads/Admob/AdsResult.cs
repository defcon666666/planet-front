﻿namespace CargoCult.Template.Global.Ads.Admob
{
    public enum AdsResult
    {
        Failed = 0,
        Skipped = 100,
        Finished = 200,
    }
}