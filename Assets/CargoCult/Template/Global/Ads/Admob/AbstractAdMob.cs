﻿using GoogleMobileAds.Api;

namespace CargoCult.Template.Global.Ads.Admob
{
    class AbstractAdMob
    {
        protected bool isFailedToLoad = false;
        
        public bool IsFailedToLoad()
        {
            return isFailedToLoad;
        }
        
        protected AdRequest CreateAdRequest(string testDeviceID = null)
        {
            if (string.IsNullOrEmpty(testDeviceID))
            {
                return new AdRequest.Builder().Build();
            }

            return new AdRequest.Builder().AddTestDevice(testDeviceID).Build();
        }
    }
}