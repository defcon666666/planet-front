﻿using GoogleMobileAds.Api;

namespace CargoCult.Template.Global.Ads.Admob
{
    internal class AdMobBanner : AbstractAdMob
    {
        private const AdPosition Position = AdPosition.Top;
        private readonly AdSize size = AdSize.Banner;

        private readonly string id;
        private BannerView bannerView;
        private string testDeviceId;
        private bool isBannerActive;

        public AdMobBanner(string id, string testDeviceId)
        {
            this.id = id;
            this.testDeviceId = testDeviceId;
        }

        public void Toggle(bool show)
        {
            if (isBannerActive == show)
            {
                return;
            }

            isBannerActive = show;
            
            if (show)
            {
                LoadNew();
            }
            else if (bannerView != null)
            {
                try
                {
                    bannerView.Hide();
                    bannerView.Destroy();
                }
                catch
                {
                    // GoogleMobileAds exception
                }
                finally
                {
                    bannerView = null;
                }
            }
        }
        
        private void LoadNew()
        {
            isFailedToLoad = false;
            if (bannerView != null && !bannerView.IsDestroyCalled())
            {
                bannerView.Destroy();
            }

            BannerView newBannerView = new BannerView(id, size, Position);
            newBannerView.OnAdFailedToLoad += (sender, args) =>
            {
                if (!newBannerView.IsDestroyCalled())
                {
                    newBannerView.Destroy();
                }
                
                if (bannerView == newBannerView)
                {
                    bannerView = null;
                }
                
                isFailedToLoad = true;
            };

            newBannerView.OnAdLoaded += (sender, args) =>
            {
                if (bannerView != newBannerView && !newBannerView.IsDestroyCalled())
                {
                    newBannerView.Hide();
                    newBannerView.Destroy();
                }
                
                if (!isBannerActive && bannerView == newBannerView)
                {
                    bannerView = null;
                }
            };
            newBannerView.LoadAd(CreateAdRequest(testDeviceId));
            bannerView = newBannerView;
        }
        
    }
}