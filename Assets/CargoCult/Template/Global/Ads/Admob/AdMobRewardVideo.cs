﻿using System;
using GoogleMobileAds.Api;

namespace CargoCult.Template.Global.Ads.Admob
{
    internal class AdMobRewardVideo : AbstractAdMob
    {
        private readonly string id;
        private readonly RewardBasedVideoAd rewardBasedVideo;
        private Action<AdsResult> rewardedCallback;
        private string testDeviceID;

        public AdMobRewardVideo(string id, string testDeviceId = null)
        {
            this.id = id;
            this.testDeviceID = testDeviceId;
            rewardBasedVideo = RewardBasedVideoAd.Instance;
            rewardBasedVideo.OnAdRewarded += (sender, args) => RewardCallbackRezult(AdsResult.Finished);
            rewardBasedVideo.OnAdFailedToLoad += (sender, args) =>
            {
                RewardCallbackRezult(AdsResult.Failed);
                isFailedToLoad = true;
            };
            rewardBasedVideo.OnAdClosed += (sender, args) =>
            {
                RewardCallbackRezult(AdsResult.Skipped);
                LoadNew();
            };
            LoadNew();
        }

        public void Run(Action<AdsResult> callback = null)
        {
            rewardedCallback = callback;
            rewardBasedVideo.Show();
        }

        public bool IsReady()
        {
            return rewardBasedVideo.IsLoaded();
        }

        public void LoadNew()
        {
            isFailedToLoad = false;
            rewardBasedVideo.LoadAd(CreateAdRequest(testDeviceID), id);
        }

        private void RewardCallbackRezult(AdsResult result)
        {
            if (null != rewardedCallback)
            {
                rewardedCallback(result);
            }
            rewardedCallback = null;
        }
    }
}