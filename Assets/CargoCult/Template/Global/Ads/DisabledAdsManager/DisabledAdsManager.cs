﻿using System;
using CargoCult.Template.Global.Ads.Admob;
using CargoCult.Template.Global.Ads.AdsManager;
using CargoCult.Template.Injectro.Core;
using UnityEngine;

namespace CargoCult.Template.Global.Ads.DisabledAdsManager
{
    [Singleton]
    public class DisabledAdsManager : MonoBehaviour, IAdsManager, IPostResolved
    {
        public void PostResolved()
        {
        }

        public void ToggleBanner(bool show)
        {
        }

        public void ShowInterstitial()
        {
        }

        public bool IsInterstitialReady()
        {
            return false;
        }

        public void RunRewardAds(Action<AdsResult> callback = null)
        {
            callback(AdsResult.Failed);
        }

        public bool IsRewardReady()
        {
            return false;
        }

        public void MakeAdsFree()
        {
        }
    }
}
