﻿using CargoCult.Template.Common.PanelManager.UI;

namespace CargoCult.Template.Global.Loading
{
    public interface ILoadingView : IView
    {
        void Open(string sceneName);
    }
}
