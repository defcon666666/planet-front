﻿using System;
using CargoCult.Template.Common.PanelManager.UI;
using CargoCult.Template.Global.Localization;
using CargoCult.Template.Injectro.Core;
using CargoCult.Template.SelectiveStrings;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace CargoCult.Template.Global.Loading
{
    [Singleton][PostResolveDependency]
    public class LoadingView : MonoBehaviour, ILoadingView, IPostResolved
    {
        private const string PercentagePostfix = "%";
        private const float ProgressTime = 30;
        
        private readonly string[] hintKeys = {"Hint1", "Hint2", "Hint3"};

        [SerializeField] private RawImage backgroundImage;
        [SerializeField] private Slider loadingSlder;
        [SerializeField] private Text loadingPercentText;
        [SerializeField] private GameObject hintPanel;
        [SerializeField] private Text hintText;
        [SerializeField] private Texture[] backgroundTextures;
        [SerializeField] private SpecificBackgroundsPack[] specificBackgroundsPacks;
        
        [Inject] private ILocalizationManager localizationManager;
        
        private bool isOpened;
        private int displayedValue;
        private float progressTimer;
        
        public void PostResolved()
        {
            Invoke("Deactivate", 0);
        }
        
        public void Open(string sceneName)
        {
            bool usedSpecificBackground = false;
            
            if (!isOpened)
            {
                for (int i = 0; i < specificBackgroundsPacks.Length; i++)
                {
                    SpecificBackgroundsPack backgroundsPack = specificBackgroundsPacks[i];
                    if (backgroundsPack.SceneName == sceneName)
                    {
                        Texture randomBack = backgroundsPack.Backgrounds[Random.Range(0, backgroundsPack.Backgrounds.Length)];
                        backgroundImage.texture = randomBack;
                        usedSpecificBackground = true;
                        break;
                    }
                }
            }
            
            TryOpen(!usedSpecificBackground);
        }
        
        public void Open()
        {
            TryOpen(true);
        }

        public void Close()
        {
            isOpened = false;
            SetLoadingPercent(100);
            Invoke("Deactivate", 0.1f);
        }

        public void OnFocus()
        {
        }

        public void OnFocusLost()
        {
        }

        public IUIController GetCurrentController()
        {
            return null;
        }

        public GameObject GetGameObject()
        {
            return gameObject;
        }

        public ViewType GetViewType()
        {
            return ViewType.Content;
        }

        public bool IsOverlapFocus()
        {
            return true;
        }
        
        public bool IsActive()
        {
            return isOpened;
        }

        public void ResetInputCallers()
        {
        }
        
        private void TryOpen(bool useUniversalBackground)
        {
            if (!isOpened)
            {
                displayedValue = 2;
                progressTimer = 0;

                SetLoadingPercent(displayedValue);
                ShowRandomHint();

                if (useUniversalBackground)
                {
                    backgroundImage.texture = backgroundTextures[Random.Range(0, backgroundTextures.Length)];
                }

                gameObject.SetActive(true);
            }

            isOpened = true;
        }

        public bool CanBeClosedWithBackButton()
        {
            return false;
        }

        public void ClosedByBackButton()
        {
        }

        private void Update()
        {
            if (!isOpened)
                return;
            
            float lerpValue = progressTimer / ProgressTime;
            displayedValue = (int) Mathf.Lerp(displayedValue, 99, lerpValue);
            SetLoadingPercent(displayedValue);
            progressTimer += Time.unscaledDeltaTime;
        }
        
        private void Deactivate()
        {
            if (!isOpened)
            {
                gameObject.SetActive(false);
            }
        }
        
        private void SetLoadingPercent(float percent)
        {
            loadingSlder.value = percent;
            loadingPercentText.text = percent + PercentagePostfix;
        }
        
        private void ShowRandomHint()
        {
            string randomHintKey = hintKeys[Random.Range(0, hintKeys.Length)];
            string localizedHint = localizationManager.Translate(randomHintKey);
            hintText.text = localizedHint;
            hintPanel.gameObject.SetActive(localizedHint.Length > 0);
        }

        [Serializable]
        private struct SpecificBackgroundsPack
        {
            [SelectiveString("LocationSceneNames")] public string SceneName;
            public Texture[] Backgrounds;
        }
    }
}
