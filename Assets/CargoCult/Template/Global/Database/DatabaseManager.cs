﻿using System.Collections.Generic;
using System.Linq;
using CargoCult.Template.Global.Sound;
using CargoCult.Template.Injectro.Core;
using UnityEngine;

namespace CargoCult.Template.Global.Database
{
    [Singleton]
    public class DatabaseManager : MonoBehaviour, IDatabaseManager, IPostResolved
    {
        [SerializeField] private SoundDatabase soundDatabase;
        
        private readonly IDictionary<string, IInfo> indexedDatabase = new Dictionary<string, IInfo>();

        public T GetInfo<T>(string infoName) where T : IInfo
        {
            return (T)indexedDatabase[infoName];
        }

        public List<T> GetAllInfoByType<T>() where T : IInfo
        {
            return indexedDatabase.Values.OfType<T>().ToList();
        }
        
        public void PostResolved()
        {
            indexedDatabase.Clear();
            FillIndexedDatabase(soundDatabase.GetEntities());
        }

        private void FillIndexedDatabase<T>(List<T> infos) where T : IInfo
        {
            foreach (T info in infos)
            {
                indexedDatabase.Add(info.GetName(), info);
            }
        }
    }
}
