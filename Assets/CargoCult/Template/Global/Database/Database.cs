﻿using System.Collections.Generic;
using UnityEngine;

namespace CargoCult.Template.Global.Database
{
    public abstract class Database<T> : ScriptableObject, IDatabase<T> where T : IInfo
    {
        [SerializeField]
        private List<T> entities = new List<T>();

        public T GetInfo(string entityName)
        {
            for (int i = 0; i < entities.Count; i++)
            {
                T npcInfo = entities[i];
                if (npcInfo.GetName().Equals(entityName))
                {
                    return npcInfo;
                }
            }

            throw new KeyNotFoundException("No entity with name `" + entityName + "` in Database: " + GetType());
        }

        public List<T> GetEntities()
        {
            return entities;
        }
    }
}