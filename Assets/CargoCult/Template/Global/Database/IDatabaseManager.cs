﻿using System.Collections.Generic;
using CargoCult.Template.Injectro.Core;

namespace CargoCult.Template.Global.Database
{
    public interface IDatabaseManager : IBean
    {
        T GetInfo<T>(string infoName) where T : IInfo;
        List<T> GetAllInfoByType<T>() where T : IInfo;
    }
}