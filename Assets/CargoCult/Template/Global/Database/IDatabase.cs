﻿using System.Collections.Generic;

namespace CargoCult.Template.Global.Database
{
    public interface IDatabase<T> where T : IInfo
    {
        T GetInfo(string entityName);
        List<T> GetEntities();
    }
}