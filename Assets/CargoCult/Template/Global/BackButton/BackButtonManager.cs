﻿using CargoCult.Template.Common.PanelManager;
using CargoCult.Template.Injectro.Core;
using UnityEngine;

namespace CargoCult.Template.Global.BackButton
{
    [Singleton]
    public class BackButtonManager : MonoBehaviour, IBackButtonManager
    {
        private IPanelManager currentLocalPanelManager;
        
        public void PanelManagerResolved(IPanelManager resolvedPanelManager)
        {
            currentLocalPanelManager = resolvedPanelManager;
        }

        private void Update()
        {
            if (UnityEngine.Input.GetKeyDown(KeyCode.Escape))
            {
                BackButtonClicked();
            }
        }

        private void BackButtonClicked()
        {
            if (currentLocalPanelManager == null)
            {
                return;
            }

            currentLocalPanelManager.TryProcessBackButtonClick();
        }
    }
}
