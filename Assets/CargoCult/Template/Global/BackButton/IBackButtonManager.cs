﻿using CargoCult.Template.Common.PanelManager;
using CargoCult.Template.Injectro.Core;

namespace CargoCult.Template.Global.BackButton
{
    public interface IBackButtonManager : IBean
    {
        void PanelManagerResolved(IPanelManager resolvedPanelManager);
    }
}
