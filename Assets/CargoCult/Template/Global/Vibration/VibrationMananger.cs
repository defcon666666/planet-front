﻿using System;
using CargoCult.Template.Injectro.Core;
using UnityEngine;

namespace CargoCult.Template.Global.Vibration
{
    [Singleton]
    public class VibrationMananger : MonoBehaviour, IVibrationManager, IPostResolved
    {
#if UNITY_ANDROID
        private AndroidJavaObject vibrator;
#endif
        
        public void PostResolved()
        {
#if UNITY_ANDROID
            try
            {
                AndroidJavaClass unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject unityPlayerActivity =
                    unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity");
                vibrator = unityPlayerActivity.Call<AndroidJavaObject>("getSystemService", "vibrator");
            }
            catch (Exception e)
            {
#if !UNITY_EDITOR
                Debug.Log(e);
#endif
            }
#endif
        }

        public bool HasVibrator()
        {
#if UNITY_ANDROID && !UNITY_EDITOR
            return vibrator.Call<bool>("hasVibrator");
#else
            return false;
#endif
        }

        public void Cancel()
        {
#if UNITY_ANDROID
            if (HasVibrator())
            {
                vibrator.Call("cancel");
            }
#endif
        }

        public void Vibrate(float time)
        {
#if UNITY_ANDROID
            Vibrate(FloatToLongTime(time));
#endif
        }

        public void Vibrate(float[] pattern, int repeate = -1)
        {
#if UNITY_ANDROID
            long[] longPattern = new long[pattern.Length];
            for (int x = 0; x < longPattern.Length; x += 1)
            {
                longPattern[x] = FloatToLongTime(pattern[x]);
            }

            Vibrate(longPattern, repeate);
#endif
        }

        public void Vibrate(long[] pattern, int repeate = -1)
        {
#if UNITY_ANDROID
            if (HasVibrator())
            {
                vibrator.Call("vibrate", pattern, repeate);
            }
#endif
        }

        public void Vibrate(long time)
        {
#if UNITY_ANDROID
            if (HasVibrator())
            {
                vibrator.Call("vibrate", time);
            }
#endif
        }

        public void VibratePermissionWrapper()
        {
            Handheld.Vibrate();
        }

        private long FloatToLongTime(float time)
        {
            time *= 1000f;
            return (long) time;
        }
    }
}
