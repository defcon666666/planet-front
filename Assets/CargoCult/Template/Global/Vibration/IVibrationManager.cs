﻿using CargoCult.Template.Injectro.Core;

namespace CargoCult.Template.Global.Vibration
{
    public interface IVibrationManager : IBean
    {
        bool HasVibrator();
        void Cancel();
        void Vibrate(float time);
        void Vibrate(float[] pattern, int repeate = -1);
        void Vibrate(long[] pattern, int repeate = -1);
        void Vibrate(long time);
    }
}
