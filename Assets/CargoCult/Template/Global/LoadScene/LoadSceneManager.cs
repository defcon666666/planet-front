﻿using System;
using System.Collections;
using CargoCult.Template.Common.EventController;
using CargoCult.Template.Global.Loading;
using CargoCult.Template.Injectro.Core;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CargoCult.Template.Global.LoadScene
{
    [Singleton]
    [PostResolveDependency]
    public class LoadSceneManager : MonoBehaviour, ILoadSceneManager, IPostResolved
    {
        [Inject] private ILoadingView loadingView;
        private EventController<SceneEvent, SceneEventParametrs> eventController;
        
        public void PostResolved()
        {
            gameObject.SetActive(true);
            eventController = new EventController<SceneEvent, SceneEventParametrs>(this, false);
            eventController.SubscribeOnSceneUnload();
            SceneManager.sceneLoaded += OnSceneLoad;
            SceneManager.sceneUnloaded += OnSceneUnload;
        }
        
        public void LoadScene(string sceneName, LoadSceneMode loadMode = LoadSceneMode.Single)
        {
            eventController.RaiseEvent<BeforeLoadSceneEvent>(new SceneEventParametrs
            {
                SceneName = sceneName,
                LoadMode = loadMode
            });
            
            StartCoroutine(LoadingProcess(sceneName, loadMode));
        }

        public bool UnloadScene(string sceneName)
        {
            Scene scene = SceneManager.GetSceneByName(sceneName);

            if (!scene.IsValid())
            {
                return false;
            }
            
            eventController.RaiseEvent<BeforeUnloadSceneEvent>(new SceneEventParametrs
            {
                Scene = scene,
                SceneName = sceneName,
            });
            SceneManager.UnloadSceneAsync(sceneName);
            return true;
        }
        
        public void AddListener<TA>(Action<SceneEventParametrs> action,
            EventContextType eventContextType = EventContextType.Scene) where TA : SceneEvent
        {
            eventController.AddListener<TA>(action, eventContextType);
        }

        public void RemoveListener<TR>(Action<SceneEventParametrs> action,
            EventContextType eventContextType = EventContextType.Scene) where TR : SceneEvent
        {
            eventController.RemoveListener<TR>(action, eventContextType);
        }

        private void OnSceneUnload(Scene s)
        {
            eventController.RaiseEvent<UnloadSceneEvent>(new SceneEventParametrs
            {
                Scene = s,
                SceneName = s.name,
            });
        }

        private void OnSceneLoad(Scene s, LoadSceneMode mode)
        {
            eventController.RaiseEvent<LoadSceneEvent>(new SceneEventParametrs
            {
                Scene = s,
                LoadMode = mode,
                SceneName = s.name,
            });
        }

        private IEnumerator LoadingProcess(string sceneName, LoadSceneMode loadSceneMode)
        {
            AsyncOperation loading = SceneManager.LoadSceneAsync(sceneName, loadSceneMode);
            loading.allowSceneActivation = false;

            while (loading.progress < 0.9f)
            {
                yield return null;
            }

            yield return new WaitForEndOfFrame();

            loading.allowSceneActivation = true;

            while (!loading.isDone)
            {
                yield return null;
            }

            yield return new WaitForEndOfFrame();

            SceneManager.SetActiveScene(SceneManager.GetSceneByName(sceneName));
        }

        private void OnDestroy()
        {
            SceneManager.sceneLoaded -= OnSceneLoad;
            SceneManager.sceneUnloaded -= OnSceneUnload;
        }
    }
}