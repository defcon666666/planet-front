﻿using CargoCult.Template.Common.EventController;
using CargoCult.Template.Injectro.Core;
using UnityEngine.SceneManagement;

namespace CargoCult.Template.Global.LoadScene
{
    public interface ILoadSceneManager : IBean, IEventController<SceneEvent, SceneEventParametrs>
    {
        void LoadScene(string sceneName, LoadSceneMode loadMode = LoadSceneMode.Single);
        bool UnloadScene(string sceneName);
    }

    public class SceneEvent : BaseEvent
    {
    }

    public class LoadSceneEvent : SceneEvent
    {
    }

    public class UnloadSceneEvent : SceneEvent
    {
    }
    
    public class BeforeLoadSceneEvent : SceneEvent
    {
    }
    
    public class BeforeUnloadSceneEvent : SceneEvent
    {
    }

    public struct SceneEventParametrs
    {
        public Scene Scene;
        public string SceneName;
        public LoadSceneMode LoadMode;
    }
}
