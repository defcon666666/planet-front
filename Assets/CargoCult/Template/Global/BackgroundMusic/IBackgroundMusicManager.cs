﻿using CargoCult.Template.Injectro.Core;

namespace CargoCult.Template.Global.BackgroundMusic
{
    public interface IBackgroundMusicManager : IBean
    {
        void StopMusic();
        void PlayMusic();
    }
}
