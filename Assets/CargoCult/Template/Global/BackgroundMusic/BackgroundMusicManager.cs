﻿using CargoCult.Template.Global.Sound;
using CargoCult.Template.Injectro.Core;
using CargoCult.Template.SelectiveStrings;
using UnityEngine;

namespace CargoCult.Template.Global.BackgroundMusic
{
    [Singleton]
    [PostResolveDependency(typeof(ISoundManager))]
    public class BackgroundMusicManager : MonoBehaviour, IBackgroundMusicManager, IPostResolved
    {
        [SerializeField] [SelectiveString("SoundTypes")]
        private string soundType = SoundType.BackgroundMusic;

        [Inject] private ISoundManager soundManager;

        private int playbackID = -1;

        public void PostResolved()
        {
            PlayMusic();
        }

        public void StopMusic()
        {
            if (playbackID == -1)
            {
                return;
            }
            
            soundManager.Stop(playbackID);
        }

        public void PlayMusic()
        {
            StopMusic();
            
            if (soundType != SoundType.None)
            {
                playbackID = soundManager.Play(soundType);
                soundManager.ToggleNotDestroyableState(playbackID, true);
            }
        }
    }
}