﻿using UnityEngine;

namespace CargoCult.Template.Global.Quality
{
    [CreateAssetMenu(fileName = "QualityConfig", menuName = "QualityManager/Create QualityConfig", order = 1)]
    public class QualityConfig : ScriptableObject
    {
        [SerializeField, Tooltip("Индекс который будет применен в UnityEngine.QualitySettings.SetQualityLevel(index)")]
        private int unityQualitySettingsIndex = 0;

        [SerializeField] private string titleKey = "localizationKey";

        [SerializeField] private float farClipPlane = 200;

        public int UnityQualitySettingsIndex => unityQualitySettingsIndex;
        public string TitleKey => titleKey;
        public float FarClipPlane => farClipPlane;
    }
}