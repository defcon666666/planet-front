using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace CargoCult.Template.Global.Quality.Editor
{
    [CustomEditor(typeof(QualityManager))]
    public class QualityManagerInspector : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            QualityManager qualityManager = target as QualityManager;

            List<string> namesConfigs = qualityManager.GetAvailableConfigsLocalizationKeys();

            for (int i = 0; i < namesConfigs.Count; i++)
            {
                if (GUILayout.Button("Apply " + namesConfigs[i]))
                {
                    qualityManager.SetConfig(i);
                }
            }
        }
    }
}