using System;
using System.Collections.Generic;
using CargoCult.Template.Common.EventController;
using CargoCult.Template.Common.KeysContrainers;
using CargoCult.Template.Global.LoadScene;
using CargoCult.Template.Global.Preference;
using CargoCult.Template.Injectro.Core;
using UnityEngine;

namespace CargoCult.Template.Global.Quality
{
    [Singleton, PostResolveDependency(typeof(ILoadSceneManager), typeof(IPreferenceManager))]
    public class QualityManager : MonoBehaviour, IQualityManager
    {
        [Inject] private IPreferenceManager preferenceManager;
        [Inject] private ILoadSceneManager loadSceneManager;

        [SerializeField] private ShaderVariantCollection preloadShaders;
        [SerializeField] private int defaultConfigIndex = 1;
        [SerializeField] private List<QualityConfig> configs = new List<QualityConfig>();
        
        private int currentConfigIndex = -1;

        private EventController<QualityChangeEvent, QualityChangeEventParametrs> eventController;

        public void PostResolved()
        {
            eventController = new EventController<QualityChangeEvent, QualityChangeEventParametrs>(loadSceneManager);
            int savedIndex = preferenceManager.LoadValue<int>(OptionsKeysContainer.QualitySettingsIndexKey, defaultConfigIndex);

            if (preloadShaders != null && preloadShaders.isWarmedUp == false)
            {
                preloadShaders.WarmUp();
            }
            
            SetConfig(savedIndex);
        }
        
        public void AddListener<TA>(Action<QualityChangeEventParametrs> action, EventContextType eventContextType = EventContextType.Scene) where TA : QualityChangeEvent
        {
            eventController.AddListener<TA>(action, eventContextType);
        }

        public void RemoveListener<TR>(Action<QualityChangeEventParametrs> action, EventContextType eventContextType = EventContextType.Scene) where TR : QualityChangeEvent
        {
            eventController.RemoveListener<TR>(action, eventContextType);
        }

        public List<string> GetAvailableConfigsLocalizationKeys()
        {
            List<string> namesConfigs = new List<string>();

            foreach (QualityConfig config in configs)
            {
                namesConfigs.Add(config.TitleKey);
            }

            return namesConfigs;
        }

        public int GetCurrentConfigIndex()
        {
            return currentConfigIndex;
        }

        public int GetConfigsCount()
        {
            if (configs != null)
            {
                return configs.Count;
            }
            else
            {
                return 0;
            }
        }

        public float GetMaxFarClipPlane()
        {
            if (currentConfigIndex < 0 || currentConfigIndex > (configs.Count - 1))
            {
                throw new IndexOutOfRangeException();
            }

            return configs[currentConfigIndex].FarClipPlane;
        }

        public void SetConfig(int index)
        {
            if (index < 0 || index > (configs.Count - 1))
            {
                throw new IndexOutOfRangeException();
            }

            if (currentConfigIndex == index)
            {
                return;
            }

            QualityConfig config = GetQualityConfigByIndex(index);
            currentConfigIndex = index;
            preferenceManager.SaveValue<int>(OptionsKeysContainer.QualitySettingsIndexKey, currentConfigIndex);
            ApplyConfigs(config);
        }
        
        private QualityConfig GetQualityConfigByIndex(int index)
        {
            return configs[index];
        }

        private void ApplyConfigs(QualityConfig config)
        {
            int unityQualitySettingsLevel = config.UnityQualitySettingsIndex;

            QualitySettings.SetQualityLevel(unityQualitySettingsLevel, true);

            if (Application.isPlaying == true)
            {
                eventController.RaiseEvent<QualityChangeEvent>(new QualityChangeEventParametrs());
            }
        }
        
        public class QualityChangeEvent : BaseEvent
        {
        }

        public struct QualityChangeEventParametrs
        {
            
        }
    }
}