using System.Collections.Generic;
using CargoCult.Template.Common.EventController;
using CargoCult.Template.Injectro.Core;

namespace CargoCult.Template.Global.Quality
{
    public interface IQualityManager : IPostResolved, IEventController<QualityManager.QualityChangeEvent, QualityManager.QualityChangeEventParametrs>
    {
        int GetConfigsCount();
        int GetCurrentConfigIndex();
        List<string> GetAvailableConfigsLocalizationKeys();
        void SetConfig(int index);
        float GetMaxFarClipPlane();
    }
}