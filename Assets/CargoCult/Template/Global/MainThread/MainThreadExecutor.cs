﻿using System;
using System.Collections.Generic;
using CargoCult.Template.Common.EventController;
using CargoCult.Template.Global.LoadScene;
using CargoCult.Template.Injectro.Core;
using UnityEngine;

namespace CargoCult.Template.Global.MainThread
{
    [Singleton]
    [PostResolveDependency(typeof(ILoadSceneManager))]
    public class MainThreadExecutor : MonoBehaviour, IMainThreadExecutor, IPostResolved
    {
        private const int RunsPerFrame = 5;

        private readonly Queue<Action> globalRunnables = new Queue<Action>();
        private readonly Queue<Action> sceneRunnables = new Queue<Action>();

        [Inject] private ILoadSceneManager loadSceneManager;

        private bool isResolved;

        public void Run(Action runnable)
        {
            lock (this)
            {
                sceneRunnables.Enqueue(runnable);
            }
        }

        public void RunGlobal(Action runnable)
        {
            lock (this)
            {
                globalRunnables.Enqueue(runnable);
            }
        }
        
        public void PostResolved()
        {
            loadSceneManager.AddListener<UnloadSceneEvent>(OnSceneUnload, EventContextType.Global);
            isResolved = true;
        }
        
        private void Update()
        {
            if (!isResolved)
            {
                return;
            }

            int executedTasksCount = 0;
            while (executedTasksCount < RunsPerFrame)
            {
                Action action = null;
                lock (this)
                {
                    if (sceneRunnables.Count != 0)
                    {
                        action = sceneRunnables.Dequeue();
                    }
                    else if (globalRunnables.Count != 0)
                    {
                        action = globalRunnables.Dequeue();
                    }
                    else
                    {
                        return;
                    }
                }

                if (action == null)
                {
                    continue;
                }

                executedTasksCount++;
                try
                {
                    action.Invoke();
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }
        }

        private void OnSceneUnload(SceneEventParametrs sceneEvent)
        {
            lock (this)
            {
                sceneRunnables.Clear();
            }
        }
    }
}