﻿using System;
using CargoCult.Template.Injectro.Core;

namespace CargoCult.Template.Global.MainThread
{
    public interface IMainThreadExecutor : IBean
    {
        void Run(Action runnable);
        void RunGlobal(Action runnable);
    }
}