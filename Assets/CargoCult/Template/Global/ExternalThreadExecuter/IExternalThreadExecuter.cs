﻿using System;
using CargoCult.Template.Common.EventController;
using CargoCult.Template.Injectro.Core;

namespace CargoCult.Template.Global.ExternalThreadExecuter
{
	public interface IExternalThreadExecuter : IBean
	{
		void Run(Action<ExternalThreadExecuter.ExecuteInfo> runnable, EventContextType contextType = EventContextType.Scene);
		void StopAll();
	}
}
