﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace CargoCult.Template.Global.ExternalThreadExecuter
{
    public class BlockingQueue<T> : IEnumerable<T>
    {

        private readonly Queue<T> queue = new Queue<T>();

        public int Count()
        {
            lock (queue)
            {
                return queue.Count;
            }
        }

        public T Dequeue()
        {
            lock (queue)
            {
                return queue.Dequeue();
            }
        }

        public void Enqueue(T data)
        {
            if (data == null) throw new ArgumentNullException("data");

            lock (queue)
            {
                queue.Enqueue(data);
            }
        }

        public void Clear()
        {
            lock (queue)
            {
                queue.Clear();
            }
        }

        // Lets the consumer thread consume the queue with a foreach loop.
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            while (true) yield return Dequeue();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<T>)this).GetEnumerator();
        }
    }
}