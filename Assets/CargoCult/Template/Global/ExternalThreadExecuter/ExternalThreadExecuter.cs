﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using CargoCult.Template.Common.EventController;
using CargoCult.Template.Global.LoadScene;
using CargoCult.Template.Global.MainThread;
using CargoCult.Template.Injectro.Core;
using UnityEngine;

namespace CargoCult.Template.Global.ExternalThreadExecuter
{
    [Singleton]
    [PostResolveDependency(typeof(ILoadSceneManager))]
    public class ExternalThreadExecuter : MonoBehaviour, IExternalThreadExecuter, IPostResolved,
        ExternalThreadExecuter.ExecuteInfo
    {
        private readonly ReadOnlyDictionary<EventContextType, Queue<Action<ExecuteInfo>>> tasks =
            new ReadOnlyDictionary<EventContextType, Queue<Action<ExecuteInfo>>>(
                new Dictionary<EventContextType, Queue<Action<ExecuteInfo>>>
                {
                    {EventContextType.Global, new Queue<Action<ExecuteInfo>>()},
                    {EventContextType.Scene, new Queue<Action<ExecuteInfo>>()},
                });
        
        [Inject] private IMainThreadExecutor mainThreadExecutor;
        [Inject] private ILoadSceneManager loadSceneManager;
        
        private Thread externalThread;
        private bool sceneChanged;

        public void PostResolved()
        {
            loadSceneManager.AddListener<UnloadSceneEvent>(sceneEvent => { SceneSwitch(); }, EventContextType.Global);

            externalThread = new Thread(() =>
            {
                try
                {
                    ExternalThreadLoop();
                }
                catch (Exception e)
                {
                    // ExternalThreadLoop exception. must not happen
                    mainThreadExecutor.Run(() => { Debug.LogError(e); });
                }
            });

            externalThread.Start();
        }

        public bool IsSceneChanged()
        {
            return sceneChanged;
        }

        private void SceneSwitch()
        {
            lock (tasks)
            {
                tasks[EventContextType.Scene].Clear();
                sceneChanged = true;
            }
        }

        public void Run(Action<ExecuteInfo> action, EventContextType contextType = EventContextType.Scene)
        {
            if (action == null)
            {
                Debug.LogError("ExternalThreadExecutor: cannot run null action");
                return;
            }

            lock (tasks)
            {
                tasks[contextType].Enqueue(action);
                Monitor.PulseAll(tasks);
            }
        }

        public void StopAll()
        {
            lock (tasks)
            {
                foreach (KeyValuePair<EventContextType,Queue<Action<ExecuteInfo>>> pair in tasks)
                {
                    pair.Value.Clear();
                }
            }
        }

        private void ExternalThreadLoop()
        {
            Action<ExecuteInfo> task;
            while (true)
            {
                task = null;
                lock (tasks)
                {
                    foreach (KeyValuePair<EventContextType, Queue<Action<ExecuteInfo>>> pair in tasks)
                    {
                        if (task != null || pair.Value.Count == 0)
                        {
                            break;
                        }

                        task = pair.Value.Dequeue();
                    }
                    
                    if (task == null)
                    {
                        Monitor.Wait(tasks);
                        continue;
                    }
                }

                sceneChanged = false;
                try
                {
                    task(this);
                }
                catch (Exception e)
                {
                    mainThreadExecutor.Run(() =>
                    {
                        // task inner exception
                        Debug.LogError(e);
                    });
                }
            }
        }

        private void OnDestroy()
        {
            StopAll();
        }

        public interface ExecuteInfo
        {
            bool IsSceneChanged();
        }
    }
}