﻿using CargoCult.Template.Injectro.Core;
using CargoCult.Template.SelectiveStrings;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CargoCult.Template.Global.Sound
{
    public class PointerDownSound : MonoBehaviour, IPointerDownHandler
    {
        [SelectiveString("SoundTypes")] public string Sound;
        private ISoundManager soundManager;

        private void Start()
        {
            soundManager = GlobalContext.Instance.GetBean<ISoundManager>();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            soundManager.PlayOneShotUI(Sound);
        }
    }
}