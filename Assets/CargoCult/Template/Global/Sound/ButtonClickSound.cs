﻿using CargoCult.Template.Injectro.Core;
using CargoCult.Template.SelectiveStrings;
using UnityEngine;
using UnityEngine.UI;

namespace CargoCult.Template.Global.Sound
{
    [RequireComponent(typeof(Button))]
    public class ButtonClickSound : MonoBehaviour
    {
        [SelectiveString("SoundTypes")] [SerializeField] 
        private string sound;
        private ISoundManager soundManager;

        private void Start()
        {
            //TODO заюзать prototype beans, когда они будут готовы
            soundManager = GlobalContext.Instance.GetBean<ISoundManager>();
            GetComponent<Button>().onClick.AddListener(PlaySound);
        }

        private void PlaySound()
        {
            soundManager.PlayOneShotUI(sound);
        }
    }
}