﻿using CargoCult.Template.Common.Pool;
using UnityEngine;

namespace CargoCult.Template.Global.Sound
{
	public class Playback
	{
		public readonly int Id;
		
		private readonly AudioSource sourceInstance;
		private readonly float expireTime;
		private readonly IPoolManager pool;
		private readonly float defaultPitch;
		private readonly float defaultVolume;
		private readonly float defaultSpatialBlend;

		private Transform targetTransform;
		
		public bool IsExpired
		{
			get { return sourceInstance==null || !sourceInstance.loop && Time.unscaledTime >= expireTime; }
		}

		public Playback(int id,
			IPoolManager pool,
			RuntimeSoundInfo soundInfo,
			float additionalClipLength)
		{
			Id = id;
			this.pool = pool;
			sourceInstance = this.pool.GetFromPool(soundInfo.SourcePrefab);
			defaultPitch = sourceInstance.pitch;
			defaultVolume = sourceInstance.volume;
			defaultSpatialBlend = sourceInstance.spatialBlend;
			expireTime = Time.unscaledTime + soundInfo.Clip.length + additionalClipLength;
			sourceInstance.transform.position = Vector3.zero;
			sourceInstance.clip = soundInfo.Clip;
			sourceInstance.Play();
		}

		public Playback(int id, 
			IPoolManager pool, 
			RuntimeSoundInfo soundInfo,
			Vector3 position,
			float additionalClipLength): this(id, pool, soundInfo, additionalClipLength)
		{
			sourceInstance.transform.position = position;
		}

		public Playback(int id,
			IPoolManager pool,
			RuntimeSoundInfo soundInfo,
			Transform targetTransform,
			float additionalClipLength): this(id, pool, soundInfo, additionalClipLength)
		{
			this.targetTransform = targetTransform;
			sourceInstance.transform.position = targetTransform.position;
		}

		public void PerformUpdate()
		{
			if (targetTransform)
			{
				sourceInstance.transform.position = targetTransform.position;
			}
		}

		public void SetPitch(float pitch)
		{
			sourceInstance.pitch = pitch;
		}

		public void SetVolume(float volume)
		{
			sourceInstance.volume = volume;
		}

		public void SetSpatialBlend(float spatialBlend)
		{
			sourceInstance.spatialBlend = spatialBlend;
		}

		public void SetTarget(Transform target)
		{
			targetTransform = target;
		}

		public void Destroy()
		{
			if (null == sourceInstance)
			{
				return;
			}
			if (sourceInstance.isPlaying)
			{
				sourceInstance.Stop();				
			}

			sourceInstance.time = 0;
			sourceInstance.pitch = defaultPitch;
			sourceInstance.volume = defaultVolume;
			sourceInstance.spatialBlend = defaultSpatialBlend;
			sourceInstance.clip = null;
			pool.ReturnToPool(sourceInstance);
		}

		public float GetDistanceToPoint(Vector3 point)
		{
			return Vector3.Distance(sourceInstance.transform.position, point) * sourceInstance.spatialBlend;
		}

		public float GetTimeToEnd()
		{
			return sourceInstance.loop ? float.PositiveInfinity : Mathf.Min(expireTime - Time.unscaledTime, 0f);
		}
	}
}
