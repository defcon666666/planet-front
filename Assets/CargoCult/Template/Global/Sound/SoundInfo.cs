﻿using System;
using System.Collections.Generic;
using CargoCult.Template.Global.Database;
using CargoCult.Template.SelectiveStrings;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CargoCult.Template.Global.Sound
{
    [Serializable]
    [CreateAssetMenu(fileName = "NewSoundInfo", menuName = "Configs/SoundInfo", order = 0)]   
    public class SoundInfo : ScriptableObject, IInfo
    {
        [SerializeField] [SelectiveString("SoundTypes")]
        private string soundType;
        [SerializeField] private AudioSource sourcePrefab;
        [SerializeField] private List<AudioClip> clips;
        public string GetName()
        {
            return soundType;
        }

        public RuntimeSoundInfo GetRuntimeSoundInfo()
        {
            AudioClip randomClip = clips[Random.Range(0, clips.Count)];
            return new RuntimeSoundInfo {Clip = randomClip, SourcePrefab = sourcePrefab};
        }
    }
}
