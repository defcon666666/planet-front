﻿using System;
using UnityEngine;

namespace CargoCult.Template.Global.Sound
{
    [Serializable]
    public struct SoundChannelSetting
    {
        [SerializeField] private AudioSource sourceIdentifier;
        [SerializeField] private int quantityLimit;

        public AudioSource Source => sourceIdentifier;
        public int QuantityLimit => quantityLimit;

        public SoundChannelSetting(AudioSource source, int quantityLimit)
        {
            sourceIdentifier = source;
            this.quantityLimit = quantityLimit;
        }
    }
}