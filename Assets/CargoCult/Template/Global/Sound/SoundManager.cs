﻿using System;
using System.Collections.Generic;
using System.Linq;
using CargoCult.Template.Common.EventController;
using CargoCult.Template.Common.KeysContrainers;
using CargoCult.Template.Common.Pool;
using CargoCult.Template.Global.Database;
using CargoCult.Template.Global.LoadScene;
using CargoCult.Template.Global.Preference;
using CargoCult.Template.Injectro.Core;
using CargoCult.Template.SelectiveStrings;
using UnityEngine;
using UnityEngine.Audio;

namespace CargoCult.Template.Global.Sound
{
    [Singleton]
    [PostResolveDependency(typeof(IPreferenceManager),typeof(IPoolManager),typeof(ILoadSceneManager))]
    public class SoundManager : MonoBehaviour, ISoundManager
    {
        private const int MaxSoundHandlesPerUpdate = 20;
        private const int MaxCrossfadeHandlesPerUpdate = 20;
        private const float MinDecibelLevel = -80f;
        //ключи для доступа к exposed parameters микшера
        private const string MasterVolumeKey = "masterVol";
        private const string SoundVolumeKey = "soundVol";
        private const string MusicVolumeKey = "musicVol";
        private const float SnapshotTransitionTime = 0.1f;
        private const string mainSnapshotName = "Main";
        private const string pauseSnapshotName = "Pause";

        public AudioMixer Mixer;

        [SerializeField] private AudioSource uiSoundEmitter;
        
        private static int playbackIdCounter;
        
        private AudioMixerSnapshot normalSnapshot;
        private AudioMixerSnapshot pausedSnapshot;

        private readonly ICollection<int> deletePlaybackIdSet = new HashSet<int>();
        private readonly IDictionary<int, Playback> activePlaybackMap = new Dictionary<int, Playback>();
        private readonly Queue<Playback> playbackQueue = new Queue<Playback>();
        private readonly Queue<Crossfade> crossfadeQueue = new Queue<Crossfade>();
        private readonly List<ISoundPauseChanger> pauseChangers = new List<ISoundPauseChanger>();
        private readonly List<int> notDestroyableSounds = new List<int>();
        private readonly List<int> toDestroyCache = new List<int>();

        [Inject] private IPoolManager poolManager;
        [Inject] private IPreferenceManager preferenceManager;
        [Inject] private InjectroContext context;
        [Inject] private ILoadSceneManager loadSceneManager;
        [Inject] private IDatabaseManager databaseManager;
        [Inject] private IChannelManager channelManager;

        private bool soundStatus;
        private float curMasterVolume;
        private float curMusicVolume;
        private float curSoundVolume;
        private bool isPostResolved = false;

        public void PostResolved()
        {
            if (!context.IsTest() && !IsDatabaseOk())
            {
                throw new Exception("All types of sounds declared in SoundType selective string container should exist in database");
            }

            loadSceneManager.AddListener<UnloadSceneEvent>(OnSceneUnload, EventContextType.Global);

            normalSnapshot = Mixer.FindSnapshot(mainSnapshotName);
            pausedSnapshot = Mixer.FindSnapshot(pauseSnapshotName);
            
            LoadVolumes();
            isPostResolved = true;
        }

        public void PlayOneShotUI(string type)
        {
            if (type == SoundType.None || string.IsNullOrEmpty(type))
            {
                return;
            }
            
            RuntimeSoundInfo soundInfo = GetSoundInfo(type);
            uiSoundEmitter.PlayOneShot(soundInfo.Clip);
        }

        public int Play(string type, float additionalClipLength = 1f)
        {
            if (type == SoundType.None || string.IsNullOrEmpty(type))
            {
                return -1;
            }

            playbackIdCounter++;
            RuntimeSoundInfo soundInfo = GetSoundInfo(type);
            if (channelManager.FillChannelWith(soundInfo, playbackIdCounter))
            {
                Playback newPlayback = new Playback(playbackIdCounter, poolManager, soundInfo, additionalClipLength);
                RegisterPlayback(newPlayback);
                return playbackIdCounter;
            }

            playbackIdCounter--;
            return -1;
        }

        public int Play(string type, Vector3 position, float additionalClipLength = 0f)
        {
            if (type == SoundType.None || string.IsNullOrEmpty(type))
            {
                return -1;
            }
            
            playbackIdCounter++;
            RuntimeSoundInfo soundInfo = GetSoundInfo(type);
            if (channelManager.FillChannelWith(soundInfo, playbackIdCounter, position))
            {
                Playback newPlayback = new Playback(playbackIdCounter, poolManager, soundInfo, position,
                    additionalClipLength);
                RegisterPlayback(newPlayback);
                return playbackIdCounter;
            }
            
            playbackIdCounter--;
            return -1;
        }

        public int Play(string type, Transform targetTransform, float additionalClipLength = 0f)
        {
            if (type == SoundType.None)
            {
                return -1;
            }
            playbackIdCounter++;
            RuntimeSoundInfo soundInfo = GetSoundInfo(type);
            if (channelManager.FillChannelWith(soundInfo, playbackIdCounter, targetTransform.position))
            {
                Playback newPlayback = new Playback(playbackIdCounter, poolManager, soundInfo, targetTransform,
                    additionalClipLength);
                RegisterPlayback(newPlayback);
                return playbackIdCounter;
            }
            
            playbackIdCounter--;
            return -1;
        }

        public int CrossfadeTo(int playbackId, string type, float time)
        {
            if (type == SoundType.None)
            {
                return -1;
            }

            Playback p;
            if (!activePlaybackMap.TryGetValue(playbackId, out p))
            {
                return -1;
            }
            
            playbackIdCounter++;
            Playback newPlayback = new Playback(playbackIdCounter, poolManager, GetSoundInfo(type), 0f);
            RegisterPlayback(newPlayback);

            crossfadeQueue.Enqueue(new Crossfade {Progress = 0, From = p, To = newPlayback, Time = time});

            return playbackIdCounter;
        }

        public float GetDistanceToPoint(int playbackId, Vector3 point)
        {
            if (activePlaybackMap.TryGetValue(playbackId, out Playback playback))
            {
                return playback.GetDistanceToPoint(point);
            }
            
            return float.PositiveInfinity;
        }

        public float GetTimeToEnd(int playbackId)
        {
            if (activePlaybackMap.TryGetValue(playbackId, out Playback playback))
            {
                return playback.GetTimeToEnd();
            }
            
            return 0f;
        }

        private void CrossfadeUpdate()
        {
            int curHandleCount = Mathf.Min(MaxCrossfadeHandlesPerUpdate, crossfadeQueue.Count);
            for (int i = 0; i < curHandleCount; i++)
            {
                Crossfade curCrossfade = crossfadeQueue.Dequeue();

                float progress = (curCrossfade.Progress * curCrossfade.Time + Time.unscaledDeltaTime) /
                                 curCrossfade.Time;
                if (!curCrossfade.From.IsExpired)
                {
                    curCrossfade.From.SetVolume(1f - progress);                    
                }

                if (!curCrossfade.To.IsExpired)
                {
                    curCrossfade.To.SetVolume(progress);
                }

                curCrossfade.Progress = progress;
                
                if (curCrossfade.Progress>=1f)
                {
                    DeletePlayback(curCrossfade.From);
                    curCrossfade.To.SetVolume(1f);
                    continue;
                }

                crossfadeQueue.Enqueue(curCrossfade);
            }
        }

        public void SetPause(ISoundPauseChanger pauseChanger, bool isPaused)
        {
            if (!isPaused)
            {
                pauseChangers.Remove(pauseChanger);
            }
            else if (!pauseChangers.Contains(pauseChanger))
            {
                pauseChangers.Add(pauseChanger);
            }

            if (pauseChangers.Count > 0)
            {
                pausedSnapshot.TransitionTo(SnapshotTransitionTime);
            }
            else
            {
                normalSnapshot.TransitionTo(SnapshotTransitionTime);
            }
        }

        public void ToggleNotDestroyableState(int playbackID, bool state)
        {
            if (state && !notDestroyableSounds.Contains(playbackID))
            {
                notDestroyableSounds.Add(playbackID);
            }
            else if (!state && notDestroyableSounds.Contains(playbackID))
            {
                notDestroyableSounds.Remove(playbackID);
            }
        }

        public float GetMusicVolume()
        {
            return curMusicVolume;
        }

        public float GetSoundVolume()
        {
            return curSoundVolume;
        }

        public bool GetEnableSoundStatus()
        {
            return soundStatus;
        }

        public bool IsPlaying(int playbackId)
        {
            return activePlaybackMap.ContainsKey(playbackId);
        }

        public void SetMusicVolume(float volume)
        {
            curMusicVolume = volume;
            Mixer.SetFloat(MusicVolumeKey, VolumeNormalizeToDecibel(curMusicVolume));
            preferenceManager.SaveValue(OptionsKeysContainer.MusicVolumeKey, curMusicVolume);
        }

        public void SetSoundVolume(float volume)
        {
            curSoundVolume = volume;
            Mixer.SetFloat(SoundVolumeKey, VolumeNormalizeToDecibel(curSoundVolume));
            preferenceManager.SaveValue(OptionsKeysContainer.SoundVolumeKey, curSoundVolume);
        }

        public void SetEnableSoundsStatus(bool status)
        {
            soundStatus = status;
            curMasterVolume = status ? 1 : 0;
            Mixer.SetFloat(MasterVolumeKey, VolumeNormalizeToDecibel(curMasterVolume));
            preferenceManager.SaveValue(OptionsKeysContainer.MasterVolumeKey, status);
        }

        public void SetPitch(int playbackId, float pitch)
        {
            Playback p;
            if (activePlaybackMap.TryGetValue(playbackId, out p))
            {
                p.SetPitch(pitch);
            }
        }

        public void SetVolume(int playbackId, float volume)
        {
            Playback p;
            if (activePlaybackMap.TryGetValue(playbackId, out p))
            {
                p.SetVolume(volume);
            }
        }

        public void SetSpatialBlend(int playbackId, float spatialBlend)
        {
            if (activePlaybackMap.TryGetValue(playbackId, out Playback p))
            {
                p.SetSpatialBlend(spatialBlend);
            }
        }

        public void SetParams(int playbackId, float volume, float pitch)
        {
            if (activePlaybackMap.TryGetValue(playbackId, out Playback p))
            {
                p.SetVolume(volume);
                p.SetPitch(pitch);
            }
        }

        public void SetTarget(int playbackId, Transform target)
        {
            if (activePlaybackMap.TryGetValue(playbackId, out Playback p))
            {
                p.SetTarget(target);
            }
        }

        public void Stop(int id)
        {
            if (activePlaybackMap.ContainsKey(id))
            {
                deletePlaybackIdSet.Add(id);
            }
        }

        //HACK если сетать значения в AudioMixer в Awake (PostResolve вызывается в нем), то в едиторе ничего сетаться не будет
        private void Start()
        {
            if (isPostResolved)
            {
                LoadVolumes();              
            }
        }

        private void LoadVolumes()
        {
            SetEnableSoundsStatus(preferenceManager.LoadValue(OptionsKeysContainer.MasterVolumeKey, true));
            SetMusicVolume(preferenceManager.LoadValue(OptionsKeysContainer.MusicVolumeKey, 0.25f));
            SetSoundVolume(preferenceManager.LoadValue(OptionsKeysContainer.SoundVolumeKey, 0.5f));
        }

        private static float VolumeNormalizeToDecibel(float normVal)
        {
            float dB;
            if (!Mathf.Approximately(normVal, 0))
            {
                dB = 20.0f * Mathf.Log10(normVal);//формула из екзамплов юнити
            }
            else
            {
                dB = MinDecibelLevel;
            }
            return dB;
        }

        private void RegisterPlayback(Playback playback)
        {
            activePlaybackMap.Add(playback.Id, playback);
            playbackQueue.Enqueue(playback);
        }

        private void DeletePlayback(Playback playback)
        {
            channelManager.CleatChannelFrom(playback.Id);
            deletePlaybackIdSet.Remove(playback.Id);
            activePlaybackMap.Remove(playback.Id);
            playback.Destroy();
        }

        private RuntimeSoundInfo GetSoundInfo(string type)
        {
            return databaseManager.GetInfo<SoundInfo>(type).GetRuntimeSoundInfo();
        }

        private bool IsDatabaseOk()
        {
            #if UNITY_EDITOR
            //В базе данных должны быть все звуки, объявленные в selective string container SoundType
            bool isOk = true;
            List<SoundInfo> allSounds = databaseManager.GetAllInfoByType<SoundInfo>();
            foreach (string type in SelectiveStringTagsContainer.SoundTypes)
            {
                if (type == SoundType.None)
                {
                    continue;
                }
                
                if (allSounds.Any(i=>i.GetName()==type))
                {
                    continue;
                }
                isOk = false;
                Debug.LogErrorFormat("No sound of type {0} found in SoundDatabase", type);
            }
            return isOk;
            #endif
            
            return true;
        }

        private void Update()
        {
            int curHandleCount = Mathf.Min(MaxSoundHandlesPerUpdate, playbackQueue.Count);
            for (int i = 0; i < curHandleCount; i++)
            {
                Playback curPlayback = playbackQueue.Dequeue();

                //если воспроизведение закончилось или было отменено, удаляем его (т.е. не возвращаем в очередь)
                if (curPlayback.IsExpired || deletePlaybackIdSet.Contains(curPlayback.Id))
                {
                    DeletePlayback(curPlayback);
                    continue;
                }

                //иначе, обновляем позицию и пихаем обратно в очередь
                curPlayback.PerformUpdate();
                playbackQueue.Enqueue(curPlayback);
            }
            
            CrossfadeUpdate();
        }

        private void OnSceneUnload(SceneEventParametrs sceneEvent)
        {
            toDestroyCache.Clear();
            
            foreach (KeyValuePair<int, Playback> playback in activePlaybackMap)
            {
                if (notDestroyableSounds.Contains(playback.Key))
                {
                    continue;
                }
                
                playback.Value.Destroy();
                toDestroyCache.Add(playback.Key);
            }

            for (int i = 0; i < toDestroyCache.Count; i++)
            {
                int id = toDestroyCache[i];
                activePlaybackMap.Remove(id);
            }
        }
        
        private class Crossfade
        {
            public float Progress;
            public float Time;
            public Playback From;
            public Playback To;
        }
    }
}