﻿using System.Collections.Generic;
using UnityEngine;

namespace CargoCult.Template.Global.Sound
{
    public partial class ChannelManager
    {
#if UNITY_EDITOR
        [SerializeField] private SoundDatabase soundDatabase;
        
        public void UpdateChannels(List<int> unusedIndexes, List<int> updatedIndexes)
        {
            List<AudioSource> databaseSources = new List<AudioSource>();
            List<SoundInfo> databaseInfos = soundDatabase.GetEntities();
            for (int i = 0; i < databaseInfos.Count; i++)
            {
                AudioSource source = databaseInfos[i].GetRuntimeSoundInfo().SourcePrefab;
                if (!databaseSources.Contains(source))
                {
                    databaseSources.Add(source);
                }
            }

            unusedIndexes.Clear();
            List<AudioSource> beginSources = new List<AudioSource>();
            for (int i = 0; i < channelsSetting.Length; i++)
            {
                SoundChannelSetting beginChannelSetting = channelsSetting[i];

                if (beginChannelSetting.Source != null)
                {
                    beginSources.Add(beginChannelSetting.Source);
                }

                if (beginChannelSetting.Source == null || !databaseSources.Contains(beginChannelSetting.Source))
                {
                    unusedIndexes.Add(i);
                }
            }

            updatedIndexes.Clear();
            List<SoundChannelSetting> updatedChannels = new List<SoundChannelSetting>(channelsSetting);
            int counter = beginSources.Count;
            for (int i = 0; i < databaseSources.Count; i++)
            {
                AudioSource source = databaseSources[i];
                if (!beginSources.Contains(source))
                {
                    updatedChannels.Add(new SoundChannelSetting(source, defaultQuantityLimit));
                    updatedIndexes.Add(counter);
                    counter++;
                }
            }

            channelsSetting = updatedChannels.ToArray();
        }
#endif
    }
}