﻿using CargoCult.Template.Injectro.Core;
using UnityEngine;

namespace CargoCult.Template.Global.Sound
{
    public interface IChannelManager : IBean
    {
        bool FillChannelWith(RuntimeSoundInfo runtimeSoundInfo, int soundId, Vector3 position = new Vector3());
        void CleatChannelFrom(int soundId);
    }
}