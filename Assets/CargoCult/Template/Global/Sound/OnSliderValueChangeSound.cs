﻿using CargoCult.Template.Injectro.Core;
using CargoCult.Template.SelectiveStrings;
using UnityEngine;
using UnityEngine.UI;

namespace CargoCult.Template.Global.Sound
{
    [RequireComponent(typeof(Slider))]
    public class OnSliderValueChangeSound : MonoBehaviour
    {
        [SelectiveString("SoundTypes")] public string Sound;
        private ISoundManager soundManager;
        private Slider slider;

        private void Start()
        {
            soundManager = GlobalContext.Instance.GetBean<ISoundManager>();
        }

        private void OnEnable()
        {
            slider = GetComponent<Slider>();
            slider.onValueChanged.AddListener(onValueChange);
        }

        private void OnDisable()
        {
            slider.onValueChanged.RemoveListener(onValueChange);
        }

        private void onValueChange(float value)
        {
            if (null == soundManager)
            {
                return;
            }

            soundManager.Play(Sound);
        }
    }
}