﻿using System.Collections.Generic;
using CargoCult.Template.Global.LoadScene;
using CargoCult.Template.Injectro.Core;
using UnityEngine;

namespace CargoCult.Template.Global.Sound
{
    [Singleton]
    [PostResolveDependency(typeof(ILoadSceneManager))]
    public partial class ChannelManager : MonoBehaviour, IChannelManager
    {
        [SerializeField] private int defaultQuantityLimit;
        [SerializeField] private float minCriticalRadius = 0.8f;
        [SerializeField] private SoundChannelSetting[] channelsSetting;

        [Inject] private ISoundManager soundManager;
        [Inject] private ILoadSceneManager loadSceneManager;

        private Dictionary<AudioSource, Channel> sourceToChannel = null;

        private Dictionary<AudioSource, Channel> SourceToChannel
        {
            get
            {
                if (sourceToChannel == null)
                {
                    sourceToChannel = new Dictionary<AudioSource, Channel>();
                    for (int i = 0; i < channelsSetting.Length; i++)
                    {
                        sourceToChannel.Add(channelsSetting[i].Source, new Channel(channelsSetting[i]));
                    }
                }

                return sourceToChannel;
            }
        }

        public bool FillChannelWith(RuntimeSoundInfo runtimeSoundInfo, int soundId, Vector3 position = new Vector3())
        {
            AudioSource source = runtimeSoundInfo.SourcePrefab;
            if (!SourceToChannel.ContainsKey(source))
            {
                SourceToChannel.Add(source,
                    new Channel(new SoundChannelSetting(source, defaultQuantityLimit)));
            }

            Channel channel = SourceToChannel[source];

            if (channel.SoundsIds.Count >= channel.Setting.QuantityLimit)
            {
                if (channel.Setting.QuantityLimit <= 0)
                {
                    return false;
                }

                float spatialBlend = source.spatialBlend;
                int distantId = -1;
                int durationId = -1;

                float thresholdMinDistance = minCriticalRadius * spatialBlend;
                float minEndTime = float.PositiveInfinity;
                float distance = 0f;

                for (int i = 0; i < channel.SoundsIds.Count; i++)
                {
                    int id = channel.SoundsIds[i];
                    
                    if (distance > thresholdMinDistance)
                    {
                        float endTime = soundManager.GetTimeToEnd(id);

                        if (endTime < minEndTime)
                        {
                            minEndTime = endTime;
                            durationId = id;
                        }
                    }
                }

                if (distantId >= 0)
                {
                    channel.SoundsIds.Remove(distantId);
                    soundManager.Stop(distantId);
                }
                else if (durationId >= 0)
                {
                    channel.SoundsIds.Remove(durationId);
                    soundManager.Stop(durationId);
                }
                else
                {
                    return false;
                }
            }

            channel.SoundsIds.Add(soundId);
            return true;
        }

        public void CleatChannelFrom(int soundId)
        {
            foreach (AudioSource source in SourceToChannel.Keys)
            {
                Channel channel = SourceToChannel[source];
                if (channel.SoundsIds.Contains(soundId))
                {
                    channel.SoundsIds.Remove(soundId);
                    break;
                }
            }
        }
        
        private class Channel
        {
            public readonly SoundChannelSetting Setting;
            public readonly List<int> SoundsIds = new List<int>();

            public Channel(SoundChannelSetting setting)
            {
                Setting = setting;
            }
        }
    }
}