﻿using CargoCult.Template.Injectro.Core;
using UnityEngine;

namespace CargoCult.Template.Global.Sound
{
    public interface ISoundManager : IPostResolved
    {
        void PlayOneShotUI(string type);
        /// <summary>
        /// Plays random sound of specified type in zero position. Shoud be used for 2d sounds.
        /// </summary>
        /// <param name="type">Sound type</param>
        /// <param name="additionalClipLength"></param>
        /// <returns>Playback Id</returns>
        int Play(string type, float additionalClipLength = 0f);

        /// <summary>
        /// Plays random sound of specified type in specified position.
        /// </summary>
        /// <param name="type">Sound type</param>
        /// <param name="position">Sound position</param>
        /// <param name="additionalClipLength"></param>
        /// <returns>Playback Id</returns>
        int Play(string type, Vector3 position, float additionalClipLength = 0f);

        /// <summary>
        /// Plays random sound of specified type, witch will follow specified Transform.
        /// </summary>
        /// <param name="type">Sound type</param>
        /// <param name="targetTransform">Transform to follow</param>
        /// <param name="additionalClipLength"></param>
        /// <returns></returns>
        int Play(string type, Transform targetTransform, float additionalClipLength = 0f);

        int CrossfadeTo(int playbackId, string type, float time);

        float GetDistanceToPoint(int playbackId, Vector3 point);

        float GetTimeToEnd(int playbackId);

        bool IsPlaying(int playbackId);

        /// <summary>
        /// Stops playback by id. If it doesn't exists, nothing happends.
        /// </summary>
        /// <param name="playbackId">Playback Id</param>
        void Stop(int playbackId);

        /// <summary>
        /// Set pitch for playback with specified id. If it doesn't exists, nothing happends.
        /// </summary>
        /// <param name="playbackId">Playback id</param>
        /// <param name="pitch">Pitch</param>
        void SetPitch(int playbackId, float pitch);
        void SetVolume(int playbackId, float volume);
        void SetSpatialBlend(int playbackId, float spatialBlend);
        void SetParams(int playbackId, float volume, float pitch);
        void SetTarget(int playbackId, Transform target);

        void SetPause(ISoundPauseChanger pauseChanger, bool isPaused);

        void ToggleNotDestroyableState(int playbackID, bool state);

        float GetMusicVolume();
        float GetSoundVolume();
        bool GetEnableSoundStatus();

        void SetMusicVolume(float volume);
        void SetSoundVolume(float volume);
        void SetEnableSoundsStatus(bool status);
    }
}