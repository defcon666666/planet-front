﻿using CargoCult.Template.Global.Database;
using UnityEngine;

namespace CargoCult.Template.Global.Sound
{
    [CreateAssetMenu(fileName = "NewSoundDatabase", menuName = "Databases/SoundDatabase", order = 0)]
    public class SoundDatabase : Database<SoundInfo>
    {
      
    }

    public struct RuntimeSoundInfo
    {
        public AudioClip Clip;
        public AudioSource SourcePrefab;
    }

    public static class SoundType
    {
        public const string None = "None";
        
        public const string ButtonClick = "UI/ButtonClick";
        public const string BackgroundMusic = "BackgroundMusic";
        public const string WinSound = "WinSound";
        public const string LooseSound = "LooseSound";
        public const string OpenDoor = "OpenDoor";
        public const string LaunchMissile = "LaunchMissile";
        public const string CoinUp = "CoinUp";
        public const string ShipDamage = "ShipDamage";
        public const string ShipDead = "ShipDead";
        public const string PlanetDead = "PlanetDead";
        public const string PlanetDamage = "PlanetDamage";
        public const string UpgradeUp = "UpgradeUp";
    }
}