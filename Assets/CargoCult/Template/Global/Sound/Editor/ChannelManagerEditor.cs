﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace CargoCult.Template.Global.Sound.Editor
{
    [CustomEditor(typeof(ChannelManager))]
    public class ChannelManagerEditor : UnityEditor.Editor
    {
        private const float HighlightTime = 10f;

        private const string ScriptName = "m_Script";
        private const string ChannelsSettingPropertyName = "channelsSetting";

        private static readonly List<string> customPropertyNames = new List<string>()
        {
            ScriptName,
            ChannelsSettingPropertyName
        };
        
        private readonly List<int> unusedIndexes = new List<int>();
        private readonly List<int> updatedIndexes = new List<int>();
        private readonly Color unusedColor = Color.red;
        private readonly Color updatedColor = Color.green;
        private float endHighlightTime;

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            
            using (SerializedProperty iterator = serializedObject.GetIterator())
            {
                bool expanded = true;
                while (iterator.NextVisible(expanded))
                {
                    expanded = false;
                    if (customPropertyNames.Contains(iterator.name))
                    {
                        CustomPropertyField(iterator);
                    }
                    else
                    {
                        EditorGUILayout.PropertyField(iterator, true);
                    }
                }
            }

            if (GUILayout.Button("Update list"))
            {
                ChannelManager channelManager = (ChannelManager) target;
                Undo.RecordObject(channelManager, "Update list");
                channelManager.UpdateChannels(unusedIndexes, updatedIndexes);
                EditorUtility.SetDirty(target);
                endHighlightTime = Time.realtimeSinceStartup + HighlightTime;

            }
            serializedObject.ApplyModifiedProperties();
        }

        private void OnEnable()
        {
            endHighlightTime = 0;
        }

        private void CustomPropertyField(SerializedProperty property)
        {
            switch (property.name)
            {
                case ScriptName:
                    using (new EditorGUI.DisabledScope(true))
                    {
                        EditorGUILayout.PropertyField(property, true);
                    }
                    break;
                case ChannelsSettingPropertyName:
                    float highlightMultiply = Mathf.Clamp01((endHighlightTime - Time.realtimeSinceStartup) / HighlightTime);
                    if (highlightMultiply <= 0 ||!property.isArray)
                    {
                        EditorGUILayout.PropertyField(property, property.hasVisibleChildren);
                    }
                    else
                    {
                        if (EditorGUILayout.PropertyField(property, false))
                        {
                            EditorGUI.indentLevel++;
                            property.arraySize = EditorGUILayout.IntField("Size", property.arraySize);
                            Color simpleColor = GUI.backgroundColor;
                            bool hasChangeColor = false;
                            Color changeColor = GUI.backgroundColor;
                            for (int i = 0; i < property.arraySize; i++)
                            {
                                hasChangeColor = false;
                                if (unusedIndexes.Contains(i))
                                {
                                    changeColor = Color.Lerp(simpleColor, unusedColor, highlightMultiply);
                                    hasChangeColor = true;
                                }
                                
                                if (updatedIndexes.Contains(i))
                                {
                                    changeColor = Color.Lerp(simpleColor, updatedColor, highlightMultiply);
                                    hasChangeColor = true;
                                }

                                using (SerializedProperty elementProperty = property.GetArrayElementAtIndex(i))
                                {
                                    if (hasChangeColor)
                                    {
                                        Rect rect = GUILayoutUtility.GetLastRect();
                                        rect.y += rect.height;
                                        rect.height = EditorGUI.GetPropertyHeight(elementProperty,
                                            elementProperty.hasVisibleChildren);
                                        EditorGUI.DrawRect(rect, changeColor);
                                    }
                                    
                                    EditorGUILayout.PropertyField(elementProperty, elementProperty.hasVisibleChildren);
                                }
                            }

                            EditorGUI.indentLevel--;
                        }
                    }

                    break;
            }
        }
    }
}