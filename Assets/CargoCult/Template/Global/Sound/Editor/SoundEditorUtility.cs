﻿using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace CargoCult.Template.Global.Sound.Editor
{
    public static class SoundEditorUtility
    {
        private static readonly Action<AudioClip> playClip;
        private static readonly Action<AudioClip, int, bool> complexPlayClip;
        private static readonly Action<AudioClip> stopClip;
        private static readonly Action stopAllClips;
        private static readonly Func<AudioClip, bool> isClipPlaying;
        
        static SoundEditorUtility()
        {
            Assembly unityEditorAssembly = typeof(AudioImporter).Assembly;
            Type audioUtilClass = unityEditorAssembly.GetType("UnityEditor.AudioUtil");
            MethodInfo playClipMethod = audioUtilClass.GetMethod(
                "PlayClip",
                BindingFlags.Static | BindingFlags.Public,
                null,
                new System.Type[]
                {
                    typeof(AudioClip)
                },
                null
            );

            playClip = clip =>
            {
                playClipMethod.Invoke(
                    null,
                    new object[]
                    {
                        clip
                    }
                );
            };
            
            MethodInfo complexPlayClipMethod = audioUtilClass.GetMethod(
                "PlayClip",
                BindingFlags.Static | BindingFlags.Public,
                null,
                new System.Type[]
                {
                    typeof(AudioClip), typeof(int), typeof(bool)
                },
                null
            );

            complexPlayClip = (clip, startSample, loop) =>
            {
                complexPlayClipMethod.Invoke(
                    null,
                    new object[]
                    {
                        clip, startSample, loop
                    }
                );
            };
            
            MethodInfo stopClipMethod = audioUtilClass.GetMethod(
                "StopClip",
                BindingFlags.Static | BindingFlags.Public,
                null,
                new System.Type[]
                {
                    typeof(AudioClip)
                },
                null
            );

            stopClip = clip => 
            {
                stopClipMethod.Invoke(
                    null,
                    new object[]
                    {
                        clip
                    }); 
            };
            
            MethodInfo stopAllClipsMethod = audioUtilClass.GetMethod(
                "StopAllClips",
                BindingFlags.Static | BindingFlags.Public,
                null,
                new System.Type[] { },
                null
            );

            stopAllClips = () =>
            {
                stopAllClipsMethod.Invoke(
                    null,
                    new object[] { }
                );
            };
            
            MethodInfo isClipPlayingMethod = audioUtilClass.GetMethod(
                "IsClipPlaying",
                BindingFlags.Static | BindingFlags.Public,
                null,
                new System.Type[]
                {
                    typeof(AudioClip)
                },
                null
            );

            isClipPlaying = clip => 
            {
                object result = isClipPlayingMethod.Invoke(
                    null,
                    new object[]
                    {
                        clip
                    });

                return (bool) result;
            };

            PlayOnIcon = EditorGUIUtility.FindTexture("preAudioPlayOn");
            PlayOffIcon = EditorGUIUtility.FindTexture("preAudioPlayOff");
        }

        public static Texture PlayOnIcon { get; }
        public static Texture PlayOffIcon { get; }

        public static void PlayClip(AudioClip clip)
        {
            playClip(clip);
        }

        public static void PlayClip(AudioClip clip, int startSample, bool loop)
        {
            complexPlayClip(clip, startSample, loop);
        }

        public static void StopClip(AudioClip clip)
        {
            stopClip(clip);
        }

        public static void StopAllClips()
        {
            stopAllClips();
        }

        public static bool IsClipPlaying(AudioClip clip)
        {
            return isClipPlaying(clip);
        }

        public static void AudioClipField(AudioClip audioClip)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(audioClip.name);
            if (GUILayout.Button(PlayOnIcon))
            {
                PlayClip(audioClip);
            }

            EditorGUILayout.EndHorizontal();
        }
    }
}