﻿using CargoCult.Template.Injectro.Core;

namespace CargoCult.Template.Global.JSON
{
	public interface IJSONSerializer : IBean
	{
		string Serialize(object obj);
		object Deserialize(string str);
		T Deserialize<T>(string str);
	}
}
