﻿using CargoCult.Template.Injectro.Core;

namespace CargoCult.Template.Global.TimeScale
{
    public interface ITimeScaleManager : IBean
    {
        void Pause(ITimeScaleChanger changer);
        void Resume(ITimeScaleChanger changer);
        void ReduceTimeScale(ITimeScaleChanger changer);
        bool IsPause();
    }

    public interface ITimeScaleChanger
    {
        
    }
    
}
