﻿using System.Collections.Generic;
using CargoCult.Template.Injectro.Core;
using UnityEngine;

namespace CargoCult.Template.Global.TimeScale
{
    [Singleton]
    public class TimeScaleManager : MonoBehaviour, ITimeScaleManager, IPostResolved
    {
        private readonly List<ITimeScaleChanger> changers = new List<ITimeScaleChanger>();
        
        private float currentTimeScale = 1;
        private float reduceTimeScale = 0.5f;
        private bool isPause;
        
        public void PostResolved()
        {
            currentTimeScale = Time.timeScale;
            isPause = currentTimeScale == 0;
        }
        
        public void Pause(ITimeScaleChanger changer)
        {
            if (!changers.Contains(changer))
            {
                changers.Add(changer);    
            }
            ChangeTimeScale(0);
            isPause = true;
        }

        public void Resume(ITimeScaleChanger changer)
        {
            if (changers.Contains(changer))
            {
                changers.Remove(changer);
            }

            if (changers.Count == 0)
            {
                ChangeTimeScale(1);
                isPause = false;
            }
        }

        public void ReduceTimeScale(ITimeScaleChanger changer)
        {
            changers.Add(changer);
            ChangeTimeScale(reduceTimeScale);
        }

        public bool IsPause()
        {
            return isPause;
        }

        private void ChangeTimeScale(float value)
        {
            if (currentTimeScale == value)
            {
                return;
            }

            Time.timeScale = value;
            currentTimeScale = value;
        }
    }
}
