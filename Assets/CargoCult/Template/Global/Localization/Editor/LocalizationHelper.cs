﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CargoCult.Template.Common.Editor;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace CargoCult.Template.Global.Localization.Editor
{
    public class LocalizationHelper : EditorWindow
    {
        private readonly LocalizationFileManager fileManager = new LocalizationFileManager();
        private Dictionary<string, string> keyToTranslationDict = new Dictionary<string, string>();
        private Dictionary<LanguageType, string> singleKeyTranslations = new Dictionary<LanguageType, string>();
        private string singleKey;
        private LanguageType currentLanguage = LanguageType.None;
        private EditMode currentMode;
        private GUIStyle dictElementStyle;
        private GUIStyle dictHeaderStyle;
        private GUIStyle languageSelectorStyle;
        private GUIStyle headerStyle;
        private GUIStyle smallButtonStyle;
        private GUIStyle languageLabelStyle;
        private GUIStyle textAreaStyle;
        private Vector2 scrollPos;
        private bool autoTranslateOnlyEmpty;
        
        [MenuItem("Editor Tools/Localization/Localization Helper")]
        public static void ShowWindow()
        {
            LocalizationHelper window = GetWindow<LocalizationHelper>();
            window.Init();
            window.Show();
        }

        private void Init()
        {
            fileManager.InitLocalizationFiles();

            dictElementStyle = new GUIStyle(EditorStyles.textField){fixedWidth = 300, stretchWidth = false};
            dictHeaderStyle = new GUIStyle(EditorStyles.boldLabel) {fixedWidth = 300, stretchWidth = false, alignment = TextAnchor.LowerCenter};
            languageSelectorStyle = new GUIStyle(EditorStyles.popup){stretchWidth = false, alignment = TextAnchor.MiddleCenter};
            headerStyle = new GUIStyle(EditorStyles.boldLabel) {alignment = TextAnchor.MiddleCenter};
            smallButtonStyle = new GUIStyle(EditorStyles.miniButton){stretchWidth = false};
            languageLabelStyle = new GUIStyle(EditorStyles.boldLabel);
            textAreaStyle = new GUIStyle(EditorStyles.textArea) {wordWrap = true, stretchHeight = true};

            foreach (LanguageType languageType in Enum.GetValues(typeof(LanguageType)))
            {
                if (languageType == LanguageType.None)
                {
                    continue;
                }
                
                singleKeyTranslations.Add(languageType, "");
            }
        }

        private void OnGUI()
        {
            GUILayout.Box("Localization Helper", GUILayout.ExpandWidth(true), GUILayout.Height(20));
            
            DrawModeSelector();

            switch (currentMode)
            {
                case EditMode.AllKeys:
                {
                    DrawAllKeysModeEditor();
                    break;
                }
                case EditMode.SingleKey:
                {
                    DrawSingleKeyModeEditor();
                    break;
                }
            }
        }

        private void DrawAllKeysModeEditor()
        {
            DrawDictionary();

            DrawKeysSeacrhButtons();
            
            DrawLanguageSelector();

            EditorGUI.BeginDisabledGroup(currentLanguage == LanguageType.None);
            {
                if (GUILayout.Button(string.Format("Save '{0}' localization", currentLanguage.ToString())))
                {
                    fileManager.SaveDictAsLocalizationFile(currentLanguage, keyToTranslationDict);
                    EditorUtility.DisplayDialog("Success", "Success", "Ok");
                }

                if (GUILayout.Button(string.Format("Load '{0}' localization", currentLanguage.ToString())))
                {
                    keyToTranslationDict.Clear();
                    fileManager.FillLanguageDictionary(currentLanguage, keyToTranslationDict);
                }
            }
            EditorGUI.EndDisabledGroup();
            
            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
            
            if (GUILayout.Button("Alphabetical sort keys"))
            {
                keyToTranslationDict = keyToTranslationDict.Keys.OrderBy(e => e).ToDictionary(e => e, e => keyToTranslationDict[e]);
            }

            EditorGUI.BeginDisabledGroup(currentLanguage == LanguageType.None || currentLanguage == LanguageType.En);
            {
                EditorGUILayout.BeginHorizontal();
                {
                    autoTranslateOnlyEmpty = EditorGUILayout.Toggle("Only empty translations", autoTranslateOnlyEmpty, GUILayout.Width(170));
                    
                    if (GUILayout.Button("Translate All using En localization"))
                    {
                        EditorCoroutine.StartCoroutine(AutoTranslateDictionary(autoTranslateOnlyEmpty));
                    }
                }
                EditorGUILayout.EndHorizontal();
            }
            EditorGUI.EndDisabledGroup();

            if (GUILayout.Button("Clear Founded Keys List"))
            {
                keyToTranslationDict.Clear();
            }
            
            if (GUILayout.Button("Add keys in all files"))
            {
                fileManager.RefreshLocalizationFiles(keyToTranslationDict.Keys.ToArray());
                AssetDatabase.Refresh();
                EditorUtility.DisplayDialog("Success", "Success", "Ok");
            }
            
            if (GUILayout.Button("Remove Unused Keys From All Files"))
            {
                if (EditorUtility.DisplayDialog("Remove unused key", "All not found keys will be deleted!", "Ok",
                    "Cancel"))
                {
                    fileManager.RemoveUnusedKeys(keyToTranslationDict.Keys.ToArray());
                    EditorUtility.DisplayDialog("Success", "Success", "Ok");
                }
            }
        }

        private void DrawDictionary()
        {
            EditorGUILayout.BeginHorizontal();
            {
                GUILayout.Label("Key", dictHeaderStyle);
                GUILayout.Label("Translation", dictHeaderStyle);
            }
            EditorGUILayout.EndHorizontal();
            
            scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
            {
                foreach (KeyValuePair<string, string> pair in keyToTranslationDict)
                {
                    EditorGUILayout.BeginHorizontal();
                    {
                        if (GUILayout.Button(pair.Key, dictElementStyle))
                        {
                            EditDictElementPopup.Show(keyToTranslationDict, pair.Key, true);
                        }

                        if (GUILayout.Button(pair.Value, dictElementStyle))
                        {
                            EditDictElementPopup.Show(keyToTranslationDict, pair.Key, false);
                        }

                        if (GUILayout.Button("E", smallButtonStyle))
                        {
                            singleKey = pair.Key;
                            currentMode = EditMode.SingleKey;
                            LoadTranslationsForSingleKey();
                        }

                        if (currentLanguage != LanguageType.None && currentLanguage != LanguageType.En)
                        {
                            if (GUILayout.Button("T", smallButtonStyle))
                            {
                                TranslateDictElement(pair);
                            }
                        }

                        if (GUILayout.Button("-", smallButtonStyle))
                        {
                            keyToTranslationDict.Remove(pair.Key);
                            return;
                        }
                    }
                    EditorGUILayout.EndHorizontal();
                }
            }
            EditorGUILayout.EndScrollView();
        }

        private void TranslateDictElement(KeyValuePair<string, string> pair)
        {
            Dictionary<string, string> enDict = new Dictionary<string, string>();
            fileManager.FillLanguageDictionary(LanguageType.En, enDict);
            string tranlation;
            if (enDict.TryGetValue(pair.Key, out tranlation))
            {
                string cachedKey = pair.Key;
                AutoTranslator.Tranlate(tranlation, LanguageType.En, currentLanguage,
                    s => { keyToTranslationDict[cachedKey] = s; });
            }
        }

        private void DrawKeysSeacrhButtons()
        {
            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
            EditorGUILayout.LabelField("Add keys", headerStyle);
            EditorGUILayout.BeginHorizontal();
            {
                bool needUpdateTranslation = false;
                
                if (GUILayout.Button("New"))
                {
                    keyToTranslationDict.Add("NEW_" + keyToTranslationDict.Count, "");
                    scrollPos.y = float.MaxValue;
                }
                
                if (GUILayout.Button("From Localized Texts"))
                {
                    needUpdateTranslation = true;
                    FindKeysFromLocalizedTexts();
                }
                
                if (GUILayout.Button("From English File"))
                {
                    needUpdateTranslation = true;
                    FindKeysFromEnLocalizationFile();
                }

                if (currentLanguage != LanguageType.None && needUpdateTranslation)
                {
                    FindTranslationsForAllKeys(currentLanguage, true);
                }
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
        }

        private void DrawSingleKeyModeEditor()
        {
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.LabelField("Key", languageLabelStyle, GUILayout.Width(30));

                singleKey = EditorGUILayout.TextField(singleKey);

                EditorGUILayout.BeginVertical();
                {
                    scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
                    {
                        foreach (LanguageType languageType in Enum.GetValues(typeof(LanguageType)))
                        {
                            if (languageType == LanguageType.None)
                            {
                                continue;
                            }
                            
                            EditorGUILayout.BeginHorizontal();
                            {

                                EditorGUILayout.BeginVertical(GUILayout.Width(20));
                                {
                                    EditorGUILayout.LabelField(languageType.ToString(), languageLabelStyle,
                                        GUILayout.Width(20));

                                    DrawSingleKeyTranslateButton(languageType);
                                }
                                EditorGUILayout.EndVertical();

                                singleKeyTranslations[languageType] = EditorGUILayout.TextArea(singleKeyTranslations[languageType], textAreaStyle);
                            }
                            EditorGUILayout.EndHorizontal();
                        }
                    }
                    EditorGUILayout.EndScrollView();
                }
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndHorizontal();

            if (GUILayout.Button("Load translation for key"))
            {
                LoadTranslationsForSingleKey();
            }

            if (GUILayout.Button("Save translations for key"))
            {
                SaveTranslationsForSingleKey();
            }

            EditorGUI.BeginDisabledGroup(string.IsNullOrEmpty(singleKeyTranslations[LanguageType.En]));
            {
                EditorGUILayout.BeginHorizontal();
                {
                    autoTranslateOnlyEmpty = EditorGUILayout.Toggle("Only empty translations", autoTranslateOnlyEmpty, GUILayout.Width(170));

                    if (GUILayout.Button("Auto translate on languages"))
                    {
                        EditorCoroutine.StartCoroutine(AutoTranslateSingleKey(autoTranslateOnlyEmpty));
                    }
                }
                EditorGUILayout.EndHorizontal();
            }
            EditorGUI.EndDisabledGroup();

            if (GUILayout.Button("Clear form"))
            {
                ClearSingleModeForm();
            }
        }

        private void DrawSingleKeyTranslateButton(LanguageType languageType)
        {
            if (languageType == LanguageType.None || languageType == LanguageType.En)
            {
                return;
            }
            
            bool isDisabled = string.IsNullOrEmpty(singleKeyTranslations[LanguageType.En]);
            EditorGUI.BeginDisabledGroup(isDisabled);
            {
                if (GUILayout.Button("T", smallButtonStyle, GUILayout.Width(20)))
                {
                    AutoTranslator.Tranlate(
                        singleKeyTranslations[LanguageType.En],
                        LanguageType.En,
                        languageType,
                        s => { singleKeyTranslations[languageType] = s; });
                }
            }
            EditorGUI.EndDisabledGroup();
        }

        private IEnumerator AutoTranslateSingleKey(bool onlyEmpty)
        {
            AutoTranslator.Reset();
            Array languageTypes = Enum.GetValues(typeof(LanguageType));
            for (int i = 0; i < languageTypes.Length; i++)
            {
                LanguageType languageType = (LanguageType) languageTypes.GetValue(i);
                if (languageType == LanguageType.None || languageType == LanguageType.En)
                {
                    continue;
                }

                if (onlyEmpty && !string.IsNullOrEmpty(singleKeyTranslations[languageType]))
                {
                    continue;
                }

                bool isFinished = false;
                LanguageType type = languageType;
                AutoTranslator.Tranlate(singleKeyTranslations[LanguageType.En], LanguageType.En, languageType, s =>
                {
                    singleKeyTranslations[type] = s;
                    isFinished = true;
                });

                while (!isFinished)
                {
                    float progress = i / (float)(languageTypes.Length - 2);
                    if (EditorUtility.DisplayCancelableProgressBar("Translation", "Progress translation", progress))
                    {
                        AutoTranslator.CancelTranslate();
                        EditorUtility.ClearProgressBar();
                        yield break;
                    }
                    
                    yield return new WaitForSecondsRealtime(0.1f);
                }
            }

            EditorUtility.ClearProgressBar();
        }

        private IEnumerator AutoTranslateDictionary(bool onlyEmpty)
        {
            AutoTranslator.Reset();
            Dictionary<string, string> enDict = new Dictionary<string, string>();
            fileManager.FillLanguageDictionary(LanguageType.En, enDict);

            string[] keys = keyToTranslationDict.Keys.ToArray();
            for (int i = 0; i < keys.Length; i++)
            {
                string key = keys[i];
                if (onlyEmpty && !string.IsNullOrEmpty(keyToTranslationDict[key]))
                {
                    continue;
                }

                string enTranslation;
                if (enDict.TryGetValue(key, out enTranslation) && !string.IsNullOrEmpty(enTranslation))
                {
                    bool isFinished = false;
                    string key1 = key;
                    AutoTranslator.Tranlate(enTranslation, LanguageType.En, currentLanguage, s =>
                    {
                        keyToTranslationDict[key1] = s;
                        isFinished = true;
                    });

                    while (!isFinished)
                    {
                        float progress = i / (float) keys.Length;
                        if (EditorUtility.DisplayCancelableProgressBar("Translation", "Progress translation", progress))
                        {
                            AutoTranslator.CancelTranslate();
                            EditorUtility.ClearProgressBar();
                            yield break;
                        }

                        yield return new WaitForSecondsRealtime(0.1f);
                    }
                }
            }

            EditorUtility.ClearProgressBar();
        }

        private void SaveTranslationsForSingleKey()
        {
            fileManager.RefreshKeyWithTranslationInAllFiles(singleKey, singleKeyTranslations);
            if (keyToTranslationDict.ContainsKey(singleKey) && currentLanguage != LanguageType.None)
            {
                keyToTranslationDict[singleKey] = singleKeyTranslations[currentLanguage];
            }
            EditorUtility.DisplayDialog("Success", "Success", "Ok");
        }

        private void ClearSingleModeForm()
        {
            singleKey = string.Empty;
            foreach (LanguageType languageType in Enum.GetValues(typeof(LanguageType)))
            {
                if (languageType == LanguageType.None)
                {
                    continue;
                }

                singleKeyTranslations[languageType] = string.Empty;
            }
        }

        private void LoadTranslationsForSingleKey()
        {
            foreach (LanguageType languageType in Enum.GetValues(typeof(LanguageType)))
            {
                if (languageType == LanguageType.None)
                {
                    continue;
                }
                
                Dictionary<string, string> dict = new Dictionary<string, string>();
                fileManager.FillLanguageDictionary(languageType, dict);
                string translation;

                if (dict.TryGetValue(singleKey, out translation))
                {
                    singleKeyTranslations[languageType] = translation;
                }
            }
        }

        private void DrawModeSelector()
        {
            Array modes = Enum.GetValues(typeof(EditMode));
            string[] modesNames = new string[modes.Length];
            for (int i = 0; i < modes.Length; i++)
            {
                modesNames[i] = modes.GetValue(i).ToString();
            }
            
            currentMode = (EditMode) GUILayout.Toolbar((int)currentMode, modesNames);
        }

        private void DrawLanguageSelector()
        {
            LanguageType selectedLanguage = (LanguageType) EditorGUILayout.EnumPopup("Current language", currentLanguage, languageSelectorStyle);

            if (currentLanguage != selectedLanguage)
            {
                FindTranslationsForAllKeys(selectedLanguage, false);
            }
            
            currentLanguage = selectedLanguage;
        }

        private void FindKeysFromEnLocalizationFile()
        {
            Dictionary<string, string> enDict = new Dictionary<string, string>();
            fileManager.FillLanguageDictionary(LanguageType.En, enDict);

            foreach (KeyValuePair<string,string> pair in enDict)
            {
                if (!keyToTranslationDict.ContainsKey(pair.Key))
                    keyToTranslationDict.Add(pair.Key, "");
            }
        }

        private void FindKeysFromLocalizedTexts()
        {
            foreach (string path in AssetDatabase.GetAllAssetPaths())
            {
                if (path.EndsWith(".prefab") && !Directory.Exists(path))
                {
                    FindLocalizedTextInPrefab(path);
                }

                if (path.EndsWith(".unity") && !Directory.Exists(path))
                {
                    FindLocalizedTextOnScene(path);
                }
            }
        }
        
        private void FindLocalizedTextInPrefab(string prefabPath)
        {
            GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(prefabPath);
            if (prefab)
            {
                SearchLocalizedTextRecurse(prefab);
            }
        }

        private void FindLocalizedTextOnScene(string scenePath)
        {
            EditorSceneManager.OpenScene(scenePath, OpenSceneMode.Additive);

            GameObject[] objects = FindObjectsOfType<GameObject>();
            for (int i = 0; i < objects.Length; i++)
            {
                GameObject obj = objects[i];
                if (obj.transform.parent == null)
                    SearchLocalizedTextRecurse(obj);
            }

            EditorSceneManager.CloseScene(SceneManager.GetSceneByPath(scenePath), true);
        }

        private void SearchLocalizedTextRecurse(GameObject startObj)
        {
            for (int i = 0; i < startObj.transform.childCount; i++)
            {
                Transform child = startObj.transform.GetChild(i);
                LocalizedText localizedText = child.GetComponent<LocalizedText>();
                if (localizedText != null)
                    FindKeysInText(localizedText.GetComponent<Text>().text);
                
                SearchLocalizedTextRecurse(child.gameObject);
            }
        }

        private void FindKeysInText(string text)
        {
            List<string> keysList = new List<string>();
            LocalizationUtils.SpliTextOnKeys(text, keysList, true);

            for (int i = 0; i < keysList.Count; i++)
            {
                string key = keysList[i];
                
                if (!keyToTranslationDict.ContainsKey(key))
                    keyToTranslationDict.Add(key, "");
            }
        }
        
        private void FindTranslationsForAllKeys(LanguageType languageType, bool onlyForEmptyTranslation)
        {
            string[] keys;
            
            if (languageType == LanguageType.None)
            {
                keys = keyToTranslationDict.Keys.ToArray();
                for (int i = 0; i < keys.Length; i++)
                {
                    string key = keys[i];
                    keyToTranslationDict[key] = string.Empty;
                }
                
                return;
            }
            
            Dictionary<string, string> dict = new Dictionary<string, string>();
            fileManager.FillLanguageDictionary(languageType, dict);
            keys = keyToTranslationDict.Keys.ToArray();
            for (int i = 0; i < keys.Length; i++)
            {
                string key = keys[i];
                
                if (onlyForEmptyTranslation && !string.IsNullOrEmpty(keyToTranslationDict[key]))
                {
                    continue;
                }
                
                string translation;
                dict.TryGetValue(key, out translation);
                keyToTranslationDict[key] = translation;
            }
        }

        private enum EditMode
        {
            AllKeys,
            SingleKey,
        }

        private class EditDictElementPopup : EditorWindow
        {
            private const int Width = 250;
            private const int Height = 150;
            
            private Dictionary<string, string> currentDict;
            private string currentEditedKey;
            private bool currentIsEditKey;
            private string newString;
            private GUIStyle textAreaStyle;
                
            public static void Show(Dictionary<string, string> dict, string editedKey, bool isEditKey)
            {
                EditDictElementPopup window = CreateInstance<EditDictElementPopup>();
                Vector2 currentMousePosition = Event.current.mousePosition;
                currentMousePosition = GUIUtility.GUIToScreenPoint(currentMousePosition);
                window.position = new Rect(currentMousePosition.x - Width / 2.0f, currentMousePosition.y - Height / 2.0f, Width, Height);
                window.Init(dict, editedKey, isEditKey);
                window.ShowPopup();
            }

            private void Init(Dictionary<string, string> dict, string editedKey, bool isEditKey)
            {
                currentDict = dict;
                currentEditedKey = editedKey;
                currentIsEditKey = isEditKey;

                newString = isEditKey ? editedKey : dict[editedKey];
                
                textAreaStyle = new GUIStyle(EditorStyles.textArea) {wordWrap = true, fixedHeight = 120};
            }
            
            private void OnGUI()
            {
                newString = EditorGUILayout.TextArea(newString, textAreaStyle);
                
                EditorGUILayout.BeginHorizontal();
                {
                    if (GUILayout.Button("Apply"))
                    {
                        if (currentIsEditKey)
                        {
                            string value = currentDict[currentEditedKey];
                            currentDict.Remove(currentEditedKey);
                            currentDict.Add(newString, value);
                        }
                        else
                        {
                            currentDict[currentEditedKey] = newString;
                        }
                        
                        Close();
                    }

                    if (GUILayout.Button("Cancel"))
                    {
                        Close();
                    }
                }
                EditorGUILayout.EndHorizontal();

                if (focusedWindow != this)
                {
                    Close();
                }
            }
        }
    }
}

