﻿using System;
using System.Collections;
using System.Collections.Generic;
using CargoCult.Template.Common.Editor;
using FullSerializer;
using UnityEngine;
using UnityEngine.Networking;

namespace CargoCult.Template.Global.Localization.Editor
{
    public static class AutoTranslator
    {
        private static readonly Translator[] Translators = 
        {
            new GoogleTranslator(),
            new YandexTranslator(), 
        };
        
        private static EditorCoroutine translationCoroutine;
        private static Action<string> currentCallback;
        private static int activeTranslatorIndex;

        public static void Reset()
        {
            activeTranslatorIndex = 0;
        }

        public static void Tranlate(string text, LanguageType sourceLang, LanguageType targetLang, Action<string> callback)
        {
            if (translationCoroutine == null)
            {
                translationCoroutine = EditorCoroutine.StartCoroutine(TranslateAsync(text, sourceLang,  targetLang));
                currentCallback = callback;
            }
        }

        public static void CancelTranslate()
        {
            if (translationCoroutine != null)
            {
                translationCoroutine.Stop();
                translationCoroutine = null;
                currentCallback = null;
            }
        }

        private static IEnumerator TranslateAsync(string text, LanguageType sourceLang, LanguageType targetLang)
        {
            for (int i = activeTranslatorIndex; i < Translators.Length; i++)
            {
                Translator translator = Translators[i];
                UnityWebRequest req = UnityWebRequest.Get(translator.GetRequest(text, sourceLang, targetLang));
                req.SendWebRequest();

                while (!req.isDone)
                {
                    yield return new WaitForSecondsRealtime(0.1f);
                }

                if (string.IsNullOrEmpty(req.error))
                {
                    string translation = translator.GetTranslationFromResponse(req.downloadHandler.text);

                    req.Dispose();
                    translationCoroutine = null;
                    
                    if (currentCallback != null)
                    {
                        currentCallback(translation);
                    }

                    yield break;
                }

                activeTranslatorIndex++;
                
                Debug.Log(string.Format("Translator '{0}' send error: '{1}'. Try another translator.", translator.GetType(), req.error));
                
                req.Dispose();
            }
            
            Debug.LogError("All translators send error!");
            
            translationCoroutine = null;
            
            if (currentCallback != null)
            {
                currentCallback(string.Empty);
            }
        }

        private abstract class Translator
        {
            public abstract string GetRequest(string text, LanguageType sourceLang, LanguageType targetLang);
            public abstract string GetTranslationFromResponse(string response);
        }

        private class GoogleTranslator : Translator
        {
            private const string GoogleUrl = "https://translate.googleapis.com/translate_a/single?client=gtx&sl={0}&tl={1}&dt=t&q={2}";

            public override string GetRequest(string text, LanguageType sourceLang, LanguageType targetLang)
            {
                return string.Format(GoogleUrl, sourceLang.ToString(), targetLang.ToString(), text);
            }

            public override string GetTranslationFromResponse(string response)
            {
                fsData parsedData = fsJsonParser.Parse(response);
                List<fsData> lst1 = parsedData.AsList;
                List<fsData> lst2 = lst1[0].AsList;
                fsData finalData = lst2[0];

                fsSerializer serializer = new fsSerializer();
                string[] jsResult = null;
                serializer.TryDeserialize(finalData, ref jsResult);
                return jsResult[0];
            }
        }
        
        private class YandexTranslator : Translator
        {
            private const string YandexUrl = "https://translate.yandex.net/api/v1.5/tr.json/translate?key={0}&text={1}&lang={2}-{3}";
            private const string YandexApiKey = "trnsl.1.1.20190507T072112Z.2b7e37bb6bc5008d.85e63ec35ad08bf63773878a23084285c1d9dbce";
            
            public override string GetRequest(string text, LanguageType sourceLang, LanguageType targetLang)
            {
                return string.Format(YandexUrl, YandexApiKey, text, sourceLang.ToString().ToLower(), targetLang.ToString().ToLower());
            }

            public override string GetTranslationFromResponse(string response)
            {
                fsData parsedData = fsJsonParser.Parse(response);
                Dictionary<string, fsData> dict = parsedData.AsDictionary;
                fsData finalData = dict["text"];

                fsSerializer serializer = new fsSerializer();
                string[] jsResult = null;
                serializer.TryDeserialize(finalData, ref jsResult);
                return jsResult[0];
            }
        }
    }
}