﻿using CargoCult.Template.Common.EventController;
using CargoCult.Template.Injectro.Core;
using UnityEngine;

namespace CargoCult.Template.Global.Localization
{
    public interface ILocalizationManager : IBean, IEventController<LanguageChangedEvent, LanguageChangedEventParametrs>
    {
        LanguageType GetCurrentLanguageType();
        
        void ChangeLanguage(LanguageType newLanguage);
        
        string Translate(string text);

        Font GetFontForCurrentLanguage();

        Font GetFontForLanguage(LanguageType languageType);

        LanguageType[] GetAvaibleLanguages();

        string GetLanguageName(LanguageType languageType);
    }
}
