﻿using CargoCult.Template.Common.EventController;
using CargoCult.Template.Injectro.Core;
using UnityEngine;
using UnityEngine.UI;

namespace CargoCult.Template.Global.Localization
{
    [RequireComponent(typeof(Text))][PostResolveDependency(typeof(ILocalizationManager))]
    public class LocalizedText : MonoBehaviour, IPostResolved
    {
        [SerializeField] private bool inGlobalContext;
        [Inject] private ILocalizationManager localizationManager;
        private Text uiText;
        private string keyString;
        
        public void PostResolved()
        {
            uiText = GetComponent<Text>();
            keyString = uiText.text;
            TranslateText();
            localizationManager.AddListener<LanguageChangedEvent>(languageEvent =>
            {
                TranslateText();
            }, inGlobalContext ? EventContextType.Global : EventContextType.Scene);
        }

        public string GetText()
        {
            return uiText.text;
        }

        private void TranslateText()
        {
            uiText.text = localizationManager.Translate(keyString);
            uiText.font = localizationManager.GetFontForCurrentLanguage();
        }
    }
}