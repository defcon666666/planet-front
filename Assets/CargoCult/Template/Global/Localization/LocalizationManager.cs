﻿using System;
using System.Collections.Generic;
using System.Text;
using CargoCult.Template.Common.EventController;
using CargoCult.Template.Common.KeysContrainers;
using CargoCult.Template.Global.LoadScene;
using CargoCult.Template.Global.Preference;
using CargoCult.Template.Injectro.Core;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace CargoCult.Template.Global.Localization
{
    public enum LanguageType
    {
        None = -1,
        En = 0, //Английский
        Es = 200, //Испанский
        It = 300, //Итальянский
        Zh = 400, //Китайский
        Ko = 500, //Корейский
        De = 700, //Немецкий
        Ru = 1000, //Русский
        Fr = 1200, //Французский
        Ja = 1400, //Японский
    }

    [Serializable]
    public class LanguageParametrs
    {
        public LanguageType LanguageType;
        public Font LanguageFont;
    }
    
    [Singleton][PostResolveDependency(typeof(IPreferenceManager), typeof(ILoadSceneManager))]
    public class LocalizationManager : MonoBehaviour, ILocalizationManager, IPostResolved
    {
        public LanguageParametrs[] SpecificParametrs;
        public bool WriteUnusedKeysInFiles;
        
        private readonly Dictionary<LanguageType, string> languageNames = new Dictionary<LanguageType, string>()
        {
            {LanguageType.En, "English"},
            {LanguageType.Ru, "Русский"},
            {LanguageType.De, "Deutsch"},
            {LanguageType.Fr, "Français"},
            {LanguageType.Es, "Spanish"},
            {LanguageType.It, "Italian"},
            {LanguageType.Ja, "日本語"},
            {LanguageType.Ko, "우리말"},
            {LanguageType.Zh, "漢語"},
        };
        private readonly Dictionary<string, string> translations = new Dictionary<string, string>();
        private readonly Dictionary<LanguageType, LanguageParametrs> typeToParametrs = new Dictionary<LanguageType, LanguageParametrs>(); 
        private readonly List<string> keysList = new List<string>();
        private readonly List<string> unusedKeys = new List<string>();
        private readonly LocalizationFileManager fileManager = new LocalizationFileManager();
        [Inject]
        private IPreferenceManager preferenceManager;
        [Inject]
        private ILoadSceneManager loadSceneManager;
        private EventController<LanguageChangedEvent, LanguageChangedEventParametrs> eventController;
        private LanguageType currentLanguage;
        private Font standartFont;
        
        public void PostResolved()
        {
            for (int i = 0; i < SpecificParametrs.Length; i++)
            {
                LanguageParametrs languageParametrs = SpecificParametrs[i];
                typeToParametrs.Add(languageParametrs.LanguageType, languageParametrs);
            }

            fileManager.InitLocalizationFiles();
            eventController = new EventController<LanguageChangedEvent, LanguageChangedEventParametrs>(loadSceneManager);
            standartFont = Resources.GetBuiltinResource<Font>("Arial.ttf"); // Default unity font
            currentLanguage = LanguageType.None;

            //SystemLanguage systemLanguage = Application.systemLanguage;
            LanguageType savedLanguage = preferenceManager.LoadValue(OptionsKeysContainer.CurrentLanguage,
                LanguageType.En);
            
            ChangeLanguage(savedLanguage);
        }

        public LanguageType GetCurrentLanguageType()
        {
            return currentLanguage;
        }

        public void ChangeLanguage(LanguageType newLanguage)
        {
            if (newLanguage == currentLanguage)
                return;
            
            currentLanguage = newLanguage;
            translations.Clear();
            fileManager.FillLanguageDictionary(currentLanguage, translations);
            eventController.RaiseEvent<LanguageChangedEvent>(new LanguageChangedEventParametrs());
            preferenceManager.SaveValue(OptionsKeysContainer.CurrentLanguage, currentLanguage);
        }

        public string Translate(string text)
        {
            keysList.Clear();
            LocalizationUtils.SpliTextOnKeys(text, keysList, false);
            StringBuilder translatedText = new StringBuilder();

            for (int i = 0; i < keysList.Count; i++)
            {
                string key = keysList[i];
                string translatedKey = key;
                
                if (!LocalizationUtils.StringIsWhiteSpaces(key) && !translations.TryGetValue(key, out translatedKey))
                {
                    Debug.Log("Not found treanslation for: '" + key + "'.");
                    Debug.Log(
                        "Set on true 'WriteUnusedKeysInFiles' on LocalizationManager " +
                        "if you want to add this key in localization files, when playmode will stop.");
                    
                    if (!unusedKeys.Contains(key))
                        unusedKeys.Add(key);
                }

                translatedText.Append(translatedKey);
            }

            translatedText = translatedText.Replace("\r", string.Empty).Replace("\\n", "\n"); 
            return translatedText.ToString();
        }

        public Font GetFontForCurrentLanguage()
        {
            return GetFontForLanguage(currentLanguage);
        }

        public Font GetFontForLanguage(LanguageType languageType)
        {
            Font currentFont;
            LanguageParametrs parametrs;
            if (typeToParametrs.TryGetValue(languageType, out parametrs) && parametrs.LanguageFont != null)
            {
                currentFont = parametrs.LanguageFont;
            }
            else
            {
                currentFont = standartFont;
            }
            return currentFont;
        }

        public LanguageType[] GetAvaibleLanguages()
        {
            return fileManager.AvaibleLanguages;
        }

        public string GetLanguageName(LanguageType languageType)
        {
            string languageName;
            if (!languageNames.TryGetValue(languageType, out languageName))
                throw new Exception(string.Format("Not found language name for type: '{0}'.", languageType));

            return languageName;
        }
        
        public void AddListener<TA>(Action<LanguageChangedEventParametrs> action, EventContextType eventContextType = EventContextType.Scene) where TA : LanguageChangedEvent
        {
            eventController.AddListener<TA>(action, eventContextType);
        }

        public void RemoveListener<TR>(Action<LanguageChangedEventParametrs> action, EventContextType eventContextType = EventContextType.Scene) where TR : LanguageChangedEvent
        {
            eventController.RemoveListener<TR>(action, eventContextType);
        }

        private LanguageType ParseSystemLanguageToLanguageType(SystemLanguage sysLanguage)
        {
            switch (sysLanguage)
            {
                case SystemLanguage.Russian:
                    return LanguageType.Ru;
                       
                case SystemLanguage.French:
                    return LanguageType.Fr;
                           
                default:
                    return LanguageType.En;
            }
        }

        private void WriteUnusedKeys()
        {
            if (!WriteUnusedKeysInFiles || unusedKeys.Count == 0)
                return;

            fileManager.RefreshLocalizationFiles(unusedKeys.ToArray());
        }

        private void OnApplicationQuit()
        {
            #if UNITY_EDITOR
            WriteUnusedKeys();
            AssetDatabase.Refresh();
            #endif
        }
    }
}