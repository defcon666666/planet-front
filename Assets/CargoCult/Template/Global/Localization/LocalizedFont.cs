﻿using CargoCult.Template.Common.EventController;
using CargoCult.Template.Injectro.Core;
using UnityEngine;
using UnityEngine.UI;

namespace CargoCult.Template.Global.Localization
{
    [RequireComponent(typeof(Text))][PostResolveDependency(typeof(ILocalizationManager))]
    public class LocalizedFont : MonoBehaviour, IPostResolved
    {
        [SerializeField] private bool inGlobalContext;
        [Inject] private ILocalizationManager localizationManager;
        private Text uiText;
        
        public void PostResolved()
        {
            uiText = GetComponent<Text>();
            ApplyFont();
            localizationManager.AddListener<LanguageChangedEvent>(languageEvent =>
            {
                ApplyFont();
            }, inGlobalContext ? EventContextType.Global : EventContextType.Scene);
        }

        private void ApplyFont()
        {
            uiText.font = localizationManager.GetFontForCurrentLanguage();
        }
    }
}