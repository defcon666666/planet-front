﻿using System.Collections.Generic;

namespace CargoCult.Template.Global.Localization
{
    public static class LocalizationUtils
    {
        public static void SpliTextOnKeys(string text, List<string> listToFill, bool exceptWhiteSpaces)
        {
            string[] keys = text.Split('~');
            for (int i = 0; i < keys.Length; i++)
            {
                string key = keys[i];
                if (string.IsNullOrEmpty(key))
                    continue;
                
                if (exceptWhiteSpaces && StringIsWhiteSpaces(key))
                    continue;
                
                listToFill.Add(key);
            }
        }

        public static bool StringIsWhiteSpaces(string str)
        {
            if (str.Trim(' ').Length == 0)
                return true;

            return false;
        }
    }
}