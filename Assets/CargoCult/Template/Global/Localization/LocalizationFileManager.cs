﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace CargoCult.Template.Global.Localization
{
    public class LocalizationFileManager
    {
        private const string LocalizationFilePrefix = "LocalizationFile";
        private const string LocalizationFileExtension = ".csv";
        private const string LocalizationFilesSavePath = "Assets/CargoCult/Template/Global/Localization/Resources/";

        private readonly Dictionary<LanguageType, TextAsset> files = new Dictionary<LanguageType, TextAsset>();

        public LanguageType[] AvaibleLanguages
        {
            get { return files.Keys.ToArray(); }
        }

        public void InitLocalizationFiles()
        {
            foreach (LanguageType type in Enum.GetValues(typeof(LanguageType)))
            {
                if (type == LanguageType.None || files.ContainsKey(type))
                    continue;

                TextAsset file = LoadLocalizationFile(type);
                if (file != null)
                {
                    files.Add(type, file);
                }
            }
        }

        public void FillLanguageDictionary(LanguageType languageType, Dictionary<string, string> dictForFill)
        {
            TextAsset localizationFile;
            if (!files.TryGetValue(languageType, out localizationFile))
                throw new Exception("Localization file for '" + languageType + "' not find");

            Dictionary<string, string> textAsset = ParseCSVToDict(localizationFile.text);
            foreach (KeyValuePair<string, string> pair in textAsset)
            {
                dictForFill.Add(pair.Key, pair.Value);
            }
        }

        public void SaveDictAsLocalizationFile(LanguageType languageType, Dictionary<string, string> dictionary)
        {
            string csvFile = ParseDictToCSV(dictionary);
            SaveLocalizationFile(languageType, csvFile);
        }

        public void RefreshLocalizationFiles(string[] allKeys)
        {
            foreach (LanguageType type in Enum.GetValues(typeof(LanguageType)))
            {
                if (type == LanguageType.None)
                    continue;

                TextAsset localizationFile = LoadLocalizationFile(type);
                string csvFile = localizationFile == null ? string.Empty : localizationFile.text;

                Dictionary<string, string> dict = ParseCSVToDict(csvFile);

                for (int i = 0; i < allKeys.Length; i++)
                {
                    string key = allKeys[i];
                    if (!dict.ContainsKey(key))
                        dict.Add(key, string.Empty);
                }

                csvFile = ParseDictToCSV(dict);

                SaveLocalizationFile(type, csvFile);
            }
        }

        public void RemoveUnusedKeys(string[] usedKeys)
        {
            foreach (LanguageType type in Enum.GetValues(typeof(LanguageType)))
            {
                if (type == LanguageType.None)
                    continue;

                TextAsset localizationFile = LoadLocalizationFile(type);
                string csvFile = localizationFile == null ? string.Empty : localizationFile.text;

                Dictionary<string, string> dict = ParseCSVToDict(csvFile);
                List<string> unusedKeys = dict.Keys.ToList();

                for (int i = 0; i < usedKeys.Length; i++)
                {
                    string key = usedKeys[i];

                    if (unusedKeys.Contains(key))
                        unusedKeys.Remove(key);
                }

                for (int i = 0; i < unusedKeys.Count; i++)
                {
                    string key = unusedKeys[i];
                    dict.Remove(key);
                }

                csvFile = ParseDictToCSV(dict);

                SaveLocalizationFile(type, csvFile);
            }
        }

        public void RefreshKeyWithTranslationInAllFiles(string key, Dictionary<LanguageType, string> translations)
        {
            foreach (LanguageType type in Enum.GetValues(typeof(LanguageType)))
            {
                if (type == LanguageType.None)
                    continue;

                TextAsset localizationFile = LoadLocalizationFile(type);
                string csvFile = localizationFile == null ? string.Empty : localizationFile.text;

                Dictionary<string, string> dict = ParseCSVToDict(csvFile);
                
                string currTranslation = translations[type];
                
                if (dict.ContainsKey(key))
                {
                    dict[key] = currTranslation;
                }
                else
                {
                    dict.Add(key, currTranslation);
                }

                csvFile = ParseDictToCSV(dict);

                SaveLocalizationFile(type, csvFile);
            }

        }

        private Dictionary<string, string> ParseCSVToDict(string csvFile)
        {
            Dictionary<string, string> newLocalizationDict = new Dictionary<string, string>();
            string[] rows = csvFile.Split('\n');
            for (int i = 0; i < rows.Length; i++)
            {
                string row = rows[i];
                if (string.IsNullOrEmpty(row) || LocalizationUtils.StringIsWhiteSpaces(row))
                    continue;

                string[] columns = row.Split(';');

                string key = columns[0];
                string value = columns.Length > 1 ? columns[1] : string.Empty;
                if (newLocalizationDict.ContainsKey(key))
                    throw new Exception(string.Format("Found duplicate key: '{0}.'", key));
                newLocalizationDict.Add(key, value);
            }
            return newLocalizationDict;
        }

        private string ParseDictToCSV(Dictionary<string, string> dict)
        {
            StringBuilder csvStringBuilder = new StringBuilder();
            foreach (KeyValuePair<string, string> pair in dict)
            {
                csvStringBuilder.Append(pair.Key)
                    .Append(";")
                    .Append(pair.Value)
                    .Append("\n");
            }
            return csvStringBuilder.ToString();
        }

        private TextAsset LoadLocalizationFile(LanguageType languageType)
        {
            return Resources.Load<TextAsset>(languageType + LocalizationFilePrefix);
        }

        private void SaveLocalizationFile(LanguageType languageType, string localizationString)
        {
            string filePath = LocalizationFilesSavePath + languageType + LocalizationFilePrefix +
                              LocalizationFileExtension;
            StreamWriter writer = new StreamWriter(filePath);
            writer.Write(localizationString);
            writer.Flush();
            writer.Close();
        }
    }
}