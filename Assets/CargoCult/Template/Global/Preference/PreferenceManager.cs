﻿using System;
using System.Collections.Generic;
using System.Threading;
using CargoCult.Template.Common.SlowUpdate;
using CargoCult.Template.Global.JSON;
using CargoCult.Template.Global.MainThread;
using CargoCult.Template.Injectro.Core;
using UnityEngine;

namespace CargoCult.Template.Global.Preference
{
    [Singleton]
    [PostResolveDependency(
        typeof(IMainThreadExecutor),
        typeof(ISerializationKeyBuilder),
        typeof(IJSONSerializer)
    )]
    public class PreferenceManager : MonoBehaviour, IPreferenceManager, IPostResolved
    {
        private const float SerializationCountdown = 30f;
        private const float SerializationCallBias = 1f;

        private Dictionary<string, object> cache = new Dictionary<string, object>();
        private Dictionary<string, object> changes = new Dictionary<string, object>();
        private List<string> cachedKeys = new List<string>(200);
        private WorkingThreadExecuter workingThreadExecuter;
        private SlowUpdateProc serializationKeysUpdateProc;
        private SlowUpdateProc serializationPrefsUpdateProc;

        [Inject] private IJSONSerializer serializer;
        [Inject] private IMainThreadExecutor mainThreadExecutor;
        [Inject] private ISerializationKeyBuilder serializationKeyBuilder;

        private bool forceSave;
        
        public void PostResolved()
        {
            forceSave = false;
            workingThreadExecuter = new WorkingThreadExecuter(mainThreadExecutor);
            PullKeys();
            PullPrefs();
            serializationKeysUpdateProc = new SlowUpdateProc(PushDataParallelKeys, SerializationCountdown);
            serializationPrefsUpdateProc = new SlowUpdateProc(PushDataParallelPrefs, SerializationCountdown + SerializationCallBias);
        }

        public void SaveValue<T>(string key, T value)
        {
            if (cache.ContainsKey(key))
            {
                cache[key] = value;
            }
            else
            {
                cache.Add(key, value);
            }

            if (!changes.ContainsKey(key))
            {
                changes.Add(key, value);
            }
            else
            {
                changes[key] = value;
            }
        }

        public T LoadValue<T>(string key, T defaultValue)
        {
            object value = null;

            if (cache.ContainsKey(key))
            {
                value = cache[key];

                if ((defaultValue != null) && value?.GetType() != defaultValue.GetType())
                {
                    Debug.LogError("Type of loaded value is not the same as default at key : " + key);
                    return defaultValue;
                }
                
                if (value is IConvertible)
                {
                    IConvertible convertible = value as IConvertible;
                    return ConvertTo<T>(convertible);
                }
            }

            if (value == null)
            {
                SaveValue(key, defaultValue);
                return defaultValue;
            }

            return (T) value;
        }

        public void EmergencySave()
        {
            SerializeKeysInstantly();
            SerializePrefsInstantly();
            forceSave = true;
        }

        private void Update()
        {
            if (forceSave)
            {
                forceSave = false;
                InnerSave();
            }
            else
            {
                serializationKeysUpdateProc.ProceedOnFixedUpdate();
                serializationPrefsUpdateProc.ProceedOnFixedUpdate();
            }
        }

        private void InnerSave()
        {
            PlayerPrefs.Save();
        }
        
        private void PushDataParallelKeys()
        {
            cachedKeys = DuplicateKeys(cache.Keys);
            
            workingThreadExecuter.Run(() => SerializeKeysParallel(cachedKeys));
        }
        
        private void PushDataParallelPrefs()
        {
            Dictionary<string, object> cashedChanges = DuplicateChanges(changes);

            workingThreadExecuter.Run(() => SerializePrefsParallel(cashedChanges));
        }

        private void SerializeKeysParallel(List<string> keysList)
        {
            string serializedKeysList = serializer.Serialize(keysList);
            string pushKey = serializationKeyBuilder.BuildKey(serializationKeyBuilder.GetSerializationPostfix());

            mainThreadExecutor.RunGlobal(() =>
            {
                PlayerPrefs.SetString(pushKey, serializedKeysList);
                PlayerPrefs.Save();
            });
        }

        private void SerializeKeysInstantly()
        {
            List<string> keysList = DuplicateKeys(cache.Keys);

            string serializedKeysList = serializer.Serialize(keysList);
            string pushKey = serializationKeyBuilder.BuildKey(serializationKeyBuilder.GetSerializationPostfix());

            PlayerPrefs.SetString(pushKey, serializedKeysList);
            PlayerPrefs.Save();
        }

        private void SerializePrefsParallel(object incObject)
        {
            Dictionary<string, object> incomingValues = (Dictionary<string, object>) incObject;
            Dictionary<string, string> serializedValues = SerializePrefs(incomingValues);
            
            mainThreadExecutor.RunGlobal(() =>
            {
                foreach (KeyValuePair<string, string> pair in serializedValues)
                {
                    PlayerPrefs.SetString(pair.Key, pair.Value);
                }

                PlayerPrefs.Save();

                Dictionary<string, object> duplicatedChanges = DuplicateChanges(changes);
                foreach (KeyValuePair<string, object> pair in incomingValues)
                {
                    if (duplicatedChanges.ContainsKey(pair.Key) && (null == pair.Value &&
                                                                    null == duplicatedChanges[pair.Key] ||
                                                                    duplicatedChanges[pair.Key].Equals(pair.Value)
                        ))
                    {
                        changes.Remove(pair.Key);
                    }
                }
            });
        }

        private void SerializePrefsInstantly()
        {
            Dictionary<string, string> serializedValues = SerializePrefs(changes);

            foreach (KeyValuePair<string, string> pair in serializedValues)
            {
                PlayerPrefs.SetString(pair.Key, pair.Value);
            }

            PlayerPrefs.Save();

            changes.Clear();
        }

        private Dictionary<string, string> SerializePrefs(Dictionary<string, object> changes)
        {
            Dictionary<string, string> serializedValues = new Dictionary<string, string>();

            foreach (KeyValuePair<string, object> pair in changes)
            {
                string postKey = serializationKeyBuilder.BuildKey(pair.Key);
                serializedValues.Add(postKey, serializer.Serialize(pair.Value));
            }

            return serializedValues;
        }

        private Dictionary<string, object> DuplicateChanges(Dictionary<string, object> original)
        {
            Dictionary<string, object> duplicate = new Dictionary<string, object>();

            if (original == null || original.Count == 0)
            {
                return duplicate;
            }

            foreach (KeyValuePair<string, object> pair in original)
            {
                duplicate.Add(pair.Key, pair.Value);
            }

            return duplicate;
        }

        private List<string> DuplicateKeys(Dictionary<string, object>.KeyCollection incKeys)
        {
            List<string> duplicate = new List<string>();

            foreach (string key in incKeys)
            {
                duplicate.Add(key);
            }

            return duplicate;
        }

        private void PullKeys()
        {
            string pullKey = serializationKeyBuilder.BuildKey(serializationKeyBuilder.GetSerializationPostfix());

            string serializedVariable = PlayerPrefs.GetString(pullKey);

            List<string> deserializedVariable = null;
            if (!serializedVariable.Equals(String.Empty))
            {
                deserializedVariable = serializer.Deserialize<List<string>>(serializedVariable);
            }

            if (deserializedVariable == null)
            {
                return;
            }

            for (int i = 0; i < deserializedVariable.Count; i++)
            {
                string key = deserializedVariable[i];
                if (!cache.ContainsKey(key))
                    cache.Add(key, null);
            }
        }

        private void PullPrefs()
        {
            //Необходимо, чтобы не возникало рассинхронизации (InvalidOperationException: out of sync) <<KrOost
            List<string> keys = new List<string>();

            foreach (string key in cache.Keys)
            {
                keys.Add(key);
            }

            foreach (string key in keys)
            {
                object value;
                value = PullObject(key);
                cache[key] = value;
            }
        }

        private object PullObject(string key)
        {
            string pullKey = serializationKeyBuilder.BuildKey(key);
            string serializedVariable = PlayerPrefs.GetString(pullKey);
            object deserializedVariable = null;

            if (serializedVariable.Equals(String.Empty))
            {
                return null;
            }

            try
            {
                deserializedVariable = serializer.Deserialize(serializedVariable);
            }
            catch (Exception e)
            {
                Debug.LogErrorFormat("PreferenceManager: Can't load value with key {0}", pullKey);
                Debug.LogErrorFormat("PreferenceManager throws: {0}", e);
            }

            return deserializedVariable;
        }

        private T ConvertTo<T>(IConvertible obj)
        {
            Type t = typeof(T);
            Type u = Nullable.GetUnderlyingType(t);

            if (u != null)
            {
                if (obj == null)
                {
                    return default(T);
                }

                return (T) Convert.ChangeType(obj, u);
            }

            return (T) Convert.ChangeType(obj, t);
        }
        
        private void OnApplicationQuit()
        {
            ForceSaving();
        }
        
        private void OnApplicationPause(bool pause)
        {
            ForceSaving();
        }

        private void ForceSaving()
        {
            EmergencySave();
            forceSave = false;
            InnerSave();
        }
        
        private class WorkingThreadExecuter
        {
            private readonly Thread workingThread;
            private readonly Queue<Action> actions = new Queue<Action>();
            private readonly object syncRoot = new object();
            private IMainThreadExecutor mainThreadExecutor;

            public WorkingThreadExecuter(IMainThreadExecutor mainThreadExecutor)
            {
                this.mainThreadExecutor = mainThreadExecutor;
                workingThread = new Thread(() =>
                {
                    Action cachedAction;

                    while (true)
                    {
                        lock (syncRoot)
                        {
                            if (actions.Count <= 0)
                            {
                                Monitor.Wait(syncRoot);
                            }

                            cachedAction = actions.Dequeue();
                        }

                        try
                        {
                            cachedAction();
                        }
                        catch (Exception e)
                        {
                            this.mainThreadExecutor.Run(() => {Debug.LogError(e);});
                        }
                    }
                });
                workingThread.Start();
            }

            public void Run(Action action)
            {
                if (null == action)
                {
                    Debug.LogError("ExternalThreadExecutor: cannot run null action");
                    return;
                }

                lock (syncRoot)
                {
                    actions.Enqueue(action);
                    Monitor.Pulse(syncRoot);
                }
            }
        }
    }
}