﻿using CargoCult.Template.Injectro.Core;

namespace CargoCult.Template.Global.Preference
{
    public interface IPreferenceManager : IBean
    {
        void SaveValue<T>(string key, T value);
        T LoadValue<T>(string key, T defaultValue);
        void EmergencySave();
    }
}
