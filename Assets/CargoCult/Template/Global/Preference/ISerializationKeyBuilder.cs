﻿using CargoCult.Template.Injectro.Core;

namespace CargoCult.Template.Global.Preference
{
    public interface ISerializationKeyBuilder : IBean
    {
        string GetSerializationPostfix();
        string BuildKey(string incKey);
    }
}
