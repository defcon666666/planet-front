﻿using System;
using CargoCult.Template.Injectro.Core;
using UnityEngine;

namespace CargoCult.Template.Global.Preference
{
    [Singleton]
    public class SerializationKeyBuilder : MonoBehaviour, ISerializationKeyBuilder
    {
        public string Postfix = String.Empty;
        public string Prefix = String.Empty;

        public string GetSerializationPostfix()
        {
            return Postfix;
        }

        public string BuildKey(string incKey)
        {
            return string.Concat(Prefix, incKey);
        }
    }
}
