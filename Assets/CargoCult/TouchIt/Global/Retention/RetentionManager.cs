﻿using System;
using CargoCult.Template.Global.Preference;
using CargoCult.Template.Injectro.Core;
using UnityEngine;

namespace CargoCult.TouchIt.Global.Retention
{
    [Singleton][PostResolveDependency(typeof(IPreferenceManager))]
    public class RetentionManager : MonoBehaviour, IRetentionManager, IPostResolved
    {
        private const string FistDay = "FirstDay";
        private const string LastCheckedDate = "LastCheckedDate";
        private const string DayInRow = "DayInRow";
        
        [Inject] private IPreferenceManager preferenceManager;

        public void PostResolved()
        {
            Invoke("CheckRetention", 2);
        }

        private void CheckRetention()
        {
            DateTime firstDate = preferenceManager.LoadValue(FistDay, DateTime.MinValue);

            if (firstDate == DateTime.MinValue)
            {
                firstDate = DateTime.Now;
                AppMetrica.Instance.ReportEvent("DayRet_1");
                preferenceManager.SaveValue(FistDay, firstDate);
                preferenceManager.SaveValue(LastCheckedDate, firstDate);
                preferenceManager.SaveValue(DayInRow, 1);
            }
            else
            {
                DateTime lastCheckedDate = preferenceManager.LoadValue(LastCheckedDate, DateTime.MinValue);
                if (lastCheckedDate.Date != DateTime.Now.Date)
                {
                    int dayInRow = preferenceManager.LoadValue(DayInRow, 1);
                    dayInRow++;
                    preferenceManager.SaveValue(DayInRow, dayInRow);
                    AppMetrica.Instance.ReportEvent($"DayRet_{dayInRow}");
                    preferenceManager.SaveValue(LastCheckedDate, DateTime.Now);
                }
            }
        }
    }
}
