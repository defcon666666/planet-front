﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetEnvironment : MonoBehaviour
{
    public MeshFilter MeshLand;
    public MeshFilter MeshOcean;
    public List<GameObject> House;
    public List<GameObject> Tree;
    public List<ParticleSystem> DamageParticle;
    public List<ParticleSystem> HappyParticle;
    [Space(10)] 
    public int HouseCount=20;
    public int TreeCount=20;
    public int DamageParticleCount=3;
    public int HappyParticleCount=3;
    [Space(10)] 
    public DamageDifficulty CurrentDamage;
    
    
    private List<ParticleSystem> actualDamageParticle = new List<ParticleSystem>();
    private List<ParticleSystem> actualHappyParticle = new List<ParticleSystem>();
    public enum DamageDifficulty
    {
        Easy,
        Medium,
        Hard,
        Destroy,
        Happy,
        None
    }
    
    
    
    void Start()
    {
        CurrentDamage = DamageDifficulty.None;
        
        Matrix4x4 localToWorld = transform.localToWorldMatrix;
        
        for (int i = 0; i < HouseCount; i++)
        {
            Vector3 spawnPoint = localToWorld.MultiplyPoint3x4(MeshLand.sharedMesh.vertices[Random.Range(0, MeshLand.sharedMesh.vertices.Length)]);
            Quaternion quaternion = Quaternion.LookRotation(spawnPoint-transform.position);
            GameObject go = Instantiate(House[Random.Range(0,House.Count)],spawnPoint,quaternion);
            go.transform.parent = MeshLand.transform;
        }
        
        for (int i = 0; i < TreeCount; i++)
        {
            Vector3 spawnPoint = localToWorld.MultiplyPoint3x4(MeshLand.sharedMesh.vertices[Random.Range(0, MeshLand.sharedMesh.vertices.Length)]);
            Quaternion quaternion = Quaternion.LookRotation(spawnPoint-transform.position);
            GameObject go = Instantiate(Tree[Random.Range(0,Tree.Count)],spawnPoint,quaternion);
            go.transform.parent = MeshLand.transform;
        }

        for (int i = 0; i < DamageParticleCount; i++)
        {
            Vector3 spawnPoint = localToWorld.MultiplyPoint3x4(MeshLand.sharedMesh.vertices[Random.Range(0, MeshLand.sharedMesh.vertices.Length)]);
            Quaternion quaternion = Quaternion.LookRotation(spawnPoint-transform.position);
            ParticleSystem part = Instantiate(DamageParticle[Random.Range(0,DamageParticle.Count)],spawnPoint,quaternion);
            part.Stop(true);
            part.transform.parent = MeshLand.transform;
            actualDamageParticle.Add(part);
        }
        
        for (int i = 0; i < HappyParticleCount; i++)
        {
            Vector3 spawnPoint = localToWorld.MultiplyPoint3x4(MeshOcean.sharedMesh.vertices[Random.Range(0, MeshOcean.sharedMesh.vertices.Length)]);
            Quaternion quaternion = Quaternion.LookRotation(spawnPoint-transform.position);
            ParticleSystem part = Instantiate(HappyParticle[Random.Range(0,HappyParticle.Count)],spawnPoint,quaternion);
            part.Stop(true);
            part.transform.parent = MeshOcean.transform;
            actualHappyParticle.Add(part);
        }
    }

    public void SwitchDamageStay(float normalValue)
    {
        
        PlanetEnvironment.DamageDifficulty damage=PlanetEnvironment.DamageDifficulty.None;

        if (normalValue > -1 && normalValue <= 0.3f)
        {
            damage = PlanetEnvironment.DamageDifficulty.Hard;
        }
        if (normalValue > 0.3f && normalValue <= 0.6f)
        {
            damage = PlanetEnvironment.DamageDifficulty.Medium;
        }
        
        if (normalValue > 0.6f && normalValue <= 0.9f)
        {
            damage = PlanetEnvironment.DamageDifficulty.Easy;
        }
        
        
        int playPartCount=0;
        
        if (damage == DamageDifficulty.Easy)
        {
            playPartCount = DamageParticleCount / 3;
        }
        if (damage == DamageDifficulty.Medium)
        {
            playPartCount = DamageParticleCount / 2;
        }
        if (damage == DamageDifficulty.Hard)
        {
            playPartCount = DamageParticleCount;
        }
        
        if (damage == DamageDifficulty.None)
        {
            foreach (var part in actualDamageParticle)
            {
                part.Stop();
            }
        }
        
        

        for (int i = 0; i < playPartCount; i++)
        {
            actualDamageParticle[Random.Range(0,actualDamageParticle.Count)].Play();
        }

        CurrentDamage = damage;
    }

    public void Happy()
    {
        SwitchDamageStay(1);
        StartCoroutine(DelayFireWorksOn());
    }

    public void UnHappy()
    {
        EnableFireWorks();
    }

    private void EnableFireWorks()
    {
        foreach (var fireWorks in actualHappyParticle )
        {
            fireWorks.Stop(true);
        }
    }

    IEnumerator DelayFireWorksOn()
    {
        for (int i = 0; i < HappyParticleCount; i++)
        {
            actualHappyParticle[i].Play(true);
            yield return new WaitForSeconds(0.8f);
            
        }
    }
}
