﻿using System.Collections;
using System.Collections.Generic;
using CargoCult.Template.Injectro.Core;
using CargoCult.Template.Local.Game;
using CargoCult.Template.Local.GameResult;
using UnityEngine;

public class TurnManager : MonoBehaviour
{
    public static TurnManager instance;
    public Player Player;
    public EnemyShip Enemy;
    [Space(10)] 
    public TurnOwner CurrentTurn;

    public List<EnemyShip> activEnemy;
    
    private GameView gameView;
    private GameResultView gameResultView;

    private float delayShowResultPanel = 3;
    
    public enum TurnOwner
    {
        Player,
        Enemy
    }
    
    
    void Awake()
    {
        instance = this;
    }

    public void PrepareGame()
    {
        if (activEnemy != null)
        {
            foreach (var lastEnemy in activEnemy)
            {
                Destroy(lastEnemy.gameObject);
            }
            activEnemy.Clear();
        }

        activEnemy =  LevelManager.instance.GetActualLevel().GetEnemyShipList();
        gameView = SceneContext.Instance.GetBean<GameView>();
        gameResultView = SceneContext.Instance.GetBean<GameResultView>();
        
        Player.Planet.UnHappy();
    }

    public void StartGame()
    {

        foreach (var currentEnemyShip in activEnemy)
        {
            currentEnemyShip.Init(Player);
        }
        
        Player.Init(activEnemy[0]);
        Player.isCanAttack = true;
        CurrentTurn = TurnOwner.Player;
        TurnOfPlayer();     
    }

    public void TurnEnd(Entity entity)
    {
        if (entity as Player)
        {
            CurrentTurn = TurnOwner.Enemy;
        }
        if (entity as EnemyShip)
        {
            CurrentTurn = TurnOwner.Player;
        }


        if (CurrentTurn == TurnOwner.Player)
        {            
            CameraManager.instance.SetView(CameraManager.CameraView.Shoot);
            
            TurnOfPlayer();
            
            return;
        }

        if (CurrentTurn == TurnOwner.Enemy)
        {            
            foreach (var currentEnemyShip in activEnemy)
            {
                currentEnemyShip.isCanAttack=true;
            }
            
            CameraManager.instance.SetView(CameraManager.CameraView.Damage);
            return;
        }
    }

    public void EnemyOnDead(EnemyShip destroyShip)
    {
        CameraManager.instance.SetView(CameraManager.CameraView.Damage);
        

        if (activEnemy.Count != 0)
        {
            activEnemy.Remove(destroyShip);            
        }
        if(activEnemy.Count==0)
        {
            PlayerWin();
        }
    }

    public void PlayerOnDead()
    {
        CameraManager.instance.SetView(CameraManager.CameraView.Damage);
        Enemy.Win();

    }

    public void PlayerSelectDamage(int selectedDamage)
    {
        Player.Damage = selectedDamage;
        Player.isAttack = true;
    }
    
    
    
    private void PlayerWin()
    {
        Player.Win();
        
        StartCoroutine(ShowResult(true));
    }

    public void PlayerLoose()
    {
        Player.isCanAttack = false;
        gameResultView.OpenForLoose();
    }

    private void TurnOfPlayer()
    {
        Player.isCanAttack = true;
        Player.NextTarget(true);
        gameView.OpenControlPanel(Player,activEnemy.Count);       
    }

    private IEnumerator ShowResult(bool isWin)
    {
        yield return new WaitForSeconds(delayShowResultPanel);
        if (isWin)
        {
            gameResultView.OpenForWin();
        }
        else
        {
            gameResultView.OpenForLoose();
        }
    }
}
