﻿using System;
using System.Collections;
using System.Collections.Generic;
using CargoCult.Template.Common.Separator;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public static CameraManager instance;

    public Camera MainCamera;
    [Separator]
    public Material Space;
    public Material Planet;
    [Separator]
    public Transform MenuPoint;
    public Transform ShootPoint;
    public Transform ShellStartPoint;
    public Transform ShellEndPoint;
    public Transform DamageViewPoint;

    public CameraView currentView;

    private Transform targetPoint;
    private Transform lastPoint;
    private Material targetSkyBox;
    private bool isMove = false;
    private float speed = 8f;
    
    public enum CameraView
    {
        Menu,
        Shoot,
        ShellStart,
        ShellEnd,
        Damage,
        None
    }

    private void Awake()
    {
        instance = this;
        targetPoint = MainCamera.transform;
        lastPoint = MainCamera.transform;

        
    }

    public void SetShootEndPoint(Transform point)
    {
        ShellEndPoint.position  = new Vector3(point.position.x,ShellEndPoint.position.y,ShellEndPoint.position.z);
    }

    private void Update()
    {
        if(!isMove)
            return;
        
        MainCamera.transform.position = Vector3.Lerp(lastPoint.position,targetPoint.position,Time.deltaTime*speed);
        MainCamera.transform.rotation = Quaternion.Lerp(lastPoint.rotation,targetPoint.rotation,Time.deltaTime*speed);
        
        RenderSettings.skybox.Lerp(RenderSettings.skybox,targetSkyBox,Time.deltaTime*(speed/2));
        
//        if ((lastPoint.position-targetPoint.position).magnitude<0.1f)
//        {
//            MainCamera.transform.position = targetPoint.position;
//            MainCamera.transform.rotation = targetPoint.rotation;
////            isMove = false;
//        }
    }

    public void SetView(CameraView view)
    {
        lastPoint.position = targetPoint.position;
        lastPoint.rotation = targetPoint.rotation;
        switch (view)
        {
            case CameraView.Menu :
                currentView = CameraView.Menu;
                targetPoint = MenuPoint;
                targetSkyBox = Space;
            break;
            
            case CameraView.Shoot :
                currentView = CameraView.Shoot;
                targetPoint = ShootPoint;
                targetSkyBox = Planet;
                break;
            
            case CameraView.ShellEnd :
                currentView = CameraView.ShellEnd;
                targetPoint = ShellEndPoint;
                targetSkyBox = Space;
                break;
            
            case CameraView.ShellStart :
                currentView = CameraView.ShellStart;
                targetPoint = ShellStartPoint;
                targetSkyBox = Planet;
                break;
            
            case CameraView.Damage :
                currentView = CameraView.Damage;
                targetPoint = DamageViewPoint;
                targetSkyBox = Space;
                break;
            
            default:
                currentView = CameraView.None;
                targetPoint = MainCamera.transform;
                break;
        }

        isMove = true;
    }

    private void OnDrawGizmos()
    {
        if(!targetPoint || !lastPoint)
            return;
        
        Gizmos.color = new Color(0,1,0,0.5f);
        Gizmos.DrawSphere(targetPoint.position,1.3f);
        
        Gizmos.color = new Color(1,0,0,0.5f);
        Gizmos.DrawSphere(lastPoint.position,1);
        
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(MenuPoint.position,ShootPoint.position);
        Gizmos.DrawLine(ShootPoint.position,ShellStartPoint.position);
        Gizmos.DrawLine(ShellStartPoint.position,ShellEndPoint.position);
        Gizmos.DrawLine(ShellEndPoint.position,DamageViewPoint.position);
    }
}
