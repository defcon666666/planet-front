﻿using System.Collections;
using System.Collections.Generic;
using CargoCult.Template.Injectro.Core;
using CargoCult.Template.Local.Game;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    private GameView gameView;
    
    
    void Awake()
    {
        instance = this;
    }


    public void StartGameMenu()
    {
        CameraManager.instance.SetView(CameraManager.CameraView.Menu);
        TurnManager.instance.PrepareGame();
    }

    public void StartGameLevel()
    {
        CameraManager.instance.SetView(CameraManager.CameraView.Shoot);
        TurnManager.instance.StartGame();
    }

}
