﻿using System.Collections;
using System.Collections.Generic;
using CargoCult.Template.Injectro.Core;
using CargoCult.Template.Local.Game;
using UnityEngine;

public class UpGradeManager : MonoBehaviour,IBean
{
    public static UpGradeManager instance;
    public Coin CoinPrefab;
    [Space(10)]    
    public Sprite DamagaSprite;
    public Sprite HealthSprite;
    public Sprite SupportGun;
    public Sprite CoinSprite;
    public Sprite AdsSprite;

    [Space(10)]
    public Player Player;
    
    private Dictionary<UpgradeType,PlayerUpgrade> upgrades  = new Dictionary<UpgradeType, PlayerUpgrade>();

    [Inject]private GameView gameView;
    private int coinCountSpawn=5;
    
    
    
    private enum UpgradeType
    {
        Damage,
        Health
    }
    
    private void Awake()
    {
        instance = this;
    }

    public void Start()
    {
        InitUpgradeList();
    }

    public List<PlayerUpgrade> GetActualUpgrades()
    {
        List<PlayerUpgrade> actualUpgrades = new List<PlayerUpgrade>();

        foreach (var upgrade in upgrades)
        {
            if (upgrade.Value.CheckForUse())
            {
                actualUpgrades.Add(upgrade.Value);
            }
        }

        return actualUpgrades;
    }

    private void InitUpgradeList()
    {
        DamageUpgrade damageUpgrade = new DamageUpgrade();
        damageUpgrade.Init();
        
        HealthUpgrade healthUpgrade  = new HealthUpgrade();
        healthUpgrade.Init();
        
        upgrades.Add(UpgradeType.Damage,damageUpgrade);
        upgrades.Add(UpgradeType.Health,healthUpgrade);
    }

    public void DamageUpgradeUp(int value)
    {
        Player.DamageUp(value);
        ShowDamage();
    }

    public void HealthUpgradeUp(int value)
    {
        Player.HealthUp(value);
    }

    private void ShowDamage()
    {
        gameView.ShowShootPower(Player);
    }

    public void SpawnCoins(Vector3 pos)
    {        
        for (int i = 0; i < coinCountSpawn; i++)
        {
            Vector3 spawPos = Random.onUnitSphere + pos;
            Coin coin = Instantiate(CoinPrefab, spawPos, Quaternion.identity);
            coin.Init();
        }
        StartCoroutine(DelayTransaction());
    }

    private IEnumerator DelayTransaction()
    {
        yield return new WaitForSeconds(1);
        LevelManager.instance.AddMoneyInLevelForKill();
    }
}
