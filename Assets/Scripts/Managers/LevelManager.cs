﻿using System.Collections;
using System.Collections.Generic;
using CargoCult.Template.Global.Ads.AdsManager;
using CargoCult.Template.Global.Sound;
using CargoCult.Template.Injectro.Core;
using UnityEngine;


[PostResolveDependency()]
public class LevelManager : MonoBehaviour,IBean,IPostResolved
{
    public static LevelManager instance;

    public Transform[] BattlePosition;
    public List<EnemyShip> AllEnemyShips;

    [Inject]private GameInfoView gameInfo;
    [Inject] public AdsManager ads;
    [Inject] public SoundManager Sound;
    
    private int money;
    private int moneyInLevel;
    private int LevelIndex;
    
    void Awake()
    {
        
    }
    
    public void PostResolved()
    {
        instance = this;
        LoadLevelIndex();
        LoadMoney();
    }
    
    public void AddMoneyInLevelForKill()
    {
        moneyInLevel += LevelIndex*10 + Random.Range(0,LevelIndex*10);
        gameInfo.ShowMoneyInLevel(moneyInLevel);
    }

    public void AddCoinForLoose()
    {
        moneyInLevel += LevelIndex*10;
        gameInfo.ShowMoneyInLevel(moneyInLevel);
    }

    public void ConvertCoinToMoney(int multiplier = 1)
    {
        money += moneyInLevel * multiplier;
        gameInfo.ShowMoney(money);
        SaveMoney();
        ResetMoneyInLevel();
    }

    public void AddMoney(int value)
    {
        LoadMoney();

        money += value;
        if (money <= 0)
        {
            money = 0;
        }
        
        SaveMoney();
        gameInfo.ShowMoney(money);
    }

    public bool EnoughMoney(int price)
    {
        LoadMoney();
        if (money >= price)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private void ResetMoneyInLevel()
    {
        moneyInLevel = 0;
        gameInfo.DisableMoneyInLevel();
    }


    public Level GetActualLevel()
    {
        Level actualLevel = new Level();
        actualLevel.InitLevel(LevelIndex);
        return actualLevel;
    }

    public int GetActualLevelIndex()
    {
        LoadLevelIndex();
        return LevelIndex;
    }

    public void NextLevel()
    {
        LevelIndex ++;
        SaveLevelIndex();
    }

    public class Level
    {
        private List<ShipStats> enemyStats;
        private List<EnemyShip> enemyShips;
        private int number;
        private int enemyCount;

        public List<EnemyShip> GetEnemyShipList()
        {
            return enemyShips;
        }

        public int GetEnemyCount()
        {
            return enemyCount;
        }

        public void InitLevel(int levelIndex)
        {
            number = levelIndex;
            
            if(number<13)
                enemyCount = levelIndex % 3;
            else
            {
                enemyCount = Random.Range(1, 4);
            }

            enemyCount = enemyCount == 0 ? 1 : enemyCount;
            
            enemyStats = new List<ShipStats>();
            for (int i = 0; i < enemyCount; i++)
            {
                ShipStats stats = new ShipStats(number);
                enemyStats.Add(stats);
            }
            
            enemyShips = new List<EnemyShip>();            
            for (int i = 0; i < enemyCount; i++)
            {
                EnemyShip enemy = Instantiate(instance.AllEnemyShips[Random.Range(0,instance.AllEnemyShips.Count)], instance.BattlePosition[i]);
                enemy.transform.rotation = instance.BattlePosition[i].rotation;
                enemy.transform.localPosition = Vector3.zero;
                
                enemy.SetStats(enemyStats[i]);
                
                enemyShips.Add(enemy); 
            }
            AppMetrica.Instance.ReportEvent("NewLevel");

        }     
    }
    
    public class ShipStats
    {
        private int maxHealth;
        private int health;
        private int damage;                        
            
        public ShipStats(int levelIndex)
        {
            damage = Random.Range(levelIndex * 3, levelIndex * 20);
            maxHealth = Random.Range(levelIndex * 3, levelIndex * 9);
            health = maxHealth;
        }

        public int GetMaxHealth()
        {
            return maxHealth;
        }

        public int GetHealth()
        {
            return health;
        }

        public int GetDamage()
        {
            return damage;
        }

        public void SetHealth(int value)
        {
            health = value;
        }

        public void SetMaxHealth(int value)
        {
            maxHealth = value;
        }

        public void SetDamage(int value)
        {
            damage = value;
        }
    }
    public void SaveLevelIndex()
    {
        PlayerPrefs.SetInt("Level",LevelIndex);
        PlayerPrefs.Save();
    }

    public void LoadLevelIndex()
    {
        LevelIndex=PlayerPrefs.GetInt("Level", 1);
    }

    private void SaveMoney()
    {
        PlayerPrefs.SetInt("Money",money);
    }

    private void LoadMoney()
    {
        money = PlayerPrefs.GetInt("Money", 100);
    }
}
