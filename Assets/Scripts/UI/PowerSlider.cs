﻿using System.Collections;
using System.Collections.Generic;
using CargoCult.Template.Injectro.Core;
using CargoCult.Template.Local.Game;
using UnityEngine;
using UnityEngine.UI;

public class PowerSlider : MonoBehaviour, IBean
{
    
    public Slider Selecter;
    public Text DamageText;
    public Crosshair cross;
    [Range(1f,3f)]
    public float Speed=2;

    [Inject] private GameView gameView;
    private float selectValue;
    private float maxDamage=100;
    private float resultDamage;
    private bool isStop = false;
    
    public void Start()
    {
    }

    public void Init(float max)
    {
        
        Selecter.minValue = -1;
        Selecter.maxValue = 1;
        
        maxDamage = max;
        DamageText.text = maxDamage.ToString();
        
        gameObject.SetActive(true);
        isStop = false;
    }

    public void Update()
    {
        if(isStop)
            return;
        
        selectValue = Mathf.Sin(Time.time*Speed);
        Selecter.value = selectValue;
        resultDamage = (maxDamage+1f) - Mathf.Abs(selectValue)*maxDamage;
    }

    public void OnTapSelect()
    {
        DamageText.text = ((int)resultDamage).ToString();
        isStop = true;
        StartCoroutine(DelayEnable(0.5f));
    }

    private IEnumerator DelayEnable(float sec)
    {
        yield return new WaitForSeconds(sec);
        gameView.CloseControlPanel();
        TurnManager.instance.PlayerSelectDamage((int)resultDamage);
        cross.gameObject.SetActive(false);
        gameObject.SetActive(false);              
    }
}
