﻿using System.Collections;
using System.Collections.Generic;
using CargoCult.Template.Global.Sound;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeElement : MonoBehaviour
{
    [SerializeField] public Text Title;
    [SerializeField] public Text UpLevel;
    [SerializeField] public Text Price;
    [SerializeField] public Text AddedValue;
    [SerializeField] public Image PriceIcon;
    [SerializeField] public Image UpgradeIcon;

    private PlayerUpgrade currentUpgrade;
    private UpgradeView controller;

    public void Init(UpgradeView view)
    {
        controller = view;
    }

    public void Fill(PlayerUpgrade upgrade)
    {                
        Title.text = upgrade.Discript;
        UpLevel.text ="Lvl " + upgrade.UpgradeLevel.ToString();
        AddedValue.text = "+ "+upgrade.AppendValue;
        Price.text = upgrade.Cost.ToString();
        PriceIcon.sprite = UpGradeManager.instance.CoinSprite;
        currentUpgrade = upgrade;
        UpgradeIcon.sprite = upgrade.UpgradeIcon;

        if (!upgrade.IsSaleAtMoney())
        {
            Price.text = "Free";
            PriceIcon.sprite = UpGradeManager.instance.AdsSprite;
        }                
    }

    public void DeInit()
    {
        controller.DeleteElement(this);
    }

    public void OnClick()
    {
        currentUpgrade.OnClickUpgrade(this);
        LevelManager.instance.Sound.Play(SoundType.UpgradeUp);
    }
}
