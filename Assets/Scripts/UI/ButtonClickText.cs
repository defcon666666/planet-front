﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonClickText : MonoBehaviour, IPointerUpHandler,IPointerDownHandler
{
    public Text ButtonText;
    public Color IdleColorText;
    public Color PressColorText;


    public void OnPointerUp(PointerEventData eventData)
    {
        ButtonText.color = IdleColorText;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        ButtonText.color = PressColorText;        
    }
}
