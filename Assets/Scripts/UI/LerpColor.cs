﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LerpColor : MonoBehaviour
{
    public Image ShadowColor;
    public Color ColorA;
    public Color ColorB;
    private Color colorC;

    private float r;
    private float g;
    private float b;
    private float a;

    private void Start()
    {
        colorC = ColorA;
    }

    public void Update()
    {
        ShadowColor.color = Color.Lerp(ColorB, colorC, Time.deltaTime);

        if (ShadowColor.color == ColorA)
        {
            colorC = ColorB;
        }

        if (ShadowColor.color == ColorB)
        {
            colorC = ColorA;
        }
    
    }
}
