﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Crosshair : MonoBehaviour
{
    public Image centre;

    private Vector3 target;
    private Vector3 current;
    
    public GameObject FollowObject;
    void Update()
    {
        
        if(!FollowObject)
            return;
            target =CameraManager.instance.MainCamera.WorldToScreenPoint(FollowObject.transform.position);                       
            centre.transform.position = target;
    }
}
