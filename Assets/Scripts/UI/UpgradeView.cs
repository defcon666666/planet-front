﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeView : MonoBehaviour
{
    public UpgradeElement UpgradeElementPrefab;
    public Transform content;

    private List<UpgradeElement> currentUpgrades = new List<UpgradeElement>();
    
    
    
    public void Open()
    {   
        ClearUpgradeList();
        FillUpgradeList();
        gameObject.SetActive(true);
    }

    public void Close()
    {
        ClearUpgradeList();
        gameObject.SetActive(false);
    }

    public void DeleteElement(UpgradeElement upgradeElement)
    {
        ClearUpgradeList();
        FillUpgradeList();
    }

    private void ClearUpgradeList()
    {
        foreach (var upgradeElement in currentUpgrades)
        {
            Destroy(upgradeElement.gameObject);
        }       
        currentUpgrades.Clear();
    }

    private void FillUpgradeList()
    {
        List<PlayerUpgrade> listUpgrade = UpGradeManager.instance.GetActualUpgrades();

        foreach (var upgrade in listUpgrade)
        {
            
            UpgradeElement localElement = Instantiate(UpgradeElementPrefab, content);
            localElement.Init(this);
            localElement.Fill(upgrade);
            currentUpgrades.Add(localElement);
        }
    }
}
