﻿using System.Collections;
using System.Collections.Generic;
using CargoCult.Template.Common.Separator;
using CargoCult.Template.Global.Sound;
using UnityEngine;

public class EnemyShip : Entity
{
    [Separator] 
    public MeshRenderer Renderer;
    public ParticleSystem HitEffectPart;
    public ParticleSystem DeadParticle;
    protected override void HitEffect()
    {
        base.HitEffect();
        
        HitEffectPart.Stop(true);
        HitEffectPart.Play(true);
        LevelManager.instance.Sound.Play(SoundType.ShipDamage);
    }

    public void Update()
    {
        if (isCanAttack)
        {
            isCanAttack = false;
            StartCoroutine(DelayAttack());
        }
    }

    public void SetStats(LevelManager.ShipStats stats)
    {
        MaxDamage = stats.GetDamage();
        MaxHealth = stats.GetMaxHealth();
        Health = stats.GetHealth();
        HealthView.Init(this,Health,MaxHealth);
    }

    protected override void Dead()
    {
        base.Dead();
        ParticleSystem dead = Instantiate(DeadParticle, transform.position, Quaternion.identity);
        dead.Play(true);
        TurnManager.instance.EnemyOnDead(this);
        UpGradeManager.instance.SpawnCoins(transform.position);
        LevelManager.instance.Sound.Play(SoundType.ShipDead);
        Destroy(gameObject);
    }

    private IEnumerator DelayAttack()
    {
        yield return new WaitForSeconds(Random.Range(0.3f,1.4f));
        Damage = Random.Range(1, MaxDamage);
        Weapon.Fire();
    }
}
