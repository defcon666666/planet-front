﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipMoving : MonoBehaviour
{
    public bool isMove=true;

    private float speed = 0.3f;
    private float timer;
    private float timeChangeTarget=3;
    private Vector3 target;
    private Vector3 startPos;
    private Quaternion startRotation;
    private Quaternion targetRotation;

    private Vector3 shipForward;
    
    void Start()
    {
        startPos = transform.position;
        target = transform.position;

        startRotation = transform.rotation;
        targetRotation = transform.rotation;

        shipForward = transform.forward;
    }

    // Update is called once per frame
    void Update()
    {
        if (isMove)
        {
            transform.position = Vector3.Lerp(transform.position, target, Time.deltaTime * speed);
            transform.rotation = Quaternion.Lerp(transform.rotation,targetRotation,Time.deltaTime *speed);
            
            timer += Time.deltaTime;
            if (timer > timeChangeTarget)
            {
                target = startPos + Random.onUnitSphere*Random.Range(1,3);
                timeChangeTarget = Random.Range(0.3f, 3f);

                Vector3 outPoint = transform.position + shipForward * 3;
                Vector3 lookPoint = outPoint + Random.onUnitSphere;
                Vector3 dir =lookPoint-transform.position;
                targetRotation = Quaternion.LookRotation(dir.normalized);
                
                timer = 0;
            }
        }
    }
}
