﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthView : MonoBehaviour
{
    public Text HealthText;
    public Slider healthBar;
    public TextMesh DamageTextMesh;
    public Animator AnimatorController;
    
    private Entity ownerView;

    private float currentHealth;
    private float newHealth;
    private float maxHealth;
    private bool init = false;
    
    public void Init(Entity owner,int current,int max)
    {
        ownerView = owner;
        healthBar.enabled = true;
        maxHealth = max;
        currentHealth = current;
        newHealth = current;
        
        healthBar.minValue = 0;
        healthBar.maxValue = maxHealth;
        healthBar.value = currentHealth;
        HealthText.text = currentHealth.ToString();
        init = true;
    }

    public void ShowDamage(int damage)
    {
        newHealth = damage;
        DamageTextMesh.text = "-" + damage;
        AnimatorController.SetTrigger("Damage");
    }

    void Update()
    {
        if(!init)
            return;
        
        currentHealth = ownerView.Health;
        HealthText.text = currentHealth.ToString();
        healthBar.value = Mathf.Lerp(healthBar.value, currentHealth, Time.deltaTime*2f);

    }
}
