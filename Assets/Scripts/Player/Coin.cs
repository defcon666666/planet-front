﻿using System.Collections;
using System.Collections.Generic;
using CargoCult.Template.Global.Sound;
using UnityEngine;

public class Coin : MonoBehaviour
{
    private Vector3 startPos;
    private Vector3 endPos = Vector3.zero;
    private bool isMove;
    private float time;
    public void Init()
    {
        startPos = transform.position;
        endPos = TurnManager.instance.Player.DamagePoint.transform.position;
        
        StartCoroutine(DelayFly(Random.Range(0.3f, 0.9f)));
    }

    private IEnumerator DelayFly(float sec)
    {
        yield return new WaitForSeconds(sec);
        isMove = true;
    }

    private void Update()
    {
        if(!isMove)
            return;
         time+= Time.deltaTime; 
        transform.position = Vector3.Slerp(startPos,endPos , time);

        if (Vector3.Distance(transform.position, endPos) < 0.2f)
        {
            LevelManager.instance.Sound.Play(SoundType.CoinUp);
            Destroy(gameObject);
        }
    }
}
