﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthUpgrade : PlayerUpgrade
{
    public override void Init()
    {
        idKey = "HealthUpgrade";
        base.Init();
        UpgradeIcon = UpGradeManager.instance.HealthSprite;
        Discript = "Health";
    }

    protected override void ApplyUpgrade()
    {
        base.ApplyUpgrade();
        UpGradeManager.instance.HealthUpgradeUp(AppendValue);
        AppMetrica.Instance.ReportEvent("HealthUpgrade");
    }
}
