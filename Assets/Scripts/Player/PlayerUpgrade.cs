﻿using System.Collections;
using System.Collections.Generic;
using CargoCult.Template.Global.Ads.Admob;
using CargoCult.Template.Global.Preference;
using CargoCult.Template.Injectro.Core;
using UnityEngine;

public class PlayerUpgrade : MonoBehaviour
{
    public int Cost;
    public int UpgradeLevel;
    public int AppendValue = 15;
    public int CurrentValue;
    public bool isVisible=true;
    public string Discript;
    public Sprite UpgradeIcon;
        
    protected string idKey="Upgrade";
    protected string idValueKey="CurrentValue";
    protected string idCostKey="Cost";
    
    protected bool saleAtMoney;
    protected UpgradeElement upgradeButton;

    public virtual void Init()
    {        
        LoadData();
    }

    public virtual void OnClickUpgrade(UpgradeElement button)
    {
        upgradeButton = button;
        if (IsSaleAtMoney())
        {
            LevelManager.instance.AddMoney(-Cost);
            UpgradeUp();
            
        }
        else if (LevelManager.instance.ads.IsRewardReady())
        {
            LevelManager.instance.ads.RunRewardAds(ResultRewardAds);            
        }

    }

    private void ResultRewardAds(AdsResult adsResult)
    {
        if (adsResult == AdsResult.Finished)
        {
            UpgradeUp();
        }
    }

    public virtual bool IsSaleAtMoney()
    {
        saleAtMoney = LevelManager.instance.EnoughMoney(Cost);
        return saleAtMoney;
    }

    protected virtual void SaveData()
    {
        PlayerPrefs.SetInt(idKey,UpgradeLevel);
        PlayerPrefs.SetInt(idKey+idValueKey,CurrentValue);
        PlayerPrefs.SetInt(idKey+idCostKey,Cost);
    }

    protected virtual void LoadData()
    {
        UpgradeLevel = PlayerPrefs.GetInt(idKey, 1);
        CurrentValue = PlayerPrefs.GetInt(idKey+idValueKey, 1);
        Cost = PlayerPrefs.GetInt(idKey + idCostKey, 1);
    }

    protected virtual void UpgradeUp()
    {
        ApplyUpgrade();
        
        UpgradeLevel++;
        Cost = (UpgradeLevel * 100);
        SaveData();

        if (CheckForUse())
        {
            upgradeButton.Fill(this);
        }
        else
        {
            upgradeButton.DeInit();
        }
    }

    protected virtual void ApplyUpgrade()
    {
        
    }

    public bool CheckForUse()
    {
        return LevelManager.instance.ads.IsRewardReady() || LevelManager.instance.EnoughMoney(Cost);
    }
}
