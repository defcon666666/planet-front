﻿using System.Collections;
using System.Collections.Generic;
using CargoCult.Template.Common.Input;
using CargoCult.Template.Global.Sound;
using CargoCult.Template.Injectro.Core;
using CargoCult.Template.Local.Game;
using UnityEngine;
using Input = UnityEngine.Input;

public class Player : Entity
{
    public ParticleSystem HitEffectPart;
    public ParticleSystem DeadEffectPart;

    public PlanetEnvironment Planet;

    private EnemyShip target;
    private int targetIndex;
    
    private int playerLevel;
    private int playerMaxDamage;
    private int playerMaxHealth;
    private int playerSupportGun;
    private int playerExp;

    private bool isDead = false;

    private const string levelKey = "PlayerLevel";
    private const string maxDamageKey = "PlayerMaxDamage";
    private const string maxHealthKey = "PlayerMaxHealth";
    private const string supportGunKey = "PlayerSupport";
    private const string expKey = "PlayerExp";
    
    void Start()
    {
        
    }

    public override void Init(Entity enemy)
    {
        LoadPlayerStats();
        base.Init(enemy);
        
        DeadEffectPart.Stop(true);
        Planet.gameObject.SetActive(true);
        isDead = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(isCanAttack)            
            if(isAttack)
            {
                Weapon.Fire();
                
                isCanAttack = false;
                isAttack = false;
            }
    }

    protected override void Dead()
    {
        if(isDead)
            return;
        base.Dead();
        DeadEffectPart.Play(true);
        Planet.gameObject.SetActive(false);
        isDead = true;
        TurnManager.instance.PlayerLoose();
        LevelManager.instance.Sound.Play(SoundType.PlanetDead);
    }

    public override void Win()
    {
        base.Win();
        Planet.Happy();
    }

    protected override void HitEffect()
    {
        
        
        base.HitEffect();
        print("HitPlanet");
        LevelManager.instance.Sound.Play(SoundType.PlanetDamage);
        HitEffectPart.Stop(true);
        HitEffectPart.Play(true);

        PlanetDamage();
    }

    private void SavePlayerStats()
    {
        PlayerPrefs.SetInt(levelKey,playerLevel);
        PlayerPrefs.SetInt(maxDamageKey,playerMaxDamage);
        PlayerPrefs.SetInt(maxHealthKey,playerMaxHealth);
        PlayerPrefs.SetInt(supportGunKey,playerSupportGun);
        PlayerPrefs.SetInt(expKey,playerExp);
    }

    private void LoadPlayerStats()
    {
        playerLevel = PlayerPrefs.GetInt(levelKey);
        playerMaxDamage = PlayerPrefs.GetInt(maxDamageKey,5);
        playerMaxHealth = PlayerPrefs.GetInt(maxHealthKey,10);
        playerSupportGun = PlayerPrefs.GetInt(supportGunKey);
        playerExp = PlayerPrefs.GetInt(expKey);

        MaxDamage = playerMaxDamage;
        MaxHealth = playerMaxHealth;
    }

    public void DamageUp(int addedValue)
    {
        playerMaxDamage += addedValue;
        MaxDamage = playerMaxDamage;
        
        SavePlayerStats();
    }

    public void HealthUp(int addedValue)
    {
        playerMaxHealth += addedValue;
        MaxHealth = playerMaxHealth;
        Health = MaxHealth;
        
        HealthView.Init(this,Health,MaxHealth);
        
        SavePlayerStats();
    }

    public void LeftTarget()
    {
        NextTarget(false);
    }

    public void RightTarget()
    {
        NextTarget(true);
        
    }

    private void PlanetDamage()
    {
        float normalValue = (float)Health / MaxHealth;                
        Planet.SwitchDamageStay(normalValue);
    }

    public void NextTarget(bool isRight)
    {
        if(TurnManager.instance.activEnemy==null)
            return;
        
        int maxIndex = TurnManager.instance.activEnemy.Count;
        
        if (isRight)
            targetIndex++;

        if (!isRight)
            targetIndex--;

        if (targetIndex >= maxIndex)
            targetIndex = 0;

        if (targetIndex < 0)
            targetIndex = maxIndex - 1;

        target = TurnManager.instance.activEnemy[targetIndex];
        Weapon.Init(this,target);
        SceneContext.Instance.GetBean<GameView>().Cross.FollowObject = target.gameObject;

        CameraManager.instance.SetShootEndPoint(target.transform);
    }
}
