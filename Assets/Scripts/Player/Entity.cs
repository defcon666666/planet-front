﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{
    public int Health=10;
    public int MaxHealth=10;
    public int Damage=1;
    public int MaxDamage=1;
    [Space(10)]
    public GameObject DamagePoint;
    public Turrent Weapon;
    public HealthView HealthView;
    [Space(10)]
    public bool isCanAttack=false;
    public bool isAttack;

    public virtual void Init(Entity enemy)
    {
         Health  =MaxHealth;
        
        Weapon.Init(this,enemy);
        HealthView.Init(this,Health,MaxHealth);
    }

    public virtual void OnHit(int damage)
    { 
        HealthCalculate(damage);
        
        HitEffect();
               
    }

    public virtual void FinishShoot()
    {
        StartCoroutine(DelayEndTurn(0.8f));
    }

    protected virtual void HitEffect()
    {
        
    }

    protected virtual void Dead()
    {
        
    }

    public virtual void Win()
    {
        
    }

    private void HealthCalculate(int hitedDamge)
    {
        HealthView.ShowDamage(hitedDamge);
        Health -= hitedDamge;
        if (Health <= 0)
        {
            Health = 0;
            Dead();
        }
    }

    private IEnumerator DelayEndTurn(float sec)
    {
        yield return new WaitForSeconds(sec);
        TurnManager.instance.TurnEnd(this);
    }
}
