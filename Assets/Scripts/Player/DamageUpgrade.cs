﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageUpgrade : PlayerUpgrade
{
    public override void Init()
    {
        idKey = "DamageUpgrade";
        base.Init();        
        UpgradeIcon = UpGradeManager.instance.DamagaSprite;
        Discript = "Attack";
    }

    protected override void ApplyUpgrade()
    {        
        base.ApplyUpgrade();
        UpGradeManager.instance.DamageUpgradeUp(AppendValue);
        AppMetrica.Instance.ReportEvent("DamageUpgrade");
    }
}
