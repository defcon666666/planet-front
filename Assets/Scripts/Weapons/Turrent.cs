﻿using System.Collections;
using System.Collections.Generic;
using CargoCult.Template.Global.Sound;
using UnityEngine;

public class Turrent : MonoBehaviour
{
    public Shell BulletPrefab;
    public Transform Muzzle;
    public ParticleSystem LaunchPart;
    public Animator animController;
    public GameObject Barrel;
    
    
    private Entity weaponOwner;
    private Entity enemy;
    private Transform target;
    private Quaternion lerp;
    private Vector3 targetPos;
    private bool init = false;
    
    
    void Awake()
    {
        LaunchPart.Stop(true);
        targetPos = transform.position + transform.forward * 10;
    }

    public virtual void Init(Entity owner,Entity enemy)
    {
        weaponOwner = owner;
        this.enemy = enemy;
        target = enemy.DamagePoint.transform;       
        init = true;
    }

    protected virtual void Update()
    {
        if(!init)
            return;

        if (Barrel && target)
        {

            targetPos = Vector3.Lerp(targetPos, target.position, Time.deltaTime);    
            Barrel.transform.LookAt(targetPos,Vector3.up);
        }
    }

    public virtual void Fire()
    {
        if(!init)
            return;

        Shell bullet = Instantiate(BulletPrefab, Muzzle.position, Muzzle.rotation);
        bullet.Launch(target.position,this);
        animController.SetTrigger("Rollback");
        
        LaunchPart.Stop(true);
        LaunchPart.Play(true);

        if (weaponOwner is Player)
        {
            CameraManager.instance.SetView(CameraManager.CameraView.ShellStart);
        }

        LevelManager.instance.Sound.Play(SoundType.LaunchMissile);

        if (weaponOwner is EnemyShip)
        {
            
        }
    }

    public virtual void Hit()
    {
        if(!init)
            return;
        
        enemy.OnHit(weaponOwner.Damage);
        weaponOwner.FinishShoot();
    }

    public Entity GetWeaponOwner()
    {
        return weaponOwner;
    }
}
