﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shell : MonoBehaviour
{
    public ParticleSystem FlyEffect;
    public ParticleSystem BoomEffect;
    public Rigidbody rigid;
    public MeshRenderer Renderer;

    
    private Turrent turrent;
    private Vector3 target;
    private Vector3 dirMove;
    private Vector3 shellVelocity;
    private Vector3 startPoint;

    private float startForce = 30f;
    private float coldLaunchTime = 0.3f;
    private float timeFly;
    private bool isFly;

    public void Launch(Vector3 targetPoint,Turrent owner)
    {
        turrent = owner;
        target = targetPoint;
        dirMove = transform.forward;
        
        FlyEffect.Stop(true);
        BoomEffect.Stop(true);
        
        if(rigid==null)
            rigid = GetComponent<Rigidbody>();
        rigid.AddForce(dirMove*startForce,ForceMode.Impulse);

        StartCoroutine(DelayStart(coldLaunchTime));
    }

    IEnumerator DelayStart(float sec)
    {
        yield return new WaitForSeconds(sec);
        FlyEffect.Play(true);
        startPoint = transform.position;
        rigid.useGravity = false;
        rigid.isKinematic = true;
        isFly = true;

        if (turrent.GetWeaponOwner() is Player)
        {
            CameraManager.instance.SetView(CameraManager.CameraView.ShellEnd);
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(transform.position+dirMove);
        
        if(!isFly)
            return;
        dirMove = target - transform.position;
        dirMove.Normalize();

        timeFly += Time.deltaTime+Time.deltaTime/2;
        transform.position = Vector3.Slerp(startPoint, target, timeFly);
        if (Vector3.Distance(transform.position, target) < 0.1f)
        {
            EndFly();
        }
    }

    private void EndFly()
    {
        isFly = false;
        turrent.Hit();
        Renderer.enabled = false;
        FlyEffect.Stop(true);
        BoomEffect.Play();
        Destroy(gameObject,0.7f);
    }
}
