﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadarLaser : MonoBehaviour
{
    public LineRenderer line;
    void Start()
    {
         line = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
         line.SetPosition(0,transform.position);
         line.SetPosition(1,transform.position+transform.forward*3);
        
    }
}
